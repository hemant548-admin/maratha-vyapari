package com.example.marathavyapari.API;


import com.example.marathavyapari.Model.BusinessAdvertiseModel;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessPostModel;
import com.example.marathavyapari.Model.BusinessServiceModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.ReadNotiModel;
import com.example.marathavyapari.Model.ResponseFCMModel;
import com.example.marathavyapari.Model.SearchResultModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServicePostModel;
import com.example.marathavyapari.Model.ServiceRevioewModelData;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.Model.ShowBusiServiceModel;
import com.example.marathavyapari.Model.ShowBusineePostModel;
import com.example.marathavyapari.Model.ShowBusinessReviewModel;
import com.example.marathavyapari.Model.ShowReViewServiceModel;
import com.example.marathavyapari.Model.ShowServicePostModel;
import com.example.marathavyapari.Model.SubBusinessDataModel;
import com.example.marathavyapari.Model.SubBusinessListModel;
import com.example.marathavyapari.Model.SubServiceDataModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.Model.UnreadNotificationModel;
import com.example.marathavyapari.Model.UserNotificationModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    public static String BaseUrl = "http://marathavyapari.com/maratha/api/";

    @GET("common/getTaluka")
    Call<TalukaDataModel> talukalist(@Query("district_id") String districtid);

    @POST("business/businessSubCategory")
    Call<SubBusinessListModel> showSubBusinessCat(@Query("category_id") String category_id, @Query("lang")String lang);


    @FormUrlEncoded
    @POST("users/getBusinessBySubcategory")
    Call<SubBusinessDataModel> showSubbusinessCat(@Header("token") String token, @Field("subcategory_id") String sub_category_id);

    @FormUrlEncoded
    @POST("users/getServiceBySubcategory")
    Call<SubServiceDataModel> showSubServicesCat(@Header("token") String token, @Field("subcategory_id") String sub_category_id);


    @FormUrlEncoded
    @POST("users/getServiceBySubcategory")
    Call<SubServiceDataModel> showSubdisttalukawise(@Header("token")String token, @Field("subcategory_id")String sub_category_id,
                                                    @Field("district_id")String districtid,  @Field("taluka_id")String talukaid);

    @FormUrlEncoded
    @POST("users/getBusinessBySubcategory")
    Call<SubBusinessDataModel> showSubbusincatdisttalwise(@Header("token")String token, @Field("subcategory_id")String sub_category_id,
                                                          @Field("district_id")String districtid, @Field("taluka_id")String talukaid);

    @GET("users/servicePostInLimit")
    Call<ServicePostModel> showOneServicePost();

    @GET("users/getAllPostInLimit")
    Call<BusinessPostModel> showOneBusinessPost();

    @FormUrlEncoded
    @POST("users/getBusinessDetailById")
    Call<ShowBusineePostModel> showShopdetailbuswise(@Header("token")String token, @Field("business_id")String businessid);

    @FormUrlEncoded
    @POST("users/getServiceDetailById")
    Call<ShowServicePostModel> showShopdetailservicewise(@Header("token")String token, @Field("service_id")String serviceId);


    @GET("users/getServiceAllReview")
    Call<ServiceRevioewModelData> showServiceReview(@Header("token")String token, @Query("service_id") String serviceId);

    @GET("users/getBusinessAllReview")
    Call<ShowBusinessReviewModel> showBusinessReview(@Header("token")String token, @Query("business_id") String businessId);

    @GET("users/getAllPost")
    Call<BusinessServiceModel> showBusinessServicePost();

    @GET("common/getTaluka")
    Call<TalukaDataModel> talukalist(@Query("district_id")String districtid, @Query("lang") String lang);
    @GET("common/getDistrict")
    Call<DistrictModel> districtlist(@Query("lang") String lang);

    @POST("users/updateUserFcmToken")
    Call<ResponseFCMModel> updateUserFCM(@Query("user_id")String user_id, @Query("fcm_token") String fcmregistrationId);

    @POST("services/updateServiceFcmToken")
    Call<ResponseFCMModel> updateServiceFCM(@Query("service_id") String serviceId, @Query("fcm_token") String fcmregistrationId);

    @POST("business/updateBusinessFcmToken")
    Call<ResponseFCMModel> updateBusinessFCM(@Query("business_id") String businessId, @Query("fcm_token") String fcmregistrationId);


    @POST("users/getUserNotification")
    Call<UserNotificationModel> showUserNotify(@Query("user_id") String user_id, @Query("language") String lang);

    @POST("business/getBusinessNotification")
    Call<UserNotificationModel> showBusinessNotify(@Query("business_id")String businessid, @Query("language")String lang);

    @POST("services/getServiceNotification")
    Call<UserNotificationModel> showServiceNotify(@Query("service_id")String serviceid, @Query("language")String lang);

    @POST("users/getBusinessAdvBySubCategory")
    Call<BusinessAdvertiseModel> showBusinssAdv(@Query("subcategory_id") String sub_category_id);

    @POST("users/getServiceAdvBySubcategory")
    Call<BusinessAdvertiseModel> showServiceAdv(@Query("subcategory_id")String sub_category_id);

    @FormUrlEncoded
    @POST("users/getBusinessBySubcategory")
    Call<SubBusinessDataModel> showsearchbuslist(@Header("token")String token, @Field("subcategory_id")String sub_category_id,
                                                 @Field("district_id")String districtid, @Field("taluka_id")String talukaid, @Field("search_key") String text);

    @FormUrlEncoded
    @POST("users/getServiceBySubcategory")
    Call<SubServiceDataModel> showSearchService(@Header("token")String token, @Field("subcategory_id")String sub_category_id,
                                                @Field("district_id")String districtid, @Field("taluka_id")String talukaid, @Field("search_key") String text);


    @Headers("Content-Type: application/json")
    @POST("users/homePageSearch")
    Call<SearchResultModel> ShowSearchResult(@Query("search_key") String search_key);

    @GET("services/serviceSubCategory")
    Call<ServiceSubCatModel> showSubCat(@Query("category_id")String serviceCatId, @Query("lang") String lang);

    @GET("services/serviceSubCategory")
    Call<ServiceSubCatModel> showSubCat(@Query("category_id") String serviceCatId);


    @GET("business/businessCategory")
    Call<BusinessCategoryModel> showBusinessCategory(@Query("lang")String lang);

    @GET("services/serviceCategory")
    Call<ServiceCategoryModel> showServiceCate(@Query("lang")String lang);

    @POST("users/getUserUnreadNotificationCount")
    Call<UnreadNotificationModel> showUserCount(@Query("user_id") String user_id);

    @POST("users/unreadUserNotification")
    Call<ReadNotiModel> showreadnoti(@Query("user_id")String user_id);

    @POST("services/getServiceUnreadNotificationCount")
    Call<UnreadNotificationModel> showServiceUnreadNoti(@Query("service_id")String serviceId);

    @POST("services/unreadServiceNotification")
    Call<ReadNotiModel> showServicereadnoti(@Query("service_id")String serviceId);

    @POST("business/getBusinessUnreadNotificationCount")
    Call<UnreadNotificationModel> showBusinessUnreadNotify(@Query("business_id")String businessId);

    @POST("business/unreadBusinessNotification")
    Call<ReadNotiModel> showBusinessreadnoti(@Query("business_id") String businessId);
}
