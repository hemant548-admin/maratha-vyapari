package com.example.marathavyapari.Model;

import android.icu.text.StringSearch;

public class ServiceLoginModel {

    private String token;
    private String sus_id;
    private String category_id_fk;
    private String subcategory_id_fk;
    private String sus_mobile;
    private String sus_name;
    private String name;
    private String service_address;
    private String service_description;
    private String service_type;
    private String licence;
    private String service_status;
    private String sc_id;
    private String sc_name;
    private String sc_image;
    private String live_status;
    private String ssc_name;



    public ServiceLoginModel(String token, String sus_id, String category_id_fk, String subcategory_id_fk, String sus_mobile, String sus_name, String name, String service_address, String service_description, String service_type, String licence, String service_status, String sc_id, String sc_name, String live_status, String ssc_name) {
        this.token = token;
        this.sus_id = sus_id;
        this.category_id_fk = category_id_fk;
        this.subcategory_id_fk = subcategory_id_fk;
        this.sus_mobile = sus_mobile;
        this.sus_name = sus_name;
        this.name = name;
        this.service_address = service_address;
        this.service_description = service_description;
        this.service_type = service_type;
        this.licence = licence;
        this.service_status = service_status;
        this.sc_id = sc_id;
        this.sc_name = sc_name;
        this.sc_image = sc_image;
        this.live_status = live_status;
        this.ssc_name = ssc_name;
    }



    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSus_id() {
        return sus_id;
    }

    public void setSus_id(String sus_id) {
        this.sus_id = sus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getSubcategory_id_fk() {
        return subcategory_id_fk;
    }

    public void setSubcategory_id_fk(String subcategory_id_fk) {
        this.subcategory_id_fk = subcategory_id_fk;
    }

    public String getSus_mobile() {
        return sus_mobile;
    }

    public void setSus_mobile(String sus_mobile) {
        this.sus_mobile = sus_mobile;
    }

    public String getSus_name() {
        return sus_name;
    }

    public void setSus_name(String sus_name) {
        this.sus_name = sus_name;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_address() {
        return service_address;
    }

    public void setService_address(String service_address) {
        this.service_address = service_address;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getSc_id() {
        return sc_id;
    }

    public void setSc_id(String sc_id) {
        this.sc_id = sc_id;
    }

    public String getSc_name() {
        return sc_name;
    }

    public void setSc_name(String sc_name) {
        this.sc_name = sc_name;
    }

    public String getSc_image() {
        return sc_image;
    }

    public void setSc_image(String sc_image) {
        this.sc_image = sc_image;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }



    public String getSsc_name() {
        return ssc_name;
    }

    public void setSsc_name(String ssc_name) {
        this.ssc_name = ssc_name;
    }


}
