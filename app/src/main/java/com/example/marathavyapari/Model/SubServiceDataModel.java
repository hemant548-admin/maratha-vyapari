package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class SubServiceDataModel {

    private String status;
    private String message;
    private ArrayList<ServiceListData> service;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ServiceListData> getService() {
        return service;
    }

    public void setService(ArrayList<ServiceListData> service) {
        this.service = service;
    }

    public static class ServiceListData{

        private String sus_id,category_id_fk,subcategory_id_fk, district_id_fk, taluka_id_fk, sus_mobile, sus_name, updated_at, created_at,
                name, service_address, service_description, service_type, service_latitude, service_longitude, avrage_rating, no_of_rating,licence,
                service_status,sc_id, sc_name, sc_image, live_status, ssc_id, sc_id_fk, ssc_name,ssc_image,district_id, district_name,taluka_id, taluka_name;

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getSus_id() {
            return sus_id;
        }

        public void setSus_id(String sus_id) {
            this.sus_id = sus_id;
        }

        public String getCategory_id_fk() {
            return category_id_fk;
        }

        public void setCategory_id_fk(String category_id_fk) {
            this.category_id_fk = category_id_fk;
        }

        public String getSubcategory_id_fk() {
            return subcategory_id_fk;
        }

        public void setSubcategory_id_fk(String subcategory_id_fk) {
            this.subcategory_id_fk = subcategory_id_fk;
        }

        public String getDistrict_id_fk() {
            return district_id_fk;
        }

        public void setDistrict_id_fk(String district_id_fk) {
            this.district_id_fk = district_id_fk;
        }

        public String getTaluka_id_fk() {
            return taluka_id_fk;
        }

        public void setTaluka_id_fk(String taluka_id_fk) {
            this.taluka_id_fk = taluka_id_fk;
        }

        public String getSus_mobile() {
            return sus_mobile;
        }

        public void setSus_mobile(String sus_mobile) {
            this.sus_mobile = sus_mobile;
        }

        public String getSus_name() {
            return sus_name;
        }

        public void setSus_name(String sus_name) {
            this.sus_name = sus_name;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getService_address() {
            return service_address;
        }

        public void setService_address(String service_address) {
            this.service_address = service_address;
        }

        public String getService_description() {
            return service_description;
        }

        public void setService_description(String service_description) {
            this.service_description = service_description;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getService_latitude() {
            return service_latitude;
        }

        public void setService_latitude(String service_latitude) {
            this.service_latitude = service_latitude;
        }

        public String getService_longitude() {
            return service_longitude;
        }

        public void setService_longitude(String service_longitude) {
            this.service_longitude = service_longitude;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getService_status() {
            return service_status;
        }

        public void setService_status(String service_status) {
            this.service_status = service_status;
        }

        public String getSc_id() {
            return sc_id;
        }

        public void setSc_id(String sc_id) {
            this.sc_id = sc_id;
        }

        public String getSc_name() {
            return sc_name;
        }

        public void setSc_name(String sc_name) {
            this.sc_name = sc_name;
        }

        public String getSc_image() {
            return sc_image;
        }

        public void setSc_image(String sc_image) {
            this.sc_image = sc_image;
        }

        public String getLive_status() {
            return live_status;
        }

        public void setLive_status(String live_status) {
            this.live_status = live_status;
        }

        public String getSsc_id() {
            return ssc_id;
        }

        public void setSsc_id(String ssc_id) {
            this.ssc_id = ssc_id;
        }

        public String getSc_id_fk() {
            return sc_id_fk;
        }

        public void setSc_id_fk(String sc_id_fk) {
            this.sc_id_fk = sc_id_fk;
        }

        public String getSsc_name() {
            return ssc_name;
        }

        public void setSsc_name(String ssc_name) {
            this.ssc_name = ssc_name;
        }

        public String getSsc_image() {
            return ssc_image;
        }

        public void setSsc_image(String ssc_image) {
            this.ssc_image = ssc_image;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(String taluka_id) {
            this.taluka_id = taluka_id;
        }

        public String getTaluka_name() {
            return taluka_name;
        }

        public void setTaluka_name(String taluka_name) {
            this.taluka_name = taluka_name;
        }


    }
}
