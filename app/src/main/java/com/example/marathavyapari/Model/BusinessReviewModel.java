package com.example.marathavyapari.Model;

public class BusinessReviewModel {

    private String br_id, user_id_fk, business_id_fk, review_message, review_date, review_time, created_at, bus_id, category_id_fk, bus_mobile, bus_name,
            owner_name, business_address, business_description, business_type, detail_info, licence, photo, business_latitude, business_longitude,
            avrage_rating, no_of_rating, business_status, business_email, uus_mobile, user_email, user_name, user_photo;

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public BusinessReviewModel(String br_id, String user_id_fk, String business_id_fk, String review_message, String review_date, String review_time, String created_at, String bus_id, String category_id_fk, String bus_mobile, String bus_name, String owner_name, String business_address, String business_description, String business_type, String detail_info, String licence, String photo, String business_latitude, String business_longitude, String avrage_rating, String no_of_rating, String business_status, String business_email, String uus_mobile, String user_email, String user_name, String user_photo) {
        this.br_id = br_id;
        this.user_id_fk = user_id_fk;
        this.business_id_fk = business_id_fk;
        this.review_message = review_message;
        this.review_date = review_date;
        this.review_time = review_time;
        this.created_at = created_at;
        this.bus_id = bus_id;
        this.category_id_fk = category_id_fk;
        this.bus_mobile = bus_mobile;
        this.bus_name = bus_name;
        this.owner_name = owner_name;
        this.business_address = business_address;
        this.business_description = business_description;
        this.business_type = business_type;
        this.detail_info = detail_info;
        this.licence = licence;
        this.photo = photo;
        this.business_latitude = business_latitude;
        this.business_longitude = business_longitude;
        this.avrage_rating = avrage_rating;
        this.no_of_rating = no_of_rating;
        this.business_status = business_status;
        this.business_email = business_email;
        this.uus_mobile = uus_mobile;
        this.user_email = user_email;
        this.user_name = user_name;
        this.user_photo = user_photo;
    }

    public String getBr_id() {
        return br_id;
    }

    public void setBr_id(String br_id) {
        this.br_id = br_id;
    }

    public String getUser_id_fk() {
        return user_id_fk;
    }

    public void setUser_id_fk(String user_id_fk) {
        this.user_id_fk = user_id_fk;
    }

    public String getBusiness_id_fk() {
        return business_id_fk;
    }

    public void setBusiness_id_fk(String business_id_fk) {
        this.business_id_fk = business_id_fk;
    }

    public String getReview_message() {
        return review_message;
    }

    public void setReview_message(String review_message) {
        this.review_message = review_message;
    }

    public String getReview_date() {
        return review_date;
    }

    public void setReview_date(String review_date) {
        this.review_date = review_date;
    }

    public String getReview_time() {
        return review_time;
    }

    public void setReview_time(String review_time) {
        this.review_time = review_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBus_id() {
        return bus_id;
    }

    public void setBus_id(String bus_id) {
        this.bus_id = bus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getBus_mobile() {
        return bus_mobile;
    }

    public void setBus_mobile(String bus_mobile) {
        this.bus_mobile = bus_mobile;
    }

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getBusiness_description() {
        return business_description;
    }

    public void setBusiness_description(String business_description) {
        this.business_description = business_description;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getDetail_info() {
        return detail_info;
    }

    public void setDetail_info(String detail_info) {
        this.detail_info = detail_info;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBusiness_latitude() {
        return business_latitude;
    }

    public void setBusiness_latitude(String business_latitude) {
        this.business_latitude = business_latitude;
    }

    public String getBusiness_longitude() {
        return business_longitude;
    }

    public void setBusiness_longitude(String business_longitude) {
        this.business_longitude = business_longitude;
    }

    public String getAvrage_rating() {
        return avrage_rating;
    }

    public void setAvrage_rating(String avrage_rating) {
        this.avrage_rating = avrage_rating;
    }

    public String getNo_of_rating() {
        return no_of_rating;
    }

    public void setNo_of_rating(String no_of_rating) {
        this.no_of_rating = no_of_rating;
    }

    public String getBusiness_status() {
        return business_status;
    }

    public void setBusiness_status(String business_status) {
        this.business_status = business_status;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getUus_mobile() {
        return uus_mobile;
    }

    public void setUus_mobile(String uus_mobile) {
        this.uus_mobile = uus_mobile;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }


}
