package com.example.marathavyapari.Model;

public class ShowTransactionModel {


    private String type, trans_id, user_id_fk, business_id_fk, service_id_fk, bus_id, category_id_fk, bus_mobile, bus_name, owner_name, business_address,
            business_description, business_type, detail_info, licence, photo, business_latitude, business_longitude, avrage_rating,no_of_rating, business_status,
            business_email,sus_id, sus_mobile, sus_name, name, service_address, service_description,service_type,
            service_latitude, service_longitude,service_status;


    public ShowTransactionModel(String type, String trans_id, String user_id_fk, String business_id_fk, String service_id_fk, String bus_id, String category_id_fk, String bus_mobile, String bus_name, String owner_name, String business_address, String business_description, String business_type, String detail_info, String licence, String photo, String business_latitude, String business_longitude, String avrage_rating, String no_of_rating, String business_status, String business_email, String sus_id, String sus_mobile, String sus_name, String name, String service_address, String service_description, String service_type, String service_latitude, String service_longitude, String service_status) {
        this.type = type;
        this.trans_id = trans_id;
        this.user_id_fk = user_id_fk;
        this.business_id_fk = business_id_fk;
        this.service_id_fk = service_id_fk;
        this.bus_id = bus_id;
        this.category_id_fk = category_id_fk;
        this.bus_mobile = bus_mobile;
        this.bus_name = bus_name;
        this.owner_name = owner_name;
        this.business_address = business_address;
        this.business_description = business_description;
        this.business_type = business_type;
        this.detail_info = detail_info;
        this.licence = licence;
        this.photo = photo;
        this.business_latitude = business_latitude;
        this.business_longitude = business_longitude;
        this.avrage_rating = avrage_rating;
        this.no_of_rating = no_of_rating;
        this.business_status = business_status;
        this.business_email = business_email;
        this.sus_id = sus_id;
        this.sus_mobile = sus_mobile;
        this.sus_name = sus_name;
        this.name = name;
        this.service_address = service_address;
        this.service_description = service_description;
        this.service_type = service_type;
        this.service_latitude = service_latitude;
        this.service_longitude = service_longitude;
        this.service_status = service_status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getUser_id_fk() {
        return user_id_fk;
    }

    public void setUser_id_fk(String user_id_fk) {
        this.user_id_fk = user_id_fk;
    }

    public String getBusiness_id_fk() {
        return business_id_fk;
    }

    public void setBusiness_id_fk(String business_id_fk) {
        this.business_id_fk = business_id_fk;
    }

    public String getService_id_fk() {
        return service_id_fk;
    }

    public void setService_id_fk(String service_id_fk) {
        this.service_id_fk = service_id_fk;
    }

    public String getBus_id() {
        return bus_id;
    }

    public void setBus_id(String bus_id) {
        this.bus_id = bus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getBus_mobile() {
        return bus_mobile;
    }

    public void setBus_mobile(String bus_mobile) {
        this.bus_mobile = bus_mobile;
    }

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getBusiness_description() {
        return business_description;
    }

    public void setBusiness_description(String business_description) {
        this.business_description = business_description;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getDetail_info() {
        return detail_info;
    }

    public void setDetail_info(String detail_info) {
        this.detail_info = detail_info;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBusiness_latitude() {
        return business_latitude;
    }

    public void setBusiness_latitude(String business_latitude) {
        this.business_latitude = business_latitude;
    }

    public String getBusiness_longitude() {
        return business_longitude;
    }

    public void setBusiness_longitude(String business_longitude) {
        this.business_longitude = business_longitude;
    }

    public String getAvrage_rating() {
        return avrage_rating;
    }

    public void setAvrage_rating(String avrage_rating) {
        this.avrage_rating = avrage_rating;
    }

    public String getNo_of_rating() {
        return no_of_rating;
    }

    public void setNo_of_rating(String no_of_rating) {
        this.no_of_rating = no_of_rating;
    }

    public String getBusiness_status() {
        return business_status;
    }

    public void setBusiness_status(String business_status) {
        this.business_status = business_status;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getSus_id() {
        return sus_id;
    }

    public void setSus_id(String sus_id) {
        this.sus_id = sus_id;
    }

    public String getSus_mobile() {
        return sus_mobile;
    }

    public void setSus_mobile(String sus_mobile) {
        this.sus_mobile = sus_mobile;
    }

    public String getSus_name() {
        return sus_name;
    }

    public void setSus_name(String sus_name) {
        this.sus_name = sus_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_address() {
        return service_address;
    }

    public void setService_address(String service_address) {
        this.service_address = service_address;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getService_latitude() {
        return service_latitude;
    }

    public void setService_latitude(String service_latitude) {
        this.service_latitude = service_latitude;
    }

    public String getService_longitude() {
        return service_longitude;
    }

    public void setService_longitude(String service_longitude) {
        this.service_longitude = service_longitude;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

}
