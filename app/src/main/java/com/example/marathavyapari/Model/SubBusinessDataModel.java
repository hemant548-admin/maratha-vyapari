package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class SubBusinessDataModel {
    private String status;
    private String message;
    private ArrayList<BusinessListModel> business;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<BusinessListModel> getBusiness() {
        return business;
    }

    public void setBusiness(ArrayList<BusinessListModel> business) {
        this.business = business;
    }

    public static class BusinessListModel{

        public String getBus_id() {
            return bus_id;
        }

        public void setBus_id(String bus_id) {
            this.bus_id = bus_id;
        }

        public String getCategory_id_fk() {
            return category_id_fk;
        }

        public void setCategory_id_fk(String category_id_fk) {
            this.category_id_fk = category_id_fk;
        }

        public String getSubcategory_id_fk() {
            return subcategory_id_fk;
        }

        public void setSubcategory_id_fk(String subcategory_id_fk) {
            this.subcategory_id_fk = subcategory_id_fk;
        }

        public String getDistrict_id_fk() {
            return district_id_fk;
        }

        public void setDistrict_id_fk(String district_id_fk) {
            this.district_id_fk = district_id_fk;
        }

        public String getTaluka_id_fk() {
            return taluka_id_fk;
        }

        public void setTaluka_id_fk(String taluka_id_fk) {
            this.taluka_id_fk = taluka_id_fk;
        }

        public String getBus_mobile() {
            return bus_mobile;
        }

        public void setBus_mobile(String bus_mobile) {
            this.bus_mobile = bus_mobile;
        }

        public String getBus_name() {
            return bus_name;
        }

        public void setBus_name(String bus_name) {
            this.bus_name = bus_name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getOwner_name() {
            return owner_name;
        }

        public void setOwner_name(String owner_name) {
            this.owner_name = owner_name;
        }

        public String getBusiness_address() {
            return business_address;
        }

        public void setBusiness_address(String business_address) {
            this.business_address = business_address;
        }

        public String getBusiness_description() {
            return business_description;
        }

        public void setBusiness_description(String business_description) {
            this.business_description = business_description;
        }

        public String getBusiness_type() {
            return business_type;
        }

        public void setBusiness_type(String business_type) {
            this.business_type = business_type;
        }

        public String getDetail_info() {
            return detail_info;
        }

        public void setDetail_info(String detail_info) {
            this.detail_info = detail_info;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getBusiness_latitude() {
            return business_latitude;
        }

        public void setBusiness_latitude(String business_latitude) {
            this.business_latitude = business_latitude;
        }

        public String getBusiness_longitude() {
            return business_longitude;
        }

        public void setBusiness_longitude(String business_longitude) {
            this.business_longitude = business_longitude;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getBusiness_status() {
            return business_status;
        }

        public void setBusiness_status(String business_status) {
            this.business_status = business_status;
        }

        public String getBusiness_email() {
            return business_email;
        }

        public void setBusiness_email(String business_email) {
            this.business_email = business_email;
        }

        public String getLive_status() {
            return live_status;
        }

        public void setLive_status(String live_status) {
            this.live_status = live_status;
        }

        public String getBc_id() {
            return bc_id;
        }

        public void setBc_id(String bc_id) {
            this.bc_id = bc_id;
        }

        public String getBc_name() {
            return bc_name;
        }

        public void setBc_name(String bc_name) {
            this.bc_name = bc_name;
        }

        public String getBc_image() {
            return bc_image;
        }

        public void setBc_image(String bc_image) {
            this.bc_image = bc_image;
        }

        public String getBsc_id() {
            return bsc_id;
        }

        public void setBsc_id(String bsc_id) {
            this.bsc_id = bsc_id;
        }

        public String getBc_id_fk() {
            return bc_id_fk;
        }

        public void setBc_id_fk(String bc_id_fk) {
            this.bc_id_fk = bc_id_fk;
        }

        public String getBsc_name() {
            return bsc_name;
        }

        public void setBsc_name(String bsc_name) {
            this.bsc_name = bsc_name;
        }

        public String getBsc_image() {
            return bsc_image;
        }

        public void setBsc_image(String bsc_image) {
            this.bsc_image = bsc_image;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(String taluka_id) {
            this.taluka_id = taluka_id;
        }

        public String getTaluka_name() {
            return taluka_name;
        }

        public void setTaluka_name(String taluka_name) {
            this.taluka_name = taluka_name;
        }

        private String bus_id, category_id_fk, subcategory_id_fk, district_id_fk, taluka_id_fk, bus_mobile,bus_name, created_at,
                owner_name, business_address, business_description, business_type, detail_info, licence, photo, business_latitude,
                business_longitude, avrage_rating, no_of_rating, business_status, business_email,live_status, bc_id, bc_name,bc_image,
                bsc_id,bc_id_fk, bsc_name, bsc_image, district_id, district_name, taluka_id, taluka_name;
    }
}
