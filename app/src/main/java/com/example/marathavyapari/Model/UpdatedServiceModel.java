package com.example.marathavyapari.Model;

public class UpdatedServiceModel {



    private String post_id, service_id_fk,post_title,post_description,post_image, updated_at,created_at, is_live;

    public UpdatedServiceModel(String post_id, String service_id_fk, String post_title, String post_description, String post_image, String updated_at, String created_at, String is_live) {
        this.post_id = post_id;
        this.service_id_fk = service_id_fk;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_image = post_image;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.is_live = is_live;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getService_id_fk() {
        return service_id_fk;
    }

    public void setService_id_fk(String service_id_fk) {
        this.service_id_fk = service_id_fk;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_image() {
        return post_image;
    }

    public void setPost_image(String post_image) {
        this.post_image = post_image;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_live() {
        return is_live;
    }

    public void setIs_live(String is_live) {
        this.is_live = is_live;
    }


}
