package com.example.marathavyapari.Model;

public class RulesModelBusiness
{
    String br_id,business_id_fk,rule_title,rule_description,created_at,live_status;

    public RulesModelBusiness(String br_id, String business_id_fk, String rule_title, String rule_description, String created_at, String live_status) {
        this.br_id = br_id;
        this.business_id_fk = business_id_fk;
        this.rule_title = rule_title;
        this.rule_description = rule_description;
        this.created_at = created_at;
        this.live_status = live_status;
    }

    public String getBr_id() {
        return br_id;
    }

    public void setBr_id(String br_id) {
        this.br_id = br_id;
    }

    public String getBusiness_id_fk() {
        return business_id_fk;
    }

    public void setBusiness_id_fk(String business_id_fk) {
        this.business_id_fk = business_id_fk;
    }

    public String getRule_title() {
        return rule_title;
    }

    public void setRule_title(String rule_title) {
        this.rule_title = rule_title;
    }

    public String getRule_description() {
        return rule_description;
    }

    public void setRule_description(String rule_description) {
        this.rule_description = rule_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }
}
