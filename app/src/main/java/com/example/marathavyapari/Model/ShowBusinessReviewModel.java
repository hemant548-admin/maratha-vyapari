package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class ShowBusinessReviewModel {

    private String status, message;
    private ArrayList<ReviewList> review;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ReviewList> getReview() {
        return review;
    }

    public void setReview(ArrayList<ReviewList> review) {
        this.review = review;
    }

    public static class ReviewList{

        private String br_id, review_message, review_date, review_time,uus_mobile, user_email, user_photo, user_name,bus_id,bus_mobile, bus_name,business_description,photo,
                avrage_rating, no_of_rating,business_address;

        public String getUser_photo() {
            return user_photo;
        }

        public void setUser_photo(String user_photo) {
            this.user_photo = user_photo;
        }

        public String getBusiness_address() {
            return business_address;
        }

        public void setBusiness_address(String business_address) {
            this.business_address = business_address;
        }

        public String getBus_id() {
            return bus_id;
        }

        public void setBus_id(String bus_id) {
            this.bus_id = bus_id;
        }

        public String getBus_mobile() {
            return bus_mobile;
        }

        public void setBus_mobile(String bus_mobile) {
            this.bus_mobile = bus_mobile;
        }

        public String getBus_name() {
            return bus_name;
        }

        public void setBus_name(String bus_name) {
            this.bus_name = bus_name;
        }

        public String getBusiness_description() {
            return business_description;
        }

        public void setBusiness_description(String business_description) {
            this.business_description = business_description;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getBr_id() {
            return br_id;
        }

        public void setBr_id(String br_id) {
            this.br_id = br_id;
        }

        public String getReview_message() {
            return review_message;
        }

        public void setReview_message(String review_message) {
            this.review_message = review_message;
        }

        public String getReview_date() {
            return review_date;
        }

        public void setReview_date(String review_date) {
            this.review_date = review_date;
        }

        public String getReview_time() {
            return review_time;
        }

        public void setReview_time(String review_time) {
            this.review_time = review_time;
        }

        public String getUus_mobile() {
            return uus_mobile;
        }

        public void setUus_mobile(String uus_mobile) {
            this.uus_mobile = uus_mobile;
        }

        public String getUser_email() {
            return user_email;
        }

        public void setUser_email(String user_email) {
            this.user_email = user_email;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }
    }
}
