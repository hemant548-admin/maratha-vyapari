package com.example.marathavyapari.Model;

public class ProfileUserModel {

    private String uus_id, uus_mobile, user_email, user_name,district_id_fk,taluka_id_fk,user_latitude,user_longitude,user_photo, updated_at,created_at,
            district_id, district_name,live_status, taluka_id, taluka_name;

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public ProfileUserModel(String uus_id, String uus_mobile, String user_email, String user_name, String district_id_fk, String taluka_id_fk, String user_latitude, String user_longitude, String user_photo, String updated_at, String created_at, String district_id, String district_name, String live_status, String taluka_id, String taluka_name) {
        this.uus_id = uus_id;
        this.uus_mobile = uus_mobile;
        this.user_email = user_email;
        this.user_name = user_name;
        this.district_id_fk = district_id_fk;
        this.taluka_id_fk = taluka_id_fk;
        this.user_latitude = user_latitude;
        this.user_longitude = user_longitude;
        this.user_photo = user_photo;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.district_id = district_id;
        this.district_name = district_name;
        this.live_status = live_status;
        this.taluka_id = taluka_id;
        this.taluka_name = taluka_name;
    }

    public String getUus_id() {
        return uus_id;
    }

    public void setUus_id(String uus_id) {
        this.uus_id = uus_id;
    }

    public String getUus_mobile() {
        return uus_mobile;
    }

    public void setUus_mobile(String uus_mobile) {
        this.uus_mobile = uus_mobile;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDistrict_id_fk() {
        return district_id_fk;
    }

    public void setDistrict_id_fk(String district_id_fk) {
        this.district_id_fk = district_id_fk;
    }

    public String getTaluka_id_fk() {
        return taluka_id_fk;
    }

    public void setTaluka_id_fk(String taluka_id_fk) {
        this.taluka_id_fk = taluka_id_fk;
    }

    public String getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(String user_latitude) {
        this.user_latitude = user_latitude;
    }

    public String getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(String user_longitude) {
        this.user_longitude = user_longitude;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }

    public String getTaluka_id() {
        return taluka_id;
    }

    public void setTaluka_id(String taluka_id) {
        this.taluka_id = taluka_id;
    }

    public String getTaluka_name() {
        return taluka_name;
    }

    public void setTaluka_name(String taluka_name) {
        this.taluka_name = taluka_name;
    }


}
