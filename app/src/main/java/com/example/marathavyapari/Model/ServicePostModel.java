package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class ServicePostModel {

    private String status, message;
    private ArrayList<PostList> post;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PostList> getPost() {
        return post;
    }

    public void setPost(ArrayList<PostList> post) {
        this.post = post;
    }

    public static class PostList{
        private String post_id, service_id_fk, post_title, post_description, post_image, updated_at, created_at, is_live, sus_id,
                category_id_fk, subcategory_id_fk, district_id_fk, taluka_id_fk,sus_mobile, sus_name, name,service_address,
                service_description,service_type, licence,service_latitude, service_longitude, avrage_rating, no_of_rating, service_status,
                post_date, post_time;

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_time() {
            return post_time;
        }

        public void setPost_time(String post_time) {
            this.post_time = post_time;
        }

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getService_id_fk() {
            return service_id_fk;
        }

        public void setService_id_fk(String service_id_fk) {
            this.service_id_fk = service_id_fk;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_description() {
            return post_description;
        }

        public void setPost_description(String post_description) {
            this.post_description = post_description;
        }

        public String getPost_image() {
            return post_image;
        }

        public void setPost_image(String post_image) {
            this.post_image = post_image;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIs_live() {
            return is_live;
        }

        public void setIs_live(String is_live) {
            this.is_live = is_live;
        }

        public String getSus_id() {
            return sus_id;
        }

        public void setSus_id(String sus_id) {
            this.sus_id = sus_id;
        }

        public String getCategory_id_fk() {
            return category_id_fk;
        }

        public void setCategory_id_fk(String category_id_fk) {
            this.category_id_fk = category_id_fk;
        }

        public String getSubcategory_id_fk() {
            return subcategory_id_fk;
        }

        public void setSubcategory_id_fk(String subcategory_id_fk) {
            this.subcategory_id_fk = subcategory_id_fk;
        }

        public String getDistrict_id_fk() {
            return district_id_fk;
        }

        public void setDistrict_id_fk(String district_id_fk) {
            this.district_id_fk = district_id_fk;
        }

        public String getTaluka_id_fk() {
            return taluka_id_fk;
        }

        public void setTaluka_id_fk(String taluka_id_fk) {
            this.taluka_id_fk = taluka_id_fk;
        }

        public String getSus_mobile() {
            return sus_mobile;
        }

        public void setSus_mobile(String sus_mobile) {
            this.sus_mobile = sus_mobile;
        }

        public String getSus_name() {
            return sus_name;
        }

        public void setSus_name(String sus_name) {
            this.sus_name = sus_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getService_address() {
            return service_address;
        }

        public void setService_address(String service_address) {
            this.service_address = service_address;
        }

        public String getService_description() {
            return service_description;
        }

        public void setService_description(String service_description) {
            this.service_description = service_description;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getService_latitude() {
            return service_latitude;
        }

        public void setService_latitude(String service_latitude) {
            this.service_latitude = service_latitude;
        }

        public String getService_longitude() {
            return service_longitude;
        }

        public void setService_longitude(String service_longitude) {
            this.service_longitude = service_longitude;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getService_status() {
            return service_status;
        }

        public void setService_status(String service_status) {
            this.service_status = service_status;
        }
    }
}
