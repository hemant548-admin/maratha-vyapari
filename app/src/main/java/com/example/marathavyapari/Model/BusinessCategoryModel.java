package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class BusinessCategoryModel {
    private String status, message;
    private ArrayList<CategoryList> category;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoryList> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<CategoryList> category) {
        this.category = category;
    }

    public static class CategoryList{

        private String bc_id, bc_name, bc_image;

        public String getBc_id() {
            return bc_id;
        }

        public void setBc_id(String bc_id) {
            this.bc_id = bc_id;
        }

        public String getBc_name() {
            return bc_name;
        }

        public void setBc_name(String bc_name) {
            this.bc_name = bc_name;
        }

        public String getBc_image() {
            return bc_image;
        }

        public void setBc_image(String bc_image) {
            this.bc_image = bc_image;
        }
    }

}
