package com.example.marathavyapari.Model;

public class UserProfileModel {

    private String token, uus_id, uus_mobile, user_email, user_name,user_photo, district_name, taluka_name;

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public UserProfileModel(String token, String uus_id, String uus_mobile, String user_email, String user_name, String user_photo, String district_name, String taluka_name) {
        this.token = token;
        this.uus_id = uus_id;
        this.uus_mobile = uus_mobile;
        this.user_email = user_email;
        this.user_name = user_name;
        this.user_photo = user_photo;
        this.district_name = district_name;
        this.taluka_name = taluka_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUus_id() {
        return uus_id;
    }

    public void setUus_id(String uus_id) {
        this.uus_id = uus_id;
    }

    public String getUus_mobile() {
        return uus_mobile;
    }

    public void setUus_mobile(String uus_mobile) {
        this.uus_mobile = uus_mobile;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getTaluka_name() {
        return taluka_name;
    }

    public void setTaluka_name(String taluka_name) {
        this.taluka_name = taluka_name;
    }





}
