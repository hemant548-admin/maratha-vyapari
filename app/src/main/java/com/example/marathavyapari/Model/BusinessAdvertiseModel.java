package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class BusinessAdvertiseModel {

    private String status, message;
    private ArrayList<AdvertiseList> advs;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AdvertiseList> getAdvs() {
        return advs;
    }

    public void setAdvs(ArrayList<AdvertiseList> advs) {
        this.advs = advs;
    }

    public static class AdvertiseList{
        private String adv_id, adv_type,bus_cat_id,bus_subcat_id,ser_cat_id, ser_subcat_id, adv_title,adv_description,adv_image;

        public String getAdv_image() {
            return adv_image;
        }

        public void setAdv_image(String adv_image) {
            this.adv_image = adv_image;
        }

        public String getAdv_id() {
            return adv_id;
        }

        public void setAdv_id(String adv_id) {
            this.adv_id = adv_id;
        }

        public String getAdv_type() {
            return adv_type;
        }

        public void setAdv_type(String adv_type) {
            this.adv_type = adv_type;
        }

        public String getBus_cat_id() {
            return bus_cat_id;
        }

        public void setBus_cat_id(String bus_cat_id) {
            this.bus_cat_id = bus_cat_id;
        }

        public String getBus_subcat_id() {
            return bus_subcat_id;
        }

        public void setBus_subcat_id(String bus_subcat_id) {
            this.bus_subcat_id = bus_subcat_id;
        }

        public String getSer_cat_id() {
            return ser_cat_id;
        }

        public void setSer_cat_id(String ser_cat_id) {
            this.ser_cat_id = ser_cat_id;
        }

        public String getSer_subcat_id() {
            return ser_subcat_id;
        }

        public void setSer_subcat_id(String ser_subcat_id) {
            this.ser_subcat_id = ser_subcat_id;
        }

        public String getAdv_title() {
            return adv_title;
        }

        public void setAdv_title(String adv_title) {
            this.adv_title = adv_title;
        }

        public String getAdv_description() {
            return adv_description;
        }

        public void setAdv_description(String adv_description) {
            this.adv_description = adv_description;
        }
    }
}
