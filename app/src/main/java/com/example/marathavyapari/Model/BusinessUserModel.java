package com.example.marathavyapari.Model;

public class BusinessUserModel
{
    String token,bus_id,category_id_fk,subcategory_id_fk,bus_mobile,bus_name,owner_name,business_address,business_description,
            business_type,detail_info,licence,photo,business_status,business_email,bc_name,live_status,bsc_name;

    public BusinessUserModel(String token, String bus_id, String category_id_fk, String subcategory_id_fk, String bus_mobile, String bus_name, String owner_name, String business_address, String business_description, String business_type, String detail_info, String licence, String photo, String business_status, String business_email, String bc_name, String live_status, String bsc_name) {
        this.token = token;
        this.bus_id = bus_id;
        this.category_id_fk = category_id_fk;
        this.subcategory_id_fk = subcategory_id_fk;
        this.bus_mobile = bus_mobile;
        this.bus_name = bus_name;
        this.owner_name = owner_name;
        this.business_address = business_address;
        this.business_description = business_description;
        this.business_type = business_type;
        this.detail_info = detail_info;
        this.licence = licence;
        this.photo = photo;
        this.business_status = business_status;
        this.business_email = business_email;
        this.bc_name = bc_name;
        this.live_status = live_status;
        this.bsc_name = bsc_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBus_id() {
        return bus_id;
    }

    public void setBus_id(String bus_id) {
        this.bus_id = bus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getSubcategory_id_fk() {
        return subcategory_id_fk;
    }

    public void setSubcategory_id_fk(String subcategory_id_fk) {
        this.subcategory_id_fk = subcategory_id_fk;
    }

    public String getBus_mobile() {
        return bus_mobile;
    }

    public void setBus_mobile(String bus_mobile) {
        this.bus_mobile = bus_mobile;
    }

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getBusiness_description() {
        return business_description;
    }

    public void setBusiness_description(String business_description) {
        this.business_description = business_description;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getDetail_info() {
        return detail_info;
    }

    public void setDetail_info(String detail_info) {
        this.detail_info = detail_info;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBusiness_status() {
        return business_status;
    }

    public void setBusiness_status(String business_status) {
        this.business_status = business_status;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getBc_name() {
        return bc_name;
    }

    public void setBc_name(String bc_name) {
        this.bc_name = bc_name;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }

    public String getBsc_name() {
        return bsc_name;
    }

    public void setBsc_name(String bsc_name) {
        this.bsc_name = bsc_name;
    }
}
