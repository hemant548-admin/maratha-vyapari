package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class TalukaDataModel {


    private String status;
    private String message;
    private ArrayList<TalukaList> taluka;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TalukaList> getTaluka() {
        return taluka;
    }

    public void setTaluka(ArrayList<TalukaList> taluka) {
        this.taluka = taluka;
    }

    public static class TalukaList{
        private String taluka_id;
        private String district_id_fk;
        private String taluka_name;
        private String created_at;
        private String live_status;

        public String getDistrict_id_fk() {
            return district_id_fk;
        }

        public void setDistrict_id_fk(String district_id_fk) {
            this.district_id_fk = district_id_fk;
        }

        public String getTaluka_name() {
            return taluka_name;
        }

        public void setTaluka_name(String taluka_name) {
            this.taluka_name = taluka_name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getLive_status() {
            return live_status;
        }

        public void setLive_status(String live_status) {
            this.live_status = live_status;
        }

        public String getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(String taluka_id) {
            this.taluka_id = taluka_id;
        }
    }

}
