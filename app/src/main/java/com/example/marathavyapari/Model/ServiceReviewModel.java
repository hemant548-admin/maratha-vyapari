package com.example.marathavyapari.Model;

public class ServiceReviewModel {

    private String sr_id, user_id_fk, service_id_fk, review_message, review_date, review_time, sus_id, category_id_fk, sus_mobile, sus_name, name,
            service_address, service_description, service_type, licence, service_latitude, service_longitude, avrage_rating, no_of_rating, uus_id,
            uus_mobile, user_email, user_name,user_photo;

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public ServiceReviewModel(String sr_id, String user_id_fk, String service_id_fk, String review_message, String review_date, String review_time, String sus_id, String category_id_fk, String sus_mobile, String sus_name, String name, String service_address, String service_description, String service_type, String licence, String service_latitude, String service_longitude, String avrage_rating, String no_of_rating, String uus_id, String uus_mobile, String user_email, String user_name, String user_photo) {
        this.sr_id = sr_id;
        this.user_id_fk = user_id_fk;
        this.service_id_fk = service_id_fk;
        this.review_message = review_message;
        this.review_date = review_date;
        this.review_time = review_time;
        this.sus_id = sus_id;
        this.category_id_fk = category_id_fk;
        this.sus_mobile = sus_mobile;
        this.sus_name = sus_name;
        this.name = name;
        this.service_address = service_address;
        this.service_description = service_description;
        this.service_type = service_type;
        this.licence = licence;
        this.service_latitude = service_latitude;
        this.service_longitude = service_longitude;
        this.avrage_rating = avrage_rating;
        this.no_of_rating = no_of_rating;
        this.uus_id = uus_id;
        this.uus_mobile = uus_mobile;
        this.user_email = user_email;
        this.user_name = user_name;
        this.user_photo = user_photo;
    }

    public String getSr_id() {
        return sr_id;
    }

    public void setSr_id(String sr_id) {
        this.sr_id = sr_id;
    }

    public String getUser_id_fk() {
        return user_id_fk;
    }

    public void setUser_id_fk(String user_id_fk) {
        this.user_id_fk = user_id_fk;
    }

    public String getService_id_fk() {
        return service_id_fk;
    }

    public void setService_id_fk(String service_id_fk) {
        this.service_id_fk = service_id_fk;
    }

    public String getReview_message() {
        return review_message;
    }

    public void setReview_message(String review_message) {
        this.review_message = review_message;
    }

    public String getReview_date() {
        return review_date;
    }

    public void setReview_date(String review_date) {
        this.review_date = review_date;
    }

    public String getReview_time() {
        return review_time;
    }

    public void setReview_time(String review_time) {
        this.review_time = review_time;
    }

    public String getSus_id() {
        return sus_id;
    }

    public void setSus_id(String sus_id) {
        this.sus_id = sus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getSus_mobile() {
        return sus_mobile;
    }

    public void setSus_mobile(String sus_mobile) {
        this.sus_mobile = sus_mobile;
    }

    public String getSus_name() {
        return sus_name;
    }

    public void setSus_name(String sus_name) {
        this.sus_name = sus_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_address() {
        return service_address;
    }

    public void setService_address(String service_address) {
        this.service_address = service_address;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getService_latitude() {
        return service_latitude;
    }

    public void setService_latitude(String service_latitude) {
        this.service_latitude = service_latitude;
    }

    public String getService_longitude() {
        return service_longitude;
    }

    public void setService_longitude(String service_longitude) {
        this.service_longitude = service_longitude;
    }

    public String getAvrage_rating() {
        return avrage_rating;
    }

    public void setAvrage_rating(String avrage_rating) {
        this.avrage_rating = avrage_rating;
    }

    public String getNo_of_rating() {
        return no_of_rating;
    }

    public void setNo_of_rating(String no_of_rating) {
        this.no_of_rating = no_of_rating;
    }

    public String getUus_id() {
        return uus_id;
    }

    public void setUus_id(String uus_id) {
        this.uus_id = uus_id;
    }

    public String getUus_mobile() {
        return uus_mobile;
    }

    public void setUus_mobile(String uus_mobile) {
        this.uus_mobile = uus_mobile;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

}
