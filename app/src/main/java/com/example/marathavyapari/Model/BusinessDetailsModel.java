package com.example.marathavyapari.Model;

public class BusinessDetailsModel {

    private String type, post_id, business_id_fk, post_title, post_description, post_image, post_date, post_time, bus_id, category_id_fk,
            bus_mobile, bus_name, owner_name, business_address, business_description, business_type, detail_info, licence, photo, business_latitude,
            business_longitude, avrage_rating, no_of_rating, business_email;


    public BusinessDetailsModel(String type, String post_id, String business_id_fk, String post_title, String post_description, String post_image, String post_date, String post_time, String bus_id, String category_id_fk, String bus_mobile, String bus_name, String owner_name, String business_address, String business_description, String business_type, String detail_info, String licence, String photo, String business_latitude, String business_longitude, String avrage_rating, String no_of_rating, String business_email) {
        this.type = type;
        this.post_id = post_id;
        this.business_id_fk = business_id_fk;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_image = post_image;
        this.post_date = post_date;
        this.post_time = post_time;
        this.bus_id = bus_id;
        this.category_id_fk = category_id_fk;
        this.bus_mobile = bus_mobile;
        this.bus_name = bus_name;
        this.owner_name = owner_name;
        this.business_address = business_address;
        this.business_description = business_description;
        this.business_type = business_type;
        this.detail_info = detail_info;
        this.licence = licence;
        this.photo = photo;
        this.business_latitude = business_latitude;
        this.business_longitude = business_longitude;
        this.avrage_rating = avrage_rating;
        this.no_of_rating = no_of_rating;
        this.business_email = business_email;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getBusiness_id_fk() {
        return business_id_fk;
    }

    public void setBusiness_id_fk(String business_id_fk) {
        this.business_id_fk = business_id_fk;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_image() {
        return post_image;
    }

    public void setPost_image(String post_image) {
        this.post_image = post_image;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getBus_id() {
        return bus_id;
    }

    public void setBus_id(String bus_id) {
        this.bus_id = bus_id;
    }

    public String getCategory_id_fk() {
        return category_id_fk;
    }

    public void setCategory_id_fk(String category_id_fk) {
        this.category_id_fk = category_id_fk;
    }

    public String getBus_mobile() {
        return bus_mobile;
    }

    public void setBus_mobile(String bus_mobile) {
        this.bus_mobile = bus_mobile;
    }

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getBusiness_description() {
        return business_description;
    }

    public void setBusiness_description(String business_description) {
        this.business_description = business_description;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getDetail_info() {
        return detail_info;
    }

    public void setDetail_info(String detail_info) {
        this.detail_info = detail_info;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBusiness_latitude() {
        return business_latitude;
    }

    public void setBusiness_latitude(String business_latitude) {
        this.business_latitude = business_latitude;
    }

    public String getBusiness_longitude() {
        return business_longitude;
    }

    public void setBusiness_longitude(String business_longitude) {
        this.business_longitude = business_longitude;
    }

    public String getAvrage_rating() {
        return avrage_rating;
    }

    public void setAvrage_rating(String avrage_rating) {
        this.avrage_rating = avrage_rating;
    }

    public String getNo_of_rating() {
        return no_of_rating;
    }

    public void setNo_of_rating(String no_of_rating) {
        this.no_of_rating = no_of_rating;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }
}
