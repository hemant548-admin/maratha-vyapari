package com.example.marathavyapari.Model;

public class MyPostModel
{
    String post_id,business_id_fk,post_title,post_description,post_date, post_time,post_image,updated_at,created_at,is_live;

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public MyPostModel(String post_id, String business_id_fk, String post_title, String post_description,String post_date, String post_time,  String post_image, String updated_at, String created_at, String is_live) {
        this.post_id = post_id;
        this.business_id_fk = business_id_fk;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_image = post_image;
        this.post_date = post_date;
        this.post_time = post_time;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.is_live = is_live;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getBusiness_id_fk() {
        return business_id_fk;
    }

    public void setBusiness_id_fk(String business_id_fk) {
        this.business_id_fk = business_id_fk;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_image() {
        return post_image;
    }

    public void setPost_image(String post_image) {
        this.post_image = post_image;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_live() {
        return is_live;
    }

    public void setIs_live(String is_live) {
        this.is_live = is_live;
    }
}
