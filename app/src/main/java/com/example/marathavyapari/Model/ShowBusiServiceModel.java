package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class ShowBusiServiceModel {

    private String status, message;
    private ArrayList<BusinessServicePost> post;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<BusinessServicePost> getPost() {
        return post;
    }

    public void setPost(ArrayList<BusinessServicePost> post) {
        this.post = post;
    }

    public static class BusinessServicePost{
        private String type, post_id, business_id_fk, post_title, post_description, post_image, post_date, post_time, bus_id, bus_mobile, bus_name,
                owner_name, business_address,business_description, business_type, detail_info, licence, photo,avrage_rating, no_of_rating, business_status,business_email,
                service_id_fk, sus_id, category_id_fk, subcategory_id_fk, sus_mobile, sus_name, name, service_address,
                service_description, service_type,service_status;

        public String getBusiness_description() {
            return business_description;
        }

        public void setBusiness_description(String business_description) {
            this.business_description = business_description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getBusiness_id_fk() {
            return business_id_fk;
        }

        public void setBusiness_id_fk(String business_id_fk) {
            this.business_id_fk = business_id_fk;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_description() {
            return post_description;
        }

        public void setPost_description(String post_description) {
            this.post_description = post_description;
        }

        public String getPost_image() {
            return post_image;
        }

        public void setPost_image(String post_image) {
            this.post_image = post_image;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_time() {
            return post_time;
        }

        public void setPost_time(String post_time) {
            this.post_time = post_time;
        }

        public String getBus_id() {
            return bus_id;
        }

        public void setBus_id(String bus_id) {
            this.bus_id = bus_id;
        }

        public String getBus_mobile() {
            return bus_mobile;
        }

        public void setBus_mobile(String bus_mobile) {
            this.bus_mobile = bus_mobile;
        }

        public String getBus_name() {
            return bus_name;
        }

        public void setBus_name(String bus_name) {
            this.bus_name = bus_name;
        }

        public String getOwner_name() {
            return owner_name;
        }

        public void setOwner_name(String owner_name) {
            this.owner_name = owner_name;
        }

        public String getBusiness_address() {
            return business_address;
        }

        public void setBusiness_address(String business_address) {
            this.business_address = business_address;
        }

        public String getBusiness_type() {
            return business_type;
        }

        public void setBusiness_type(String business_type) {
            this.business_type = business_type;
        }

        public String getDetail_info() {
            return detail_info;
        }

        public void setDetail_info(String detail_info) {
            this.detail_info = detail_info;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getBusiness_status() {
            return business_status;
        }

        public void setBusiness_status(String business_status) {
            this.business_status = business_status;
        }

        public String getBusiness_email() {
            return business_email;
        }

        public void setBusiness_email(String business_email) {
            this.business_email = business_email;
        }

        public String getService_id_fk() {
            return service_id_fk;
        }

        public void setService_id_fk(String service_id_fk) {
            this.service_id_fk = service_id_fk;
        }

        public String getSus_id() {
            return sus_id;
        }

        public void setSus_id(String sus_id) {
            this.sus_id = sus_id;
        }

        public String getCategory_id_fk() {
            return category_id_fk;
        }

        public void setCategory_id_fk(String category_id_fk) {
            this.category_id_fk = category_id_fk;
        }

        public String getSubcategory_id_fk() {
            return subcategory_id_fk;
        }

        public void setSubcategory_id_fk(String subcategory_id_fk) {
            this.subcategory_id_fk = subcategory_id_fk;
        }

        public String getSus_mobile() {
            return sus_mobile;
        }

        public void setSus_mobile(String sus_mobile) {
            this.sus_mobile = sus_mobile;
        }

        public String getSus_name() {
            return sus_name;
        }

        public void setSus_name(String sus_name) {
            this.sus_name = sus_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getService_address() {
            return service_address;
        }

        public void setService_address(String service_address) {
            this.service_address = service_address;
        }

        public String getService_description() {
            return service_description;
        }

        public void setService_description(String service_description) {
            this.service_description = service_description;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getService_status() {
            return service_status;
        }

        public void setService_status(String service_status) {
            this.service_status = service_status;
        }
    }
}
