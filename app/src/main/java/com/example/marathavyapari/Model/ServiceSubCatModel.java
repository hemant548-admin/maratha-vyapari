package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class ServiceSubCatModel {

    private boolean status;
    private String message;
    private ArrayList<SubCategoryList> sub_category;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SubCategoryList> getSub_category() {
        return sub_category;
    }

    public void setSub_category(ArrayList<SubCategoryList> sub_category) {
        this.sub_category = sub_category;
    }

    public static class SubCategoryList{

             private String ssc_id;
        private String sc_id_fk;
        private String ssc_name;
         private String ssc_image;

        public String getSsc_id() {
            return ssc_id;
        }

        public void setSsc_id(String ssc_id) {
            this.ssc_id = ssc_id;
        }

        public String getSc_id_fk() {
            return sc_id_fk;
        }

        public void setSc_id_fk(String sc_id_fk) {
            this.sc_id_fk = sc_id_fk;
        }

        public String getSsc_name() {
            return ssc_name;
        }

        public void setSsc_name(String ssc_name) {
            this.ssc_name = ssc_name;
        }

        public String getSsc_image() {
            return ssc_image;
        }

        public void setSsc_image(String ssc_image) {
            this.ssc_image = ssc_image;
        }
    }
}
