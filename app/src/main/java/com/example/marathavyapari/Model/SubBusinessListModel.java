package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class SubBusinessListModel {


    private String status;
    private String message;
    private ArrayList<SubBusinessCategoryList> sub_category;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SubBusinessCategoryList> getSub_category() {
        return sub_category;
    }

    public void setSub_category(ArrayList<SubBusinessCategoryList> sub_category) {
        this.sub_category = sub_category;
    }

    public static class SubBusinessCategoryList{

    private String bsc_id, bc_id_fk, bsc_name, bsc_image, created_at, live_status;

    public String getBsc_id() {
        return bsc_id;
    }

    public void setBsc_id(String bsc_id) {
        this.bsc_id = bsc_id;
    }

    public String getBc_id_fk() {
        return bc_id_fk;
    }

    public void setBc_id_fk(String bc_id_fk) {
        this.bc_id_fk = bc_id_fk;
    }

    public String getBsc_name() {
        return bsc_name;
    }

    public void setBsc_name(String bsc_name) {
        this.bsc_name = bsc_name;
    }

    public String getBsc_image() {
        return bsc_image;
    }

    public void setBsc_image(String bsc_image) {
        this.bsc_image = bsc_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }
    }

}
