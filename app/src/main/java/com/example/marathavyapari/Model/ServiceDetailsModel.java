package com.example.marathavyapari.Model;

public class ServiceDetailsModel {

    private String type, post_id, post_title, post_description, post_image, post_date,post_time, sus_id, sus_mobile, sus_name, name,service_address,
            service_description, service_type, licence, service_latitude, service_longitude,avrage_rating, no_of_rating;

    public ServiceDetailsModel(String type, String post_id, String post_title, String post_description, String post_image, String post_date, String post_time, String sus_id, String sus_mobile, String sus_name, String name, String service_address, String service_description, String service_type, String licence, String service_latitude, String service_longitude, String avrage_rating, String no_of_rating) {
        this.type = type;
        this.post_id = post_id;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_image = post_image;
        this.post_date = post_date;
        this.post_time = post_time;
        this.sus_id = sus_id;
        this.sus_mobile = sus_mobile;
        this.sus_name = sus_name;
        this.name = name;
        this.service_address = service_address;
        this.service_description = service_description;
        this.service_type = service_type;
        this.licence = licence;
        this.service_latitude = service_latitude;
        this.service_longitude = service_longitude;
        this.avrage_rating = avrage_rating;
        this.no_of_rating = no_of_rating;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_image() {
        return post_image;
    }

    public void setPost_image(String post_image) {
        this.post_image = post_image;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getSus_id() {
        return sus_id;
    }

    public void setSus_id(String sus_id) {
        this.sus_id = sus_id;
    }

    public String getSus_mobile() {
        return sus_mobile;
    }

    public void setSus_mobile(String sus_mobile) {
        this.sus_mobile = sus_mobile;
    }

    public String getSus_name() {
        return sus_name;
    }

    public void setSus_name(String sus_name) {
        this.sus_name = sus_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_address() {
        return service_address;
    }

    public void setService_address(String service_address) {
        this.service_address = service_address;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getService_latitude() {
        return service_latitude;
    }

    public void setService_latitude(String service_latitude) {
        this.service_latitude = service_latitude;
    }

    public String getService_longitude() {
        return service_longitude;
    }

    public void setService_longitude(String service_longitude) {
        this.service_longitude = service_longitude;
    }

    public String getAvrage_rating() {
        return avrage_rating;
    }

    public void setAvrage_rating(String avrage_rating) {
        this.avrage_rating = avrage_rating;
    }

    public String getNo_of_rating() {
        return no_of_rating;
    }

    public void setNo_of_rating(String no_of_rating) {
        this.no_of_rating = no_of_rating;
    }
}
