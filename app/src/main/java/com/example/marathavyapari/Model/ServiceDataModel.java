package com.example.marathavyapari.Model;

public class ServiceDataModel {

    private String sr_id, service_id_fk, rule_title, rule_description, created_at, live_status;

    public ServiceDataModel(String sr_id, String service_id_fk, String rule_title, String rule_description, String created_at, String live_status) {
        this.sr_id = sr_id;
        this.service_id_fk = service_id_fk;
        this.rule_title = rule_title;
        this.rule_description = rule_description;
        this.created_at = created_at;
        this.live_status = live_status;
    }

    public String getSr_id() {
        return sr_id;
    }

    public void setSr_id(String sr_id) {
        this.sr_id = sr_id;
    }

    public String getService_id_fk() {
        return service_id_fk;
    }

    public void setService_id_fk(String service_id_fk) {
        this.service_id_fk = service_id_fk;
    }

    public String getRule_title() {
        return rule_title;
    }

    public void setRule_title(String rule_title) {
        this.rule_title = rule_title;
    }

    public String getRule_description() {
        return rule_description;
    }

    public void setRule_description(String rule_description) {
        this.rule_description = rule_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }


}
