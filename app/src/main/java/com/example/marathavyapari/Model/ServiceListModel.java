package com.example.marathavyapari.Model;

public class ServiceListModel {

    private String sc_id, sc_name, sc_image, created_at, live_status;

    public ServiceListModel(String sc_id, String sc_name, String sc_image, String created_at, String live_status) {
        this.sc_id = sc_id;
        this.sc_name = sc_name;
        this.sc_image = sc_image;
        this.created_at = created_at;
        this.live_status = live_status;
    }

    public String getSc_id() {
        return sc_id;
    }

    public void setSc_id(String sc_id) {
        this.sc_id = sc_id;
    }

    public String getSc_name() {
        return sc_name;
    }

    public void setSc_name(String sc_name) {
        this.sc_name = sc_name;
    }

    public String getSc_image() {
        return sc_image;
    }

    public void setSc_image(String sc_image) {
        this.sc_image = sc_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }



}
