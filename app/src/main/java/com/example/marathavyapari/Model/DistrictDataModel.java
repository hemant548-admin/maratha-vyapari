package com.example.marathavyapari.Model;

public class DistrictDataModel {

    private String district_id, district_name, created_at, live_status;

    public DistrictDataModel(String district_id, String district_name, String created_at, String live_status) {
        this.district_id = district_id;
        this.district_name = district_name;
        this.created_at = created_at;
        this.live_status = live_status;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }


}
