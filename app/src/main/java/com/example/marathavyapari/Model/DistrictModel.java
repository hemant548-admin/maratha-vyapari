package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class DistrictModel {

    private String status, message;
    private ArrayList<DistrictsList> districts;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DistrictsList> getDistricts() {
        return districts;
    }

    public void setDistricts(ArrayList<DistrictsList> districts) {
        this.districts = districts;
    }

    public static class DistrictsList{
        private String district_id,district_name;

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }
    }
}
