package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class UserNotificationModel {

    private String status, message, count;
    private ArrayList<NotificationList> notification;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<NotificationList> getNotification() {
        return notification;
    }

    public void setNotification(ArrayList<NotificationList> notification) {
        this.notification = notification;
    }

    public static class NotificationList{
        private String notify_id, user_id_fk, business_id_fk, service_id_fk, notify_title, notify_message, notify_date,
                notify_time;

        public String getNotify_id() {
            return notify_id;
        }

        public void setNotify_id(String notify_id) {
            this.notify_id = notify_id;
        }

        public String getUser_id_fk() {
            return user_id_fk;
        }

        public void setUser_id_fk(String user_id_fk) {
            this.user_id_fk = user_id_fk;
        }

        public String getBusiness_id_fk() {
            return business_id_fk;
        }

        public void setBusiness_id_fk(String business_id_fk) {
            this.business_id_fk = business_id_fk;
        }

        public String getService_id_fk() {
            return service_id_fk;
        }

        public void setService_id_fk(String service_id_fk) {
            this.service_id_fk = service_id_fk;
        }

        public String getNotify_title() {
            return notify_title;
        }

        public void setNotify_title(String notify_title) {
            this.notify_title = notify_title;
        }

        public String getNotify_message() {
            return notify_message;
        }

        public void setNotify_message(String notify_message) {
            this.notify_message = notify_message;
        }

        public String getNotify_date() {
            return notify_date;
        }

        public void setNotify_date(String notify_date) {
            this.notify_date = notify_date;
        }

        public String getNotify_time() {
            return notify_time;
        }

        public void setNotify_time(String notify_time) {
            this.notify_time = notify_time;
        }
    }

}
