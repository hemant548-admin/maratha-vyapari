package com.example.marathavyapari.Model;

import java.util.ArrayList;

public class ServiceCategoryModel {
    private String status, message;
    private ArrayList<CategoryList> category;

    public ArrayList<CategoryList> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<CategoryList> category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public static class CategoryList{

        private String sc_id, sc_name, sc_image;

        public String getSc_id() {
            return sc_id;
        }

        public void setSc_id(String sc_id) {
            this.sc_id = sc_id;
        }

        public String getSc_name() {
            return sc_name;
        }

        public void setSc_name(String sc_name) {
            this.sc_name = sc_name;
        }

        public String getSc_image() {
            return sc_image;
        }

        public void setSc_image(String sc_image) {
            this.sc_image = sc_image;
        }
    }

}
