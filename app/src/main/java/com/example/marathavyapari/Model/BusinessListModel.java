package com.example.marathavyapari.Model;

public class BusinessListModel {

    private String bc_id, bc_name, bc_image, created_at, live_status;

    public BusinessListModel(String bc_id, String bc_name, String bc_image, String created_at, String live_status) {
        this.bc_id = bc_id;
        this.bc_name = bc_name;
        this.bc_image = bc_image;
        this.created_at = created_at;
        this.live_status = live_status;
    }


    public String getBc_id() {
        return bc_id;
    }

    public void setBc_id(String bc_id) {
        this.bc_id = bc_id;
    }

    public String getBc_name() {
        return bc_name;
    }

    public void setBc_name(String bc_name) {
        this.bc_name = bc_name;
    }

    public String getBc_image() {
        return bc_image;
    }

    public void setBc_image(String bc_image) {
        this.bc_image = bc_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }


}
