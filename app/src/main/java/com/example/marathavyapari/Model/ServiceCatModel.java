package com.example.marathavyapari.Model;

public class ServiceCatModel {

    private String sc_id;
    private String sc_name;
    private String created_at;
    private String live_status;

    public ServiceCatModel(String sc_id, String sc_name, String created_at, String live_status)
    {
        this.sc_id = sc_id;
        this.sc_name  = sc_name;
        this.created_at = created_at;
        this.live_status = live_status;
    }
    public String getSc_id() {
        return sc_id;
    }

    public void setSc_id(String sc_id) {
        this.sc_id = sc_id;
    }

    public String getSc_name() {
        return sc_name;
    }

    public void setSc_name(String sc_name) {
        this.sc_name = sc_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }
}
