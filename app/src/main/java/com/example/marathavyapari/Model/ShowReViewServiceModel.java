package com.example.marathavyapari.Model;

import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class ShowReViewServiceModel {
    private ArrayList<ServiceList> service;

    public ArrayList<ServiceList> getService() {
        return service;
    }

    public void setService(ArrayList<ServiceList> service) {
        this.service = service;
    }

    public static class ServiceList{

    private String sr_id, review_message, review_date, review_time, user_email, uus_mobile,user_name,sus_mobile, sus_name, service_description, service_address,
            licence, avrage_rating, no_of_rating;

        public String getSus_mobile() {
            return sus_mobile;
        }

        public void setSus_mobile(String sus_mobile) {
            this.sus_mobile = sus_mobile;
        }

        public String getSus_name() {
            return sus_name;
        }

        public void setSus_name(String sus_name) {
            this.sus_name = sus_name;
        }

        public String getService_description() {
            return service_description;
        }

        public void setService_description(String service_description) {
            this.service_description = service_description;
        }

        public String getService_address() {
            return service_address;
        }

        public void setService_address(String service_address) {
            this.service_address = service_address;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getAvrage_rating() {
            return avrage_rating;
        }

        public void setAvrage_rating(String avrage_rating) {
            this.avrage_rating = avrage_rating;
        }

        public String getNo_of_rating() {
            return no_of_rating;
        }

        public void setNo_of_rating(String no_of_rating) {
            this.no_of_rating = no_of_rating;
        }

        public String getSr_id() {
        return sr_id;
    }

    public void setSr_id(String sr_id) {
        this.sr_id = sr_id;
    }

    public String getReview_message() {
        return review_message;
    }

    public void setReview_message(String review_message) {
        this.review_message = review_message;
    }

    public String getReview_date() {
        return review_date;
    }

    public void setReview_date(String review_date) {
        this.review_date = review_date;
    }

    public String getReview_time() {
        return review_time;
    }

    public void setReview_time(String review_time) {
        this.review_time = review_time;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUus_mobile() {
        return uus_mobile;
    }

    public void setUus_mobile(String uus_mobile) {
        this.uus_mobile = uus_mobile;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    }
}
