package com.example.marathavyapari;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.API.ApiInterface;
import com.example.marathavyapari.Activity.AddPostBusiness;
import com.example.marathavyapari.Activity.BusinessSettings;
import com.example.marathavyapari.Activity.ChangePassword;
import com.example.marathavyapari.Activity.ContactUs;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.MyReviews;
import com.example.marathavyapari.Activity.MyShop;
import com.example.marathavyapari.Activity.Notifications;
import com.example.marathavyapari.Activity.PrivacyPolicy;
import com.example.marathavyapari.Activity.SelectLanguage;
import com.example.marathavyapari.Activity.TermsAndConditions;
import com.example.marathavyapari.Activity.UploadBusinessDoc;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.ShowNotifyAdapter;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ReadNotiModel;
import com.example.marathavyapari.Model.ResponseFCMModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.Model.UnreadNotificationModel;
import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceMainActivity;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_UserNotification;
import com.example.marathavyapari.User.UserMainActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity
{
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    LinearLayout mainLayout,addPost;
    TextView shopName,shopAddress,shopNumber;
    CircleImageView userPhoto;
    String fcmregistrationId,businessId;
    List<SliderItem> imagelist;
    ApiClient apiClient;

    LinearLayout myShop,Privacy,Terms,Notifications,MyPost,MyReviews;
    SharedPrefManager sharedPrefManager;
    SliderView sliderView;
    private TextView notifCount;
    private String mNotifCount, lang, latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private static final int REQUEST_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpToolbar();

        addPost = findViewById(R.id.add_post_home);
        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        businessId = userModel.getBus_id();
        apiClient = new ApiClient();

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmregistrationId = instanceIdResult.getToken();
                Log.d("TUSHARFCMREGISTRATION", fcmregistrationId);

               getSaveData(businessId, fcmregistrationId, lang, latitude, longitude);
            }
        });
        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent post = new Intent(MainActivity.this,AddPostBusiness.class);
                startActivity(post);
            }
        });

        imagelist = new ArrayList<>();

        loadBanners();
        sharedPrefManager = new SharedPrefManager(MainActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", MainActivity.this);

        navigationView = findViewById(R.id.main_navigationview);
        //navigationView.setCheckedItem(R.id.home);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {

                    case R.id.my_shop:

                        Intent myshop = new Intent(MainActivity.this, MyShop.class);
                        startActivity(myshop);


                        break;

                    case R.id.add_image:

                        Intent images = new Intent(MainActivity.this, UploadShopImages.class);
                        startActivity(images);


                        break;

                    case R.id.contactus:

                        Intent contact = new Intent(MainActivity.this, ContactUs.class);
                        startActivity(contact);


                        break;

                    case R.id.rules:

                        Intent rules = new Intent(MainActivity.this, TermsAndConditions.class);
                        startActivity(rules);

                        break;

                    case R.id.notifications:

                        Intent noti = new Intent(MainActivity.this, Notifications.class);
                        startActivity(noti);

                        break;

                    case R.id.my_review:

                        Intent review = new Intent(MainActivity.this, MyReviews.class);
                        startActivity(review);

                        break;

                    case R.id.privacy:

                        Intent privacyy = new Intent(MainActivity.this, PrivacyPolicy.class);
                        startActivity(privacyy);

                        break;

                    case R.id.my_post:

                        Intent post = new Intent(MainActivity.this, MyPosts.class);
                        startActivity(post);

                        break;

                    case R.id.settings:

                        Intent settings = new Intent(MainActivity.this, BusinessSettings.class);
                        startActivity(settings);

                        break;

                    case R.id.rateus:
                        reviewOnApp();

                        break;

                    case R.id.logout:
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Logout") .setMessage("Do you really want to Logout?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                                        { public void onClick(DialogInterface dialog, int which)
                                        {
                                            SharedPrefManager.getInstance(MainActivity.this).logout();
                                            Intent intent = new Intent(MainActivity.this, SelectLanguage.class);
                                            intent.putExtra("account_type","business");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                            Toast.makeText(MainActivity.this, "Logout successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                        }
                                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss(); } })
                                .setIcon(android.R.drawable.ic_dialog_alert) .show();
                }


                return false;
            }
        });

        View headerView = navigationView.getHeaderView(0);
        shopName = headerView.findViewById(R.id.user_businessname);
        shopAddress = headerView.findViewById(R.id.user_address);
        shopNumber = headerView.findViewById(R.id.user_contact);
        userPhoto = headerView.findViewById(R.id.profile_image);


        shopName.setText(userModel.getBus_name());
        shopAddress.setText(userModel.getBusiness_address());
        shopNumber.setText(userModel.getBus_mobile());

        String imagepath = Base_Url.imagepath + userModel.getPhoto();
        Glide.with(this)
                .load(imagepath)
                .into(userPhoto);


        myShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myshop = new Intent(MainActivity.this, MyShop.class);
                startActivity(myshop);
            }
        });

        Privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent privacy = new Intent(MainActivity.this, PrivacyPolicy.class);
                startActivity(privacy);
            }
        });

        Terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rules = new Intent(MainActivity.this, TermsAndConditions.class);
                startActivity(rules);
            }
        });

        Notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noti = new Intent(MainActivity.this, Notifications.class);
                startActivity(noti);
            }
        });

        MyPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent post = new Intent(MainActivity.this, MyPosts.class);
                startActivity(post);
            }
        });

        MyReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(MainActivity.this, MyReviews.class);
                startActivity(review);
            }
        });

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.d("TAG", "onResponseTUSHARLATI: " + latitude);
                Log.d("TAG", "onResponseTUSHARLong: " + longitude);

            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void OnGPS() {

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getSaveData(String businessId, String fcmregistrationId, String lang, String latitude, String longitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.BUSINESSFCMUPDATED, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");

                    if(jsonObject.getString("status").equals("true"))
                    {

                        String message = jsonObject.getString("message");
                        //Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {

                        String message = jsonObject.getString("message");
                        // Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

              //  Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessId);
                params.put("fcm_token", fcmregistrationId);
                params.put("language", lang);
                params.put("latitude", latitude);
                params.put("longitude", longitude);


                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void loadBanners()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_BANNER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("banner");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            imagelist.add(new SliderItem(
                                    product.getString("banner_id"),
                                    product.getString("banner_title"),
                                    product.getString("banner_image")
                            ));
                        }

                        SliderAdapterExample adapter = new SliderAdapterExample(MainActivity.this,imagelist);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();

                    }
                    else
                    {

                        String message = jsonObject.getString("message");
//                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) ;

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


    private void setUpToolbar()
    {
        drawerLayout = findViewById(R.id.main_drawer_layout);
        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle(R.string.app_name);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        myShop = findViewById(R.id.myshop_main);
        Privacy = findViewById(R.id.privacy_main);
        Terms = findViewById(R.id.terms_main);
        Notifications = findViewById(R.id.notifications_main);
        MyPost = findViewById(R.id.mypost_main);
        MyReviews = findViewById(R.id.review_main);
        sliderView = findViewById(R.id.imageSlider);

    }

    public void reviewOnApp()
    {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent gotorate = new Intent(Intent.ACTION_VIEW, uri);
        gotorate.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(gotorate);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_home_menu,menu);
        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        notifCount = (TextView) MenuItemCompat.getActionView(item);
        getBusinessNotification(businessId);
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReadNotify(businessId);
                Intent noti = new Intent(MainActivity.this, Notifications.class);
                startActivity(noti);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void getReadNotify(String businessId) {
        apiClient.apiInterface.showBusinessreadnoti(businessId).enqueue(new Callback<ReadNotiModel>() {
            @Override
            public void onResponse(Call<ReadNotiModel> call, retrofit2.Response<ReadNotiModel> response) {
                if (response.isSuccessful()) {

                }
            }@Override
            public void onFailure(Call<ReadNotiModel> call, Throwable t) {

            }
        });
    }

    private void getBusinessNotification(String businessId) {
        apiClient.apiInterface.showBusinessUnreadNotify(businessId).enqueue(new Callback<UnreadNotificationModel>() {
            @Override
            public void onResponse(Call<UnreadNotificationModel> call, retrofit2.Response<UnreadNotificationModel> response) {
                if (response.isSuccessful()) {
                    mNotifCount = response.body().getCount();
                    notifCount.setText(mNotifCount);
                }
            }@Override
            public void onFailure(Call<UnreadNotificationModel> call, Throwable t) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.notification:
                Intent notify = new Intent(MainActivity.this, Notifications.class);
                startActivity(notify);
                break;
        }
        return true;
    }
    @Override public void onBackPressed() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Exit Page") .setMessage("Do you really want to exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                        { public void onClick(DialogInterface dialog, int which)
                        { ActivityCompat.finishAffinity(MainActivity.this);
                            finish();
                        }
                        }
                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss(); } })
                .setIcon(android.R.drawable.ic_dialog_alert) .show();

    }

}