package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Adapter.MyRulesAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.RulesModelBusiness;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TermsAndConditions extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;

    LinearLayout addTerms;
    ProgressDialog progressDialog;
    String token;

    ImageView noDataFound;
    ProgressBar progressBar;
    RecyclerView myrules_rv;
    List<RulesModelBusiness> rulesModelBusinessList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        toolbar = findViewById(R.id.terms_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        progressDialog = new ProgressDialog(this);
        addTerms = findViewById(R.id.add_terms_conditions);

        toolbar_title.setText(R.string.termsandcond);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.rules_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        myrules_rv = findViewById(R.id.myrules_rv);

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        token = userModel.getToken();

        rulesModelBusinessList = new ArrayList<>();
        myrules_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        myrules_rv.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), myrules_rv, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {

                        String imagepath,title,desc,time,rule_id;

                        title = rulesModelBusinessList.get(position).getRule_title();
                        desc = rulesModelBusinessList.get(position).getRule_description();
                        time = rulesModelBusinessList.get(position).getCreated_at();
                        rule_id = rulesModelBusinessList.get(position).getBr_id();

                        Intent catlist = new Intent(TermsAndConditions.this,EditRuleActivity.class);
                        catlist.putExtra("rule_title",title);
                        catlist.putExtra("rule_time",time);
                        catlist.putExtra("rule_desc",desc);
                        catlist.putExtra("rule_id",rule_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));



        addTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                opendialog();
            }
        });

    }

    private void opendialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(TermsAndConditions.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.addterms_conditions, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        EditText gettitle,getdesc;
        TextView okay,cancel;
        gettitle = dialogView.findViewById(R.id.terms_title);
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String title,desc;
                title = gettitle.getText().toString();
                desc = getdesc.getText().toString();

                if (title.isEmpty())
                {
                    gettitle.setError("Please enter title");
                    gettitle.requestFocus();
                }
                else if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    alertDialog.dismiss();
                    addRules(title,desc);
                }
            }
        });

        alertDialog.show();
    }

    private void addRules(String title, String desc)
    {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_RULES_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(TermsAndConditions.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(TermsAndConditions.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(TermsAndConditions.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("rule_title", title);
                params.put("rule_description", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onStart()
    {
        loadRules();
        super.onStart();
    }

    private void loadRules()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.ADD_RULES_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                myrules_rv.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        rulesModelBusinessList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("rule");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            rulesModelBusinessList.add(new RulesModelBusiness(
                                    product.getString("br_id"),
                                    product.getString("business_id_fk"),
                                    product.getString("rule_title"),
                                    product.getString("rule_description"),
                                    product.getString("created_at"),
                                    product.getString("live_status")
                            ));
                        }

                        MyRulesAdapter adapter = new MyRulesAdapter(TermsAndConditions.this, rulesModelBusinessList);
                        myrules_rv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        if (adapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            myrules_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(TermsAndConditions.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressBar.setVisibility(View.GONE);
                    myrules_rv.setVisibility(View.VISIBLE);

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                myrules_rv.setVisibility(View.VISIBLE);

                Toast.makeText(TermsAndConditions.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }



}