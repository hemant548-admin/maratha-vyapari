package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class UpdatePassword extends AppCompatActivity
{
    EditText getPassword,getCnfPassword;
    LinearLayout updatePass,back_layout;
    ImageView visiblePass,visiblePass2;
    String account_type,number;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        updatePass = findViewById(R.id.update_password_btn);
        back_layout = findViewById(R.id.back_layout);
        getPassword = findViewById(R.id.update_password_ed);
        getCnfPassword = findViewById(R.id.update_cnfpassword_ed);
        visiblePass = findViewById(R.id.visibility);
        visiblePass2 = findViewById(R.id.visibility2);
        progressDialog = new ProgressDialog(this);

        account_type = getIntent().getStringExtra("account_type");
        number = getIntent().getStringExtra("number");

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        updatePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });

        visiblePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass(v);
            }
        });

        visiblePass2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass2(v);
            }
        });

    }

    public void ShowHidePass(View view) {

        if(view.getId()==R.id.visibility){
            if(getPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public void ShowHidePass2(View view) {

        if(view.getId()==R.id.visibility2){
            if(getCnfPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getCnfPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getCnfPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }



    private void updatePassword()
    {
        String pass,pass2;
        pass = getPassword.getText().toString();
        pass2 = getCnfPassword.getText().toString();

        if (pass.isEmpty())
        {
            getPassword.setError("Please enter password");
            getPassword.requestFocus();
        }
        else if (pass.length() < 6)
        {
            getPassword.setError("Please enter password more than 6 character");
            getPassword.requestFocus();
        }
        else if (!isValidPassword(pass))
        {
            getPassword.setError("Password must contain upper letters, numbers and special symbols");
            getPassword.requestFocus();
        }
        else if (pass2.isEmpty())
        {
            getCnfPassword.setError("Please confirm password");
            getCnfPassword.requestFocus();
        }
       else if (!pass.equals(pass2))
        {
            getCnfPassword.setError("Password did not matched");
            getCnfPassword.requestFocus();
        }
       else
        {
            if (account_type.equals("business"))
            {
               updateBusinessPassword(pass,pass2);
            }
            else if (account_type.equals("service"))
            {
                updateServicePassword(pass,pass2);
            }
            else if (account_type.equals("user"))
            {
                updateUserPassword(pass,pass2);
            }
        }

    }

    private void updateUserPassword(String pass, String pass2) {
        progressDialog.setTitle("Update");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UPDATE_USER_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent login = new Intent(UpdatePassword.this,LoginActivity.class);
                        login.putExtra("account_type",account_type);
                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();

                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UpdatePassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("password1", pass);
                params.put("password2", pass2);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void updateServicePassword(String pass, String pass2) {
        progressDialog.setTitle("Update");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UPDATE_SERVICE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent login = new Intent(UpdatePassword.this,LoginActivity.class);
                        login.putExtra("account_type",account_type);
                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();

                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UpdatePassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("password1", pass);
                params.put("password2", pass2);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void updateBusinessPassword(String pass, String pass2)
    {
        progressDialog.setTitle("Update");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UPDATE_BUSINESS_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent login = new Intent(UpdatePassword.this,LoginActivity.class);
                        login.putExtra("account_type",account_type);
                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();

                        String message = jsonObject.getString("message");
                        Toast.makeText(UpdatePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UpdatePassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("password1", pass);
                params.put("password2", pass2);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);

    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return PASSWORD_PATTERN.matcher(s).matches();
    }
}