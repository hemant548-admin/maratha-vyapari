package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.MyBusinessListAdapter;
import com.example.marathavyapari.Adapter.MySubBusinessListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessCatModel;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessSubCatModel;
import com.example.marathavyapari.Model.DistrictDataModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.ServiceCatModel;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.Model.SubBusinessListModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceRegister2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.BusinessActivity;
import com.example.marathavyapari.User.SubBusinessActivity;
import com.example.marathavyapari.User.UserRegister1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class RegisterBusiness2 extends AppCompatActivity {
    LinearLayout nextBtn, back_layout;
    EditText whatsappNumber, getPassword, getconfirmPassword;
    String name, businessname, address, desc, number,districtname, districtid, talukaname, talukaid, lang;
    Spinner businessType, businessCat,businessSubcat;
    ImageView visiblePass, visiblePass2;
    private static final String[] paths = {"Select/निवडा", "Wholesale/घाऊक", "Retail/किरकोळ", "Production/उत्पादक"};
    ArrayList<BusinessCategoryModel.CategoryList> businessCatModelList;
    ArrayList<SubBusinessListModel.SubBusinessCategoryList> businessSubCatModelList;
    String businessCatId,businessSubCatId;
    ApiClient apiClient;
    ArrayList<DistrictModel.DistrictsList> districtsList;
    ArrayList<TalukaDataModel.TalukaList> talukaList;
    Spinner district, taluka;
    protected boolean gps_enabled, network_enabled;
    protected LocationManager locationManager;
    SharedPrefManager sharedPrefManager;
    protected LocationListener locationListener;
    private static final int REQUEST_LOCATION = 1;
    private String latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_business2);

        nextBtn = findViewById(R.id.business_register2);
        back_layout = findViewById(R.id.back_layout);
        businessType = findViewById(R.id.register_business_gettype);
        whatsappNumber = findViewById(R.id.register_business_whtsnumber);
        getPassword = findViewById(R.id.register_business_getpassword);
        getconfirmPassword = findViewById(R.id.register_business_getpasswordcnf);
        visiblePass = findViewById(R.id.visibility);
        visiblePass2 = findViewById(R.id.visibility2);
        businessCat = findViewById(R.id.register_business_getcat);
        businessSubcat = findViewById(R.id.register_business_getsubcat);
        district = findViewById(R.id.register_districts);
        taluka = findViewById(R.id.register_taluka);
        name = getIntent().getStringExtra("name");
        businessname = getIntent().getStringExtra("businessname");
        address = getIntent().getStringExtra("address");
        desc = getIntent().getStringExtra("desc");
        number = getIntent().getStringExtra("number");

        sharedPrefManager = new SharedPrefManager(RegisterBusiness2.this);
        apiClient = new ApiClient();

        whatsappNumber.setText(number);
        whatsappNumber.setEnabled(false);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterBusiness2.this,R.layout.checked_text_white, paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        businessType.setAdapter(adapter);

        lang = sharedPrefManager.getLanguage("Lang", RegisterBusiness2.this);
        districtsList = new ArrayList<>();
        talukaList = new ArrayList<>();
        businessCatModelList = new ArrayList<>();
        getBusinessCategory(lang);
        getDistricts(lang);

        businessSubCatModelList = new ArrayList<>();
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        businessCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent1, View view1, int position, long id1) {

                businessCatId = businessCatModelList.get(position).getBc_id();
                Log.i("TAG", "onItemSelected: CategoryId = " + businessCatId);
                getSubCatList(businessCatId, lang);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        businessSubcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent1, View view1, int position, long id1) {

                businessSubCatId = businessSubCatModelList.get(position).getBsc_id();
                Log.i("TAG", "onItemSelected: SubCategoryId = " + businessSubCatId);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                districtid = districtsList.get(i).getDistrict_id();
                Log.d("TusharDistrictId", districtid);
                getTaluka(districtid, lang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        taluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                talukaid = talukaList.get(i).getTaluka_id();
                Log.d("TUSHARTALULAID", talukaid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register2();
            }
        });

        visiblePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass(v);
            }
        });

        visiblePass2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass2(v);
            }
        });

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                RegisterBusiness2.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                RegisterBusiness2.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.d("TAG", "onResponse: " + latitude);
                Log.d("TAG", "onResponse: " + longitude);

            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getDistricts(String lang) {

        apiClient.apiInterface.districtlist(lang).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, retrofit2.Response<DistrictModel> response) {
                if (response.isSuccessful()) {
                    districtsList = response.body().getDistricts();

                    if (districtsList.size() >= 0) {

                        List<String> districtlistarray = new ArrayList<String>();
                        if (districtlistarray.size() > 0) {
                            districtlistarray.clear();
                        }
//
                        for (int i = 0; i < districtsList.size() ; i++)
                        {
                            String distlist = districtsList.get(i).getDistrict_name();
                            districtlistarray.add(distlist);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                (RegisterBusiness2.this,R.layout.checked_text_white,districtlistarray);
                        district.setAdapter(adapter);
                    }

                }
            }@Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {

            }
        });
    }

    private void getTaluka(String districtid, String lang) {
        apiClient.apiInterface.talukalist(districtid, lang).enqueue(new Callback<TalukaDataModel>() {
            @Override
            public void onResponse(Call<TalukaDataModel> call, retrofit2.Response<TalukaDataModel> response) {
                if (response.isSuccessful()) {
                    talukaList = response.body().getTaluka();

                    if (talukaList.size() >= 0) {

                        List<String> usernamearry = new ArrayList<String>();
                        if (usernamearry.size() > 0) {
                            usernamearry.clear();
                        }
                        for (int i = 0; i < talukaList.size(); i++) {
                            String username = talukaList.get(i).getTaluka_name();

                            Log.e("TAG", "onResponse: username  " + username);
                            usernamearry.add(username);
                        }

                        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(RegisterBusiness2.this, R.layout.checked_text_white, usernamearry);
                        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        taluka.setAdapter(dataAdapter2);
                    }

                }
            }@Override
            public void onFailure(Call<TalukaDataModel> call, Throwable t) {

            }
        });
    }

    private void getSubCatList(String businessCatId, String lang)
    {
        apiClient.apiInterface.showSubBusinessCat(businessCatId, lang).enqueue(new Callback<SubBusinessListModel>() {
            @Override
            public void onResponse(Call<SubBusinessListModel> call, retrofit2.Response<SubBusinessListModel> response) {
                if (response.isSuccessful()) {
                    try {
                        // progressBar.setVisibility(View.GONE);
                        businessSubCatModelList = response.body().getSub_category();
                        List<String> catlistarray = new ArrayList<String>();
                        if (catlistarray.size() > 0) {
                            catlistarray.clear();
                        }

                        for (int i = 0; i < businessSubCatModelList.size() ; i++)
                        {
                            String catlist = businessSubCatModelList.get(i).getBsc_name();
                            catlistarray.add(catlist);
                        }

                        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(RegisterBusiness2.this, R.layout.checked_text_white, catlistarray);
                        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        businessSubcat.setAdapter(dataAdapter5);

                    }catch (Exception e)
                    {
                       /* noDataFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);*/
                    }

                }
            }@Override
            public void onFailure(Call<SubBusinessListModel> call, Throwable t) {

            }
        });

    }

    private void getBusinessCategory(String lang) {

        apiClient.apiInterface.showBusinessCategory(lang).enqueue(new Callback<BusinessCategoryModel>() {
            @Override
            public void onResponse(Call<BusinessCategoryModel> call, retrofit2.Response<BusinessCategoryModel> response) {
                if (response.isSuccessful()) {

                    businessCatModelList = response.body().getCategory();
                    List<String> catlistarray = new ArrayList<String>();
                    if (catlistarray.size() > 0) {
                        catlistarray.clear();
                    }
//                        catlistarray.add(0, "Select/निवडा");
                    for (int i = 0; i < businessCatModelList.size() ; i++)
                    {
                        String catlist = businessCatModelList.get(i).getBc_name();
                        catlistarray.add(catlist);
                    }

                    ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(RegisterBusiness2.this,R.layout.checked_text_white, catlistarray);
                    dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    businessCat.setAdapter(dataAdapter5);

                }
            }@Override
            public void onFailure(Call<BusinessCategoryModel> call, Throwable t) {

            }
        });

    }

    public void ShowHidePass(View view) {

        if (view.getId() == R.id.visibility) {
            if (getPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public void ShowHidePass2(View view) {

        if (view.getId() == R.id.visibility2) {
            if (getconfirmPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getconfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getconfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }


    private void register2() {
        String type, whnumber, password, cnfpassword, cat;

        type = businessType.getSelectedItem().toString();
        cat = businessCat.getSelectedItem().toString();
        whnumber = whatsappNumber.getText().toString();
        password = getPassword.getText().toString();
        cnfpassword = getconfirmPassword.getText().toString();

        if (type.equals("Select/निवडा")) {
            Toast.makeText(this, "Select business type", Toast.LENGTH_SHORT).show();
        } else if (cat.equals("Select/निवडा")) {
            Toast.makeText(this, "Select business category", Toast.LENGTH_SHORT).show();
        } else if (whnumber.isEmpty()) {
            whatsappNumber.setError("Please enter your whatsapp number");
            whatsappNumber.requestFocus();
        } else if (whnumber.length() != 10) {
            whatsappNumber.setError("Please enter 10 digit number");
            whatsappNumber.requestFocus();
        } else if (password.isEmpty()) {
            getPassword.setError("Please enter password");
            getPassword.requestFocus();
        } else if (password.length() < 6) {
            getPassword.setError("Please enter more than 6 character");
            getPassword.requestFocus();
        } else if (!isValidPassword(password)) {
            getPassword.setError("Password must contain upper letters, numbers and special symbols");
            getPassword.requestFocus();
        } else if (cnfpassword.isEmpty()) {
            getconfirmPassword.setError("Please enter confirm password");
            getconfirmPassword.requestFocus();
        } else if (!password.equals(cnfpassword)) {
            getconfirmPassword.setError("Password did not matched");
            getconfirmPassword.requestFocus();
        } else {
            Intent intent = new Intent(RegisterBusiness2.this, UploadBusinessDoc.class);
            intent.putExtra("type", type);
            intent.putExtra("whatsapp_number", whnumber);
            intent.putExtra("password", password);
            intent.putExtra("name", name);
            intent.putExtra("businessname", businessname);
            intent.putExtra("address", address);
            intent.putExtra("desc", desc);
            intent.putExtra("category_id", businessCatId);
            intent.putExtra("sub_category_id", businessSubCatId);
            intent.putExtra("DistrictId", districtid);
            intent.putExtra("TalukaId", talukaid);
            intent.putExtra("Latitude", latitude);
            intent.putExtra("Longitude", longitude);

            startActivity(intent);
            finish();
        }

    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return PASSWORD_PATTERN.matcher(s).matches();
    }
}