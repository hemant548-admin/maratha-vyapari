package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class ChangePassword extends AppCompatActivity
{
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    EditText getoldPass,getnewPass,getcnfPass;
    LinearLayout changePassBtn;
    ProgressDialog progressDialog;
    ImageView visible,visible2,visible3;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        toolbar = findViewById(R.id.changepass_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.changepassword);
        progressDialog = new ProgressDialog(this);
        visible = findViewById(R.id.visibility);
        visible2 = findViewById(R.id.visibility2);
        visible3 = findViewById(R.id.visibility3);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        visible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass(v);
            }
        });

        visible2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass2(v);
            }
        });

        visible3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass3(v);
            }
        });

        getoldPass = findViewById(R.id.pass_getoldpassword);
        getnewPass = findViewById(R.id.pass_getnewpassword);
        getcnfPass = findViewById(R.id.pass_getcnfpassword);
        changePassBtn = findViewById(R.id.change_password_btn);

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        token = userModel.getToken();

        changePassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                changePassword();
            }
        });





    }

    private void changePassword()
    {
        String oldpass,newpass,cnfpass;
        oldpass = getoldPass.getText().toString();
        newpass = getnewPass.getText().toString();
        cnfpass = getcnfPass.getText().toString();

        if (oldpass.isEmpty())
        {
            getoldPass.setError("Please enter your old password");
            getoldPass.requestFocus();
        }
        else if (newpass.isEmpty())
        {
            getnewPass.setError("Please enter your new password");
            getnewPass.requestFocus();
        }
        else if (newpass.length() < 6)
        {
            getnewPass.setError("Please enter more than 6 char password");
            getnewPass.requestFocus();
        }
        else if (!isValidPassword(newpass))
        {
            getnewPass.setError("Password must contain upper letters, numbers and special symbols");
            getnewPass.requestFocus();
        }
        else if (cnfpass.isEmpty())
        {
            getcnfPass.setError("Please reenter your password");
            getcnfPass.requestFocus();
        }
        else if (!newpass.equals(cnfpass))
        {
            getcnfPass.setError("Password did not matched");
            getcnfPass.requestFocus();
        }
        else
        {
            updatePass(oldpass,newpass,cnfpass);
        }



    }



    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return PASSWORD_PATTERN.matcher(s).matches();
    }

    public void ShowHidePass(View view) {

        if (view.getId() == R.id.visibility) {
            if (getoldPass.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getoldPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getoldPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public void ShowHidePass2(View view) {

        if (view.getId() == R.id.visibility2) {
            if (getnewPass.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getnewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getnewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public void ShowHidePass3(View view) {

        if (view.getId() == R.id.visibility3) {
            if (getcnfPass.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getcnfPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getcnfPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    private void updatePass(String oldpass, String newpass, String cnfpass)
    {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.CHANGEBUSINESSPASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                        getoldPass.setText("");
                        getnewPass.setText("");
                        getcnfPass.setText("");

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ChangePassword.this);
                        builder1.setMessage("Password updated successfully.");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ChangePassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("old_password", oldpass);
                params.put("password1", newpass);
                params.put("password2", cnfpass);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

}