package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import java.util.Locale;

public class SelectLanguage extends AppCompatActivity
{
    LinearLayout marathi,english;

    SharedPreferences preferences;
    SharedPrefManager sharedPrefManager;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_languages);

        loadlocale();
        localisation();


        marathi = findViewById(R.id.marathi_layout);
        english = findViewById(R.id.english_layout);
       sharedPrefManager = new SharedPrefManager(SelectLanguage.this);

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                editor.putString("Language", "en").commit();
                localisation();
                recreate();
                sharedPrefManager.setLangauge("Lang", "english",SelectLanguage.this);
                Intent intent = new Intent(SelectLanguage.this, SelectAccountType.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                editor.putString("Language", "en").commit();
                startActivity(intent);
              //  Toast.makeText(SelectLanguage.this, "Coming soon", Toast.LENGTH_SHORT).show();
            }
        });

        marathi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                editor.putString("Language", "mr").commit();
                localisation();
                recreate();
                sharedPrefManager.setLangauge("Lang", "marathi",SelectLanguage.this);
                Intent intent = new Intent(SelectLanguage.this, SelectAccountType.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                editor.putString("Language", "mr").commit();
                startActivity(intent);

            }
        });


    }

    public void localisation() {
        Locale locale = new Locale(preferences.getString("Language",""));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        this.getResources().updateConfiguration(config,
                this.getResources().getDisplayMetrics());
    }


    public void loadlocale(){
        preferences = getSharedPreferences("Language", Activity.MODE_PRIVATE);
        editor = preferences.edit();
    }
}