package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Notification;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.ShowNotifyAdapter;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_UserNotification;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class Notifications extends AppCompatActivity
{
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ImageView noDataFound;
    ProgressBar progressBar;
    ShowNotifyAdapter showNotifyAdapter;
    RecyclerView mypost_rv;
    String token, lang, businessid;
    SharedPrefManager sharedPrefManager;
    ApiClient apiClient;
    ArrayList<UserNotificationModel.NotificationList> notificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        toolbar = findViewById(R.id.notifications_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        apiClient = new ApiClient();
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.notifications);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        mypost_rv = findViewById(R.id.mypost_rv);
        notificationList = new ArrayList<>();
        sharedPrefManager = new SharedPrefManager(Notifications.this);
        lang = sharedPrefManager.getLanguage("Lang", Notifications.this);
        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        businessid = userModel.getBus_id();
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBussinesNotification(businessid, lang);
    }

    private void getBussinesNotification(String businessid, String lang) {
        apiClient.apiInterface.showBusinessNotify(businessid,lang).enqueue(new Callback<UserNotificationModel>() {
            @Override
            public void onResponse(Call<UserNotificationModel> call, retrofit2.Response<UserNotificationModel> response) {
                if (response.isSuccessful()) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        notificationList = response.body().getNotification();
                        showNotifyAdapter = new ShowNotifyAdapter(Notifications.this, notificationList);
                        mypost_rv.setAdapter(showNotifyAdapter);
                        showNotifyAdapter.notifyDataSetChanged();
                        if (showNotifyAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            mypost_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }@Override
            public void onFailure(Call<UserNotificationModel> call, Throwable t) {

            }
        });
    }
}