package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MyShop extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;
    EditText getName,getBusinessname,getWhatsapp,getAddress,getDesc;
    EditText getBusinessType,getBusinessCat,getBusinessSubcat;
    private static final String[] paths = {"Wholesale/घाऊक","Retail/किरकोळ","Production/उत्पादक"};
    LinearLayout saveDetails,uploadShopImg;

    String ownerName,businessName,address,number,description,businessType,businessCat,businessSubcat;
    String token;
    ProgressDialog progressDialog;
    List<SliderItem> imagelist;
    SliderView sliderView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop);

        toolbar = findViewById(R.id.mtshop_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        sliderView = findViewById(R.id.imageSlider);
        toolbar_title.setText(R.string.myshop);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        ownerName = userModel.getOwner_name();
        businessName = userModel.getBus_name();
        address = userModel.getBusiness_address();
        number = userModel.getBus_mobile();
        description = userModel.getBusiness_description();
        businessType = userModel.getBusiness_type();
        businessCat = userModel.getBc_name();
        businessSubcat = userModel.getBsc_name();
        token = userModel.getToken();

        imagelist = new ArrayList<>();


        getName = findViewById(R.id.ed_owner_name);
        getBusinessname = findViewById(R.id.ed_business_name);
        getBusinessType = findViewById(R.id.ed_business_type);
        getBusinessSubcat = findViewById(R.id.ed_business_subcat);
        getBusinessCat = findViewById(R.id.ed_business_cat);
        getAddress = findViewById(R.id.ed_address);
        getWhatsapp = findViewById(R.id.ed_whatsapp_number);
        getDesc = findViewById(R.id.ed_description);
        saveDetails = findViewById(R.id.save_details);
        uploadShopImg = findViewById(R.id.upload_shop_img);
        progressDialog = new ProgressDialog(this);

        getName.setText(ownerName);
        getBusinessname.setText(businessName);
        getBusinessType.setText(businessType);
        getBusinessType.setEnabled(false);
        getBusinessSubcat.setText(businessSubcat);
        getBusinessSubcat.setEnabled(false);
        getBusinessCat.setText(businessCat);
        getBusinessCat.setEnabled(false);
        getAddress.setText(address);
        getWhatsapp.setText(number);
        getWhatsapp.setEnabled(false);
        getDesc.setText(description);


//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyShop.this, android.R.layout.simple_spinner_item,paths);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        getBusinessType.setAdapter(adapter);

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updatedetails();
            }
        });

        uploadShopImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MyShop.this,UploadShopImages.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart()
    {
        super.onStart();
        loadBanners();
    }

    private void loadBanners()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_BUSINESS_SHOPIMAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: photos "+response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        imagelist.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("photos");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            imagelist.add(new SliderItem(
                                    product.getString("bp_id"),
                                    "",
                                    product.getString("image_file")
                            ));
                        }

                        SliderAdapterExample adapter = new SliderAdapterExample(MyShop.this,imagelist);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();

                    }
                    else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(MyShop.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                Toast.makeText(MyShop.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void updatedetails()
    {
        String name,businessname,type,address,number,desc;

        name = getName.getText().toString();
        businessname = getBusinessname.getText().toString();
        type = getBusinessType.getText().toString();
//        Toast.makeText(this, type, Toast.LENGTH_SHORT).show();
        address = getAddress.getText().toString();
//        number = getWhatsapp.getText().toString();
        desc = getDesc.getText().toString();

        if (name.isEmpty())
        {
            getName.setError("Please enter your name");
            getName.requestFocus();
        }
        else if (businessname.isEmpty())
        {
            getBusinessname.setError("Please enter your business name");
            getBusinessname.requestFocus();
        }
        else if (address.isEmpty())
        {
            getAddress.setError("Please enter your business address");
            getAddress.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else
        {
           updateProfile(name,businessname,address,desc,type);
        }

    }

    private void updateProfile(String name, String businessname, String address, String desc, String type)
    {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UPDATEBUSINESSPROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        JSONObject obj = jsonObject.getJSONObject("profile");

                        BusinessUserModel user = new BusinessUserModel(
                               token,
                                obj.getString("bus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("bus_mobile"),
                                obj.getString("bus_name"),
                                obj.getString("owner_name"),
                                obj.getString("business_address"),
                                obj.getString("business_description"),
                                obj.getString("business_type"),
                                obj.getString("detail_info"),
                                obj.getString("licence"),
                                obj.getString("photo"),
                                obj.getString("business_status"),
                                obj.getString("business_email"),
                                obj.getString("bc_name"),
                                obj.getString("live_status"),
                                obj.getString("bsc_name")
                        );

                        SharedPrefManager.getInstance(MyShop.this).userLogin(user);

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MyShop.this);
                        builder1.setMessage("Profile Details Updated Successfully");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id)
                                    {

                                        dialog.cancel();

                                        Intent login = new Intent(MyShop.this, MainActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                        finish();

                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();



                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                       // Toast.makeText(MyShop.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MyShop.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("business_name", businessname);
                params.put("business_address", address);
                params.put("business_description", desc);
                params.put("business_type", type);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}