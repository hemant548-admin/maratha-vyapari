package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterBusiness extends AppCompatActivity
{
    LinearLayout register_Btn,back_layout;
    EditText ownerName,businessName,getAddress,getDesc,getLastname;
    ProgressDialog progressDialog;
    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_business);

        register_Btn = findViewById(R.id.business_register);
        back_layout = findViewById(R.id.back_layout);
        ownerName = findViewById(R.id.register_business_getname);
        businessName = findViewById(R.id.register_business_getbusinessname);
        getAddress = findViewById(R.id.register_business_getaddress);
        getDesc = findViewById(R.id.register_business_getdesc);
        getLastname = findViewById(R.id.register_business_getlastname);
        progressDialog = new ProgressDialog(this);

        number = getIntent().getStringExtra("number");

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        register_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                registerbusiness();
            }
        });


    }

    private void registerbusiness()
    {
        String name,businessname,address,desc,lastname;

        name = ownerName.getText().toString();
        businessname = businessName.getText().toString();
        address = getAddress.getText().toString();
        desc = getDesc.getText().toString();
        lastname = getLastname.getText().toString();

        if (name.isEmpty())
        {
            ownerName.setError("Please enter your name");
            ownerName.requestFocus();
        }
        else if (lastname.isEmpty())
        {
            getLastname.setError("Please enter your last name");
            getLastname.requestFocus();
        }
        else if (businessname.isEmpty())
        {
            businessName.setError("Please enter your business name");
            businessName.requestFocus();
        }
        else if (address.isEmpty())
        {
            getAddress.setError("Please enter your business address");
            getAddress.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter your business descripton");
            getDesc.requestFocus();
        }
        else
        {
            Intent intent = new Intent(RegisterBusiness.this,RegisterBusiness2.class);
            intent.putExtra("name",name + " " + lastname);
            intent.putExtra("businessname",businessname);
            intent.putExtra("address",address);
            intent.putExtra("desc",desc);
            intent.putExtra("number",number);
            startActivity(intent);
            finish();

        }

    }
}