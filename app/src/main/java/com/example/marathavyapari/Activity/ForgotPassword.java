package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity
{

    EditText getNumber;
    String account_type;
    LinearLayout sendOtp,back_layout;
    ProgressDialog progressDialog;
    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getNumber = findViewById(R.id.forogt_number_ed);
        sendOtp = findViewById(R.id.forgot_send_otp);
        back_layout = findViewById(R.id.back_layout);
        progressDialog = new ProgressDialog(this);

        account_type = getIntent().getStringExtra("account_type");
        Log.d("TusharAcc", account_type);

        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP();
            }
        });

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void sendOTP()
    {
        number = getNumber.getText().toString();

        if (number.isEmpty())
        {
            getNumber.setError("Please enter your mobile number");
            getNumber.requestFocus();
        }
        else if (number.length() != 10)
        {
            getNumber.setError("Please enter 10 digit mobile number");
            getNumber.requestFocus();

        }
        else
        {
            if (account_type.equals("business"))
            {
                businessforgotpass();
            }
            else if (account_type.equals("service"))
            {
                serviceforgotpass();
            }
            else  if (account_type.equals("user"))
            {
                userforgotpass();
            }

        }

    }

    private void userforgotpass() {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_USER_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(ForgotPassword.this,Forgot_Pass_Verify_Otp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void serviceforgotpass() {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_SERVICES_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(ForgotPassword.this,Forgot_Pass_Verify_Otp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void businessforgotpass() {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_BUSINESS_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(ForgotPassword.this,Forgot_Pass_Verify_Otp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}