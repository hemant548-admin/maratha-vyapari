package com.example.marathavyapari.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UploadShopImages extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;

    ImageView image;
    LinearLayout uploadLayout;

    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 101;

    Bitmap bitmap;
    Uri filepath;
    String postImage= "";
    String token;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_shop_images);

        toolbar = findViewById(R.id.uploadimg_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.uploadshopimage);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        image = findViewById(R.id.upload_image);
        uploadLayout = findViewById(R.id.upload_shop_img);
        progressDialog = new ProgressDialog(this);

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        token = userModel.getToken();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                showdialog();
            }
        });

        uploadLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
//                Toast.makeText(UploadShopImages.this, postImage, Toast.LENGTH_SHORT).show();
                if(postImage.equals(""))
                {
                    Toast.makeText(UploadShopImages.this, "Please select image", Toast.LENGTH_SHORT).show();
                }else {
                    uploadImage();
                }
            }
        });



    }

    private void uploadImage()
    {
        progressDialog.setTitle("Uploading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.GET_UPLOAD_BUSINESS_SHOPIMAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        Toast.makeText(UploadShopImages.this, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UploadShopImages.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                Toast.makeText(UploadShopImages.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("photo_file", postImage);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showdialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(UploadShopImages.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_camera_dialog, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        LinearLayout camera,device;
        camera = dialogView.findViewById(R.id.with_camera);
        device = dialogView.findViewById(R.id.with_device);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_DEVICE_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent,"Select Image"),2);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            imagestore(bitmap);
            image.setImageBitmap(bitmap);
            image.setEnabled(false);

        }
        else if (requestCode==2 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imagestore(bitmap);
                image.setImageBitmap(bitmap);
                image.setEnabled(false);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void imagestore(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        postImage = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }
}