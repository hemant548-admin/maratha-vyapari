package com.example.marathavyapari.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.Manifest.permission.CALL_PHONE;

public class ContactUs extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title, number, address;
    ImageView back;

    EditText getName,getEmail,getDesc;
    LinearLayout sendLayout;
    LinearLayout callText;
    ProgressDialog progressDialog;
    String token, OwnerName, Number, Address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        toolbar = findViewById(R.id.contactus_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.contact_us);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getName = findViewById(R.id.contactus_name);
        getEmail = findViewById(R.id.contactus_email);
        getDesc = findViewById(R.id.contactus_desc);
        sendLayout = findViewById(R.id.contactus_send);
        callText = findViewById(R.id.contactus_call_txt);
        progressDialog = new ProgressDialog(this);

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        token = userModel.getToken();
        OwnerName = userModel.getOwner_name();
        getName.setText(OwnerName);
        Address = userModel.getBusiness_address();
        number = findViewById(R.id.text_numbar);
        address = findViewById(R.id.text_address);
        number.setText("9890374498");
        address.setText(R.string.adressuser);
        callText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (checkPermission())
                {
                    String number = "+919890374498";
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + number));
                    startActivity(intentcall);
                }
                else
                {
                    requestPermission();
                }
            }
        });

        sendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail();
            }
        });
    }

    private boolean checkPermission()
    {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);

        return result == PackageManager.PERMISSION_GRANTED ;
    }
    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, 200);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 200)
        {
            if (grantResults.length > 0) {
                boolean cameraaccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (cameraaccepted)
                {
                    String number = "+919172063211";
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + number));
                    startActivity(intentcall);

                }
                else
                {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    public void requestpermission()
    {
        ActivityCompat.requestPermissions(ContactUs.this,new String[]{Manifest.permission.CALL_PHONE},1);
    }


    private void sendmail()
    {
        String name,email,desc;

        name = getName.getText().toString();
        email = getEmail.getText().toString();
        desc = getDesc.getText().toString();

        if (name.isEmpty())
        {
            getName.setError("Please enter your name");
            getName.requestFocus();
        }
        else if (email.isEmpty())
        {
            getEmail.setError("Please enter your gmail id");
            getEmail.requestFocus();
        }else if (!email.contains("@")){
            getEmail.setError("please enter valid email address");
            getEmail.requestFocus();
        }
        else if (!email.contains(".")){
            getEmail.setError("please enter valid email address");
            getEmail.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else
        {
//            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
            sendMessage(name,email,desc);
        }

    }

    private void sendMessage(String name, String email, String desc)
    {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.BUSINESS_SEND_CONTACTMSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ContactUs.this, message, Toast.LENGTH_SHORT).show();
                        getName.setText("");
                        getEmail.setText("");
                        getDesc.setText("");

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ContactUs.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ContactUs.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("business_email", email);
                params.put("contact_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}