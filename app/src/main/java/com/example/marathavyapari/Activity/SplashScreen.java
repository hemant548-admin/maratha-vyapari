package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.R;
import com.example.marathavyapari.Service.PendingApplicationBusiness2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.UserMainActivity;

public class SplashScreen extends AppCompatActivity
{
    private static int SPLASH_TIME_OUT = 2000;
    String account_type;
    Handler handler;
    SharedPrefManager sharedPrefManager;
    String user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        sharedPrefManager = new SharedPrefManager(this);
        user_type = sharedPrefManager.getUserType("UserType", SplashScreen.this);
        Log.d("TusharUserType", user_type);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPrefManager.getInstance(SplashScreen.this).isLoggedIn()) {
                    if (user_type.equals("business")) {
                        startActivity(new Intent(SplashScreen.this, PendingApplicationBusiness.class));
                        finish();
                        return;
                    } else if (user_type.equals("service")) {
                        startActivity(new Intent(SplashScreen.this, PendingApplicationBusiness2.class));
                        finish();
                        return;
                    } else if (user_type.equals("user")){
                        startActivity(new Intent(SplashScreen.this, UserMainActivity.class));
                        finish();
                        return;
                    }

                } else {
                    Intent login = new Intent(SplashScreen.this, SelectLanguage.class);
                    startActivity(login);
                    finish();
                }
            }
            }, SPLASH_TIME_OUT);
                            }


            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent login = new Intent(SplashScreen.this, SelectLanguage.class);
                    startActivity(login);
                    finish();
                }
            }, SPLASH_TIME_OUT);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setExitTransition(null);
            }

    }*/
}