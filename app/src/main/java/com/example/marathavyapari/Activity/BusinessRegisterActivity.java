package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BusinessRegisterActivity extends AppCompatActivity
{
    LinearLayout sendOTP,back_layout;
    EditText getNumber;
    LinearLayout first_layout;
    String account_type, number;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_register);

        sendOTP = findViewById(R.id.business_register_sendotp);
        getNumber = findViewById(R.id.business_register_getnumber);
        back_layout = findViewById(R.id.back_layout);
        first_layout = findViewById(R.id.first_number);
        progressDialog = new ProgressDialog(this);

        account_type = getIntent().getStringExtra("account_type");
        Log.d("TUSHARAccoountType", account_type);

        sendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sendotp();
            }
        });

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void sendotp()
    {
       number = getNumber.getText().toString();

        if (number.isEmpty())
        {
            getNumber.setError("Please enter your mobile number");
            getNumber.requestFocus();
        }
        else if (number.length() != 10)
        {
            getNumber.setError("Please enter 10 digits mobile number");
            getNumber.requestFocus();
        }
        else
        {
            if (account_type.equals("business"))
            {
                getBusinessCategory();
           }else if (account_type.equals("service"))
            {
                getServiceCategory();
            }else if (account_type.equals("user"))
            {
                getUserCategory();
            }
        }
    }

    private void getUserCategory() {

        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        // if(account_type ==)
                        Intent intent = new Intent(BusinessRegisterActivity.this,VerifyOtp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);

                        Pair[] pairs = new Pair[1];
                        pairs[0] = new Pair<View,String>(first_layout,"first");
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(BusinessRegisterActivity.this,pairs);
                        startActivity(intent,options.toBundle());
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BusinessRegisterActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getServiceCategory() {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_SERVICE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        // if(account_type ==)
                        Intent intent = new Intent(BusinessRegisterActivity.this,VerifyOtp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);

                        Pair[] pairs = new Pair[1];
                        pairs[0] = new Pair<View,String>(first_layout,"first");
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(BusinessRegisterActivity.this,pairs);
                        startActivity(intent,options.toBundle());
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BusinessRegisterActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getBusinessCategory() {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SEND_OTP_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        // if(account_type ==)
                        Intent intent = new Intent(BusinessRegisterActivity.this,VerifyOtp.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);

                        Pair[] pairs = new Pair[1];
                        pairs[0] = new Pair<View,String>(first_layout,"first");
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(BusinessRegisterActivity.this,pairs);
                        startActivity(intent,options.toBundle());
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(BusinessRegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BusinessRegisterActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}