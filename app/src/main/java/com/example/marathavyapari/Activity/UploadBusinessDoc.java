package com.example.marathavyapari.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceMainActivity;
import com.example.marathavyapari.Service.UploadServiceDoc;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class UploadBusinessDoc extends AppCompatActivity
{
    LinearLayout upload_doc,back_layout;
    ImageView doc1,doc2,doc3;
    String name,businessname,address,desc,type,whatsappnumber,password, latitude,longitude, talukaId, districtId;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 200;
    Bitmap bitmap;
    String tapshil="",parwana="",photo="";
    Uri filepath;

    int CAM_DOC1,CAM_DOC2,CAM_DOC3;
    int DEVICE_DOC1,DEVICE_DOC2,DEVICE_DOC3;
    ProgressDialog progressDialog;
    String categoryId,SubCatId;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_business_doc);

        back_layout = findViewById(R.id.back_layout);
        upload_doc = findViewById(R.id.upload_doc);
        doc1 = findViewById(R.id.business_document_1);
        doc2 = findViewById(R.id.business_document_2);
        doc3 = findViewById(R.id.business_document_3);
        progressDialog = new ProgressDialog(this);

        sharedPrefManager = new SharedPrefManager(this);
        name = getIntent().getStringExtra("name");
        businessname = getIntent().getStringExtra("businessname");
        address = getIntent().getStringExtra("address");
        desc = getIntent().getStringExtra("desc");
        type = getIntent().getStringExtra("type");
        whatsappnumber = getIntent().getStringExtra("whatsapp_number");
        password = getIntent().getStringExtra("password");
        categoryId = getIntent().getStringExtra("category_id");
        SubCatId = getIntent().getStringExtra("sub_category_id");


        latitude = getIntent().getStringExtra("Latitude");
        longitude = getIntent().getStringExtra("Longitude");
        districtId = getIntent().getStringExtra("DistrictId");
        talukaId = getIntent().getStringExtra("TalukaId");

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        doc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CAM_DOC1 = 1;
                DEVICE_DOC1 = 2;
                showdialog(CAM_DOC1,DEVICE_DOC1);
            }
        });

        doc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CAM_DOC2 = 3;
                DEVICE_DOC2 = 4;
                showdialog(CAM_DOC2,DEVICE_DOC2);
            }
        });

        doc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CAM_DOC3 = 5;
                DEVICE_DOC3 = 6;
                showdialog(CAM_DOC3,DEVICE_DOC3);
            }
        });


        upload_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (tapshil.equals(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(UploadBusinessDoc.this);
                    builder1.setMessage("Please Upload All Documents.");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else if (parwana.equals(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(UploadBusinessDoc.this);
                    builder1.setMessage("Please Upload All Documents.");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else if (photo.equals(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(UploadBusinessDoc.this);
                    builder1.setMessage("Please Upload All Documents.");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else
                {
                    registeruser();
                }
            }
        });

    }

    private void registeruser()
    {
        progressDialog.setTitle("Register");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.REGISTER_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        JSONObject obj = jsonObject.getJSONObject("profile");
//                        Toast.makeText(UploadBusinessDoc.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                        BusinessUserModel user = new BusinessUserModel(
                                jsonObject.getString("token"),
                                obj.getString("bus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("bus_mobile"),
                                obj.getString("bus_name"),
                                obj.getString("owner_name"),
                                obj.getString("business_address"),
                                obj.getString("business_description"),
                                obj.getString("business_type"),
                                obj.getString("detail_info"),
                                obj.getString("licence"),
                                obj.getString("photo"),
                                obj.getString("business_status"),
                                obj.getString("business_email"),
                                obj.getString("bc_name"),
                                obj.getString("live_status"),
                                obj.getString("bsc_name")
                        );
                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), UploadBusinessDoc.this);

                        final Dialog dialog = new Dialog(UploadBusinessDoc.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.register_successffully_dialog);

                        Button dialogButton = (Button) dialog.findViewById(R.id.okay_btn);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();

                                Intent login = new Intent(UploadBusinessDoc.this, PendingApplicationBusiness.class);
                                login.putExtra("account_type","business");
                                login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(login);
                                finish();

                            }
                        });

                        dialog.show();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UploadBusinessDoc.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UploadBusinessDoc.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("mobile", whatsappnumber);
                params.put("password", password);
                params.put("business_name", businessname);
                params.put("business_address", address);
                params.put("business_description", desc);
                params.put("detail_info", tapshil);
                params.put("licence", parwana);
                params.put("photo", photo);
                params.put("business_type", type);
                params.put("district_id", districtId);
                params.put("taluka_id", talukaId);
                params.put("category_id", categoryId);
                params.put("subcategory_id", SubCatId);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void showdialog(int i, int i1)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(UploadBusinessDoc.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_camera_dialog, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        LinearLayout camera,device;
        TextView selectDevice;
        camera = dialogView.findViewById(R.id.with_camera);
        device = dialogView.findViewById(R.id.with_device);
        selectDevice = dialogView.findViewById(R.id.select_device_text);


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, i);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_DEVICE_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent,"Select Image"),i1);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        alertDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode==CAM_DOC1 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            doc1.setImageBitmap(bitmap);
            imagestore(bitmap);
        }
        else if (requestCode==DEVICE_DOC1 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                doc1.setImageBitmap(bitmap);

                imagestore(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode==CAM_DOC2 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            doc2.setImageBitmap(bitmap);
            imagestore2(bitmap);
        }
        else if (requestCode==DEVICE_DOC2 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                doc2.setImageBitmap(bitmap);

                imagestore2(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode==CAM_DOC3 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            doc3.setImageBitmap(bitmap);
            imagestore3(bitmap);
        }
        else if (requestCode==DEVICE_DOC3 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                doc3.setImageBitmap(bitmap);
                imagestore3(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void imagestore(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        tapshil = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private void imagestore2(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();
        parwana = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.d("TUSHARphoto", parwana);
    }
    private void imagestore3(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        photo = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }
}