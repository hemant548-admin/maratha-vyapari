package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Forgot_Pass_Verify_Otp extends AppCompatActivity
{
    LinearLayout verifyOtp, back_layout;
    EditText otp1,otp2,otp3,otp4;
    String account_type;
    String number, otp;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__pass__verify__otp);

        verifyOtp = findViewById(R.id.verify_otp);
        back_layout = findViewById(R.id.back_layout);
        otp1 = findViewById(R.id.otp_1);
        otp2 = findViewById(R.id.otp_2);
        otp3 = findViewById(R.id.otp_3);
        otp4 = findViewById(R.id.otp_4);
        progressDialog = new ProgressDialog(this);

        account_type = getIntent().getStringExtra("account_type");
        number = getIntent().getStringExtra("number");

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                verifyOTP();
            }
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(otp1.getText().toString().length()==1)     //size as per your requirement
                {
                    otp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(otp2.getText().toString().length()==1)     //size as per your requirement
                {
                    otp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(otp2.getText().toString().length()==0)     //size as per your requirement
                {
                    otp1.requestFocus();
                }

            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(otp3.getText().toString().length()==1)     //size as per your requirement
                {
                    otp4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(otp3.getText().toString().length()==0)     //size as per your requirement
                {
                    otp2.requestFocus();
                }
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(otp4.getText().toString().length()==0)     //size as per your requirement
                {
                    otp3.requestFocus();
                }
            }
        });


    }

    private void verifyOTP()
    {
        String first,second,third,fourth;
        first = otp1.getText().toString();
        second = otp2.getText().toString();
        third = otp3.getText().toString();
        fourth = otp4.getText().toString();
        otp = first+second+third+fourth;

        if (first.isEmpty())
        {
            Toast.makeText(this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
        }
        else if (second.isEmpty())
        {
            Toast.makeText(this,"Please enter OTP first" , Toast.LENGTH_SHORT).show();
        }
        else if (third.isEmpty())
        {
            Toast.makeText(this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
        }
        else if (fourth.isEmpty())
        {
            Toast.makeText(this,"Please enter OTP first", Toast.LENGTH_SHORT).show();
        }
        else
        {
            if (account_type.equals("business"))
            {
                businessForgotPass();
            }
            else if (account_type.equals("service"))
            {

                serviceForgotPass();
            }
            else if (account_type.equals("user"))
            {
                userForgotPass();
            }

        }
    }

    private void userForgotPass() {
        progressDialog.setTitle("Verifying");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.VERIFY_OTP_USERFORGOTVERIFY_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(Forgot_Pass_Verify_Otp.this,UpdatePassword.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("user_message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Forgot_Pass_Verify_Otp.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("otp",otp);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void serviceForgotPass() {
        progressDialog.setTitle("Verifying");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.VERIFY_OTP_FORGOTVERIFY_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(Forgot_Pass_Verify_Otp.this,UpdatePassword.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("user_message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Forgot_Pass_Verify_Otp.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("otp",otp);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void businessForgotPass() {
        progressDialog.setTitle("Verifying");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.VERIFY_OTP_BUSINESS_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        Intent intent = new Intent(Forgot_Pass_Verify_Otp.this,UpdatePassword.class);
                        intent.putExtra("number",number);
                        intent.putExtra("account_type",account_type);
                        startActivity(intent);
                        finish();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("user_message");
                        Toast.makeText(Forgot_Pass_Verify_Otp.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Forgot_Pass_Verify_Otp.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("mobile", number);
                params.put("otp",otp);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

}