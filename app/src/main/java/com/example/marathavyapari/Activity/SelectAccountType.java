package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.example.marathavyapari.R;

public class SelectAccountType extends AppCompatActivity
{
    LinearLayout businessAcount,serviceAccount,userAccount;
    String account_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_account_type);

        businessAcount = findViewById(R.id.select_account_business);
        serviceAccount = findViewById(R.id.select_account_service);
        userAccount = findViewById(R.id.select_account_user);

        businessAcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                 account_type = "business";

                gotoLoginActivity(account_type);

            }
        });

        serviceAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                account_type = "service";

                gotoLoginActivity(account_type);
            }
        });

        userAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                account_type = "user";

                gotoLoginActivity(account_type);
            }
        });
    }

    private void gotoLoginActivity(String account_type)
    {
        Intent intent = new Intent(SelectAccountType.this,LoginActivity.class);
        intent.putExtra("account_type",account_type);
        startActivity(intent);
    }
}