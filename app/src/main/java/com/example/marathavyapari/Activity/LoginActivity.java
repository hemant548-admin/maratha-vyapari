package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.PendingApplicationBusiness2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.UserMainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity
{
    LinearLayout login_btn,register_btn,skip_btn,back_btn;
    EditText getNumber,getPassword;
    TextView forgotPass;
    String account_type;
    SharedPrefManager sharedPrefManager;
    ImageView visibility;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_btn = findViewById(R.id.login_layout);
        register_btn = findViewById(R.id.login_register_layout);
        skip_btn = findViewById(R.id.skip_layout);
        back_btn = findViewById(R.id.back_layout);
        getNumber = findViewById(R.id.login_number_ed);
        getPassword = findViewById(R.id.login_password_ed);
        forgotPass = findViewById(R.id.login_forgot_text);
        visibility = findViewById(R.id.visibility);
        progressDialog = new ProgressDialog(this);

        sharedPrefManager = new SharedPrefManager(LoginActivity.this);
        account_type = getIntent().getStringExtra("account_type");

        if (account_type.equals("user"))
        {
            skip_btn.setVisibility(View.VISIBLE);
        }

        skip_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LoginActivity.this, UserMainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                loginuser();
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                registerUser();
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updatePassword();
            }
        });

       visibility.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ShowHidePass(v);
           }
       });

    }

    public void ShowHidePass(View view) {

        if(view.getId()==R.id.visibility){
            if(getPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }




    private void loginuser()
    {
        String number = getNumber.getText().toString();
        String password = getPassword.getText().toString();

        if (number.isEmpty())
        {
            String msg = "Please Enter your number";
            getNumber.setError(msg);
            getNumber.requestFocus();
        }
        else if (number.length() != 10)
        {
            getNumber.setError("Please enter 10 digit number");
            getNumber.requestFocus();
        }
        else if (password.isEmpty())
        {
            getPassword.setError("Please enter password");
            getPassword.requestFocus();
        }
        else if (password.length() < 6)
        {
            getPassword.setError("Please enter password more than 6 char");
            getPassword.requestFocus();
        }
        else
        {
            if (account_type.equals("business"))
            {
                businessLogin(number,password);
            }
            else if (account_type.equals("service"))
            {
               serviceLogin(number,password);
            }
            else if (account_type.equals("user"))
            {
                UserLogin(number,password);
            }

        }

    }

    private void UserLogin(String number, String password) {
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.LOGIN_USERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        UserProfileModel userProfileModel = new UserProfileModel(
                                jsonObject.getString("token"),
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("user_photo"),
                                obj.getString("district_name"),
                                obj.getString("taluka_name")
                        );
                        SharedPrefManager.getInstance(getApplicationContext()).userProfilelogin(userProfileModel);
                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), LoginActivity.this);
                        sharedPrefManager.setToken("Token", jsonObject.getString("token"), LoginActivity.this);
                        sharedPrefManager.setMobile("Mobile", obj.getString("uus_mobile"), LoginActivity.this);
                        sharedPrefManager.setEmail("Email", obj.getString("user_email"), LoginActivity.this);
                        sharedPrefManager.setUserName("UserName", obj.getString("user_name"), LoginActivity.this);
                        sharedPrefManager.setUserId("user_id", obj.getString("uus_id"), LoginActivity.this);
                        sharedPrefManager.setUserPhoto("userphoto", obj.getString("user_photo"), LoginActivity.this);
                        Intent servicelogin = new Intent(LoginActivity.this,UserMainActivity.class);
                        servicelogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(servicelogin);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("username", number);
                params.put("password", password);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void serviceLogin(String number, String password) {
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.LOGIN_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);


                        ServiceLoginModel serviceuser= new ServiceLoginModel(
                                jsonObject.getString("token"),
                                obj.getString("sus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("sus_mobile"),
                                obj.getString("sus_name"),
                                obj.getString("name"),
                                obj.getString("service_address"),
                                obj.getString("service_description"),
                                obj.getString("service_type"),
                                obj.getString("licence"),
                                obj.getString("service_status"),
                                obj.getString("sc_id"),
                                obj.getString("sc_name"),
                                obj.getString("live_status"),
                                obj.getString("ssc_name")
                        );

                        SharedPrefManager.getInstance(getApplicationContext()).userserviceLogin(serviceuser);

                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), LoginActivity.this);
                        sharedPrefManager.setToken("Token", jsonObject.getString("token"), LoginActivity.this);


                        Intent servicelogin = new Intent(LoginActivity.this,PendingApplicationBusiness2.class);
                        servicelogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(servicelogin);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("username", number);
                params.put("password", password);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void businessLogin(String number, String password)
    {
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.LOGIN_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        BusinessUserModel user = new BusinessUserModel(
                                jsonObject.getString("token"),
                                obj.getString("bus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("bus_mobile"),
                                obj.getString("bus_name"),
                                obj.getString("owner_name"),
                                obj.getString("business_address"),
                                obj.getString("business_description"),
                                obj.getString("business_type"),
                                obj.getString("detail_info"),
                                obj.getString("licence"),
                                obj.getString("photo"),
                                obj.getString("business_status"),
                                obj.getString("business_email"),
                                obj.getString("bc_name"),
                                obj.getString("live_status"),
                                obj.getString("bsc_name")
                        );

                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), LoginActivity.this);
                        Intent login = new Intent(LoginActivity.this,PendingApplicationBusiness.class);
                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("username", number);
                params.put("password", password);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void registerUser()
    {
            Intent register = new Intent(LoginActivity.this,BusinessRegisterActivity.class);
            register.putExtra("account_type",account_type);
            startActivity(register);
    }

    private void updatePassword()
    {
        Intent updatepassword = new Intent(LoginActivity.this,ForgotPassword.class);
        updatepassword.putExtra("account_type",account_type);
        startActivity(updatepassword);
    }
}