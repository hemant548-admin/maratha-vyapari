package com.example.marathavyapari.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.R;

import java.util.Locale;

public class ChangeLanguage extends AppCompatActivity
{
    LinearLayout marathi,english;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TextView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);

        loadlocale();
        localisation();


        marathi = findViewById(R.id.marathi_layout);
        english = findViewById(R.id.english_layout);
        back = findViewById(R.id.select_lang_back);

        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                editor.putString("Language", "en").commit();
                localisation();
                recreate();
                Intent intent = new Intent(ChangeLanguage.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                editor.putString("Language", "en").commit();
                startActivity(intent);
                finish();
            }
        });

        marathi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                editor.putString("Language", "mr").commit();
                localisation();
                recreate();
                Intent intent = new Intent(ChangeLanguage.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                editor.putString("Language", "mr").commit();
                startActivity(intent);
                finish();
            }
        });

    }

    public void localisation() {
        Locale locale = new Locale(preferences.getString("Language",""));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        this.getResources().updateConfiguration(config,
                this.getResources().getDisplayMetrics());
    }


    public void loadlocale(){
        preferences = getSharedPreferences("Language", Activity.MODE_PRIVATE);
        editor = preferences.edit();
    }
}
