package com.example.marathavyapari.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Adapter.BusinessReviewAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessReviewModel;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ShowBusinessReviewModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MyReviews extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    List<BusinessReviewModel> businessReviewList;
    RecyclerView recyclerView;
    TextView title, address, norating;
    BusinessReviewAdapter businessReviewAdapter;
    RatingBar ratingBar;
    ImageView image,noDataFound,back;
    String token, businessId, Title, Address, NoRating;
    ProgressDialog progressDialog;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reviews);

        toolbar = findViewById(R.id.review_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();
        token = userModel.getToken();
        businessId = userModel.getBus_id();
        progressDialog = new ProgressDialog(MyReviews.this);
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.myreview);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        title = findViewById(R.id.texttitle);
        address = findViewById(R.id.text_address);
        norating = findViewById(R.id.ratingcount);
        ratingBar = findViewById(R.id.show_rating);
        image = findViewById(R.id.post_image);
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        recyclerView = findViewById(R.id.myreview_rv);
        businessReviewList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBusinessReview(businessId);

    }

    private void getBusinessReview(String businessId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.REVIEW_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        businessReviewList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("review");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            businessReviewList.add(new BusinessReviewModel(
                                    product.getString("br_id"),
                                    product.getString("user_id_fk"),
                                    product.getString("business_id_fk"),
                                    product.getString("review_message"),
                                    product.getString("review_date"),
                                    product.getString("review_time"),
                                    product.getString("created_at"),
                                    product.getString("bus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("bus_mobile"),
                                    product.getString("bus_name"),
                                    product.getString("owner_name"),
                                    product.getString("business_address"),
                                    product.getString("business_description"),
                                    product.getString("business_type"),
                                    product.getString("detail_info"),
                                    product.getString("licence"),
                                    product.getString("photo"),
                                    product.getString("business_latitude"),
                                    product.getString("business_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("business_status"),
                                    product.getString("business_email"),
                                    product.getString("uus_mobile"),
                                    product.getString("user_email"),
                                    product.getString("user_name"),
                                    product.getString("user_photo")
                            ));
                        }
                        String imagepath = businessReviewList.get(0).getPhoto();

                        //loading the image
                        String imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        title.setText(businessReviewList.get(0).getBus_name());
                        address.setText(businessReviewList.get(0).getBusiness_address());
                        ratingBar.setRating(Float.parseFloat(businessReviewList.get(0).getAvrage_rating()));
                        norating.setText(businessReviewList.get(0).getNo_of_rating());
                        businessReviewAdapter = new BusinessReviewAdapter(MyReviews.this,businessReviewList);
                        recyclerView.setAdapter(businessReviewAdapter);
                        businessReviewAdapter.notifyDataSetChanged();

                        if (businessReviewAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {
                        noDataFound.setVisibility(View.VISIBLE);
                        String message = jsonObject.getString("message");
                      //  Toast.makeText(MyReviews.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }

                } catch (JSONException e) {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                Toast.makeText(MyReviews.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessId);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}