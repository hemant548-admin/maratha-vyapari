package com.example.marathavyapari.Base_Url;

public class Base_Url
{



    private static final String BASE_URL = "http://marathavyapari.com/maratha/api/business/";
    private static final String BASE_SERVICE_URL = "http://marathavyapari.com/maratha/api/services/";
    private static final String BASE_USER_URL = "http://marathavyapari.com/maratha/api/users/";

    public static final String imagepath = "http://marathavyapari.com/maratha/";

    public static final String SEND_OTP_BUSINESS = BASE_URL + "sendBusinessRegisterOtp";
    public static final String SEND_OTP_BUSINESS_PASSWORD = BASE_URL + "sendBusinessPasswordOtp";
    public static final String VERIFY_OTP_BUSINESS = BASE_URL + "varifyBusinessRegisterOtp";
    public static final String VERIFY_OTP_BUSINESS_PASSWORD = BASE_URL + "varifyBusinessPasswordOtp";
    public static final String GET_BUSINESS_CATLIST = BASE_URL + "businessCategory";
    public static final String GET_BUSINESS_SUBCATLIST = BASE_URL + "businessSubCategory";
    public static final String REGISTER_BUSINESS = BASE_URL + "registerBusiness";
    public static final String LOGIN_BUSINESS = BASE_URL + "loginBusiness";
    public static final String UPDATE_BUSINESS_PASSWORD = BASE_URL + "updateBusinessPassword";
    public static final String GET_BUSINESS_PROFILE = BASE_URL + "getbusinessProfile";
    public static final String CHANGEBUSINESSPASSWORD = BASE_URL + "changeBusinessPassword";
    public static final String UPDATEBUSINESSPROFILE = BASE_URL + "updateBusiness";
    public static final String POST_BUSINESS = BASE_URL + "businessPost";
    public static final String POST_BUSINESS_DELETE = BASE_URL + "businessPostDelete";
    public static final String POST_BUSINESS_EDIT = BASE_URL + "businessPostUpdate";
    public static final String ADD_RULES_BUSINESS = BASE_URL + "businessRule";
    public static final String BUSINESS_RULE_UPDATE = BASE_URL + "businessRuleUpdate";
    public static final String BUSINESS_RULE_DELETE = BASE_URL + "businessRuleDelete";
    public static final String BUSINESS_SEND_CONTACTMSG = BASE_URL + "updateBusinessContact";
    public static final String GET_BANNER = "http://marathavyapari.com/maratha/api/common/getBanner";
    public static final String GET_PRIVACYPOLICY = "http://marathavyapari.com/maratha/api/common/getPrivacy";
    public static final String GET_BUSINESS_SHOPIMAGES = BASE_URL + "getBusinessAllPhoto";
    public static final String GET_UPLOAD_BUSINESS_SHOPIMAGES = BASE_URL + "uploadBusinessPhoto";
    public static final String GET_SERVICE_REGISTER = BASE_SERVICE_URL + "registerService";
    public static final String GET_SERVICE_CATEGORY = BASE_SERVICE_URL + "serviceCategory";
    public static final String GET_SERVICE_SUBCATLIST = BASE_SERVICE_URL + "serviceSubCategory";
    public static final String SEND_OTP_SERVICE = BASE_SERVICE_URL + "sendServicesRegisterOtp";
    public static final String VERIFY_OTP_SERVICE = BASE_SERVICE_URL + "varifyServiceRegisterOtp";
    public static final String LOGIN_SERVICES = BASE_SERVICE_URL + "loginService" ;
    public static final String ADDSERVICEPOST = BASE_SERVICE_URL + "servicePost";
    public static final String ALL_POST = BASE_SERVICE_URL +"servicePost";
    public static final String SEND_OTP_SERVICES_PASSWORD = BASE_SERVICE_URL + "sendServicePasswordOtp";
    public static final String VERIFY_OTP_FORGOTVERIFY_PASSWORD = BASE_SERVICE_URL + "varifyServicePasswordOtp";
    public static final String UPDATE_SERVICE_PASSWORD = BASE_SERVICE_URL + "updateServicePassword" ;
    public static final String GET_SERVICE_PROFILE = BASE_SERVICE_URL + "getServiceProfile";
    public static final String ADD_RULES_SERVICES = BASE_SERVICE_URL+ "serviceRule";
    public static final String ADD_SERVICESRULE = BASE_SERVICE_URL + "serviceRule";
    public static final String POST_SERVICE_DELETE =BASE_SERVICE_URL+ "servicePostDelete";
    public static final String POST_SERVICE_EDIT =BASE_SERVICE_URL + "servicePostUpdate";
    public static final String SERVICEUPDATED = BASE_SERVICE_URL + "updateService";
    public static final String CHANGESERVICESPASSWORD = BASE_SERVICE_URL + "changeServicePassword";
    public static final String GET_UPLOAD_SERVICEIMAGE = BASE_SERVICE_URL + "serviceImagesUpload" ;
    public static final String GET_SERVICE_SHOPIMAGES = BASE_SERVICE_URL + "serviceImages" ;
    public static final String SERVICE_RULE_DELETE = BASE_SERVICE_URL + "serviceRuleDelete";
    public static final String SERVICE_RULE_UPDATE = BASE_SERVICE_URL + "serviceRuleUpdate";
    public static final String SERVICE_SEND_CONTACTMSG = BASE_SERVICE_URL + "addServiceContact";
    public static final String GET_DISTRICTS ="http://marathavyapari.com/maratha/api/common/getDistrict";
    public static final String SEND_OTP_USER = BASE_USER_URL + "sendUsersRegisterOtp";
    public static final String VERIFY_OTP_USER =BASE_USER_URL + "varifyUsersRegisterOtp";
    public static final String GET_USER_REGISTER = BASE_USER_URL + "registerUser";
    public static final String LOGIN_USERS = BASE_USER_URL + "loginUser";
    public static final String SEND_OTP_USER_PASSWORD =BASE_USER_URL + "sendUserPasswordOtp" ;
    public static final String VERIFY_OTP_USERFORGOTVERIFY_PASSWORD = BASE_USER_URL + "varifyUserPasswordOtp";
    public static final String UPDATE_USER_PASSWORD = BASE_USER_URL + "updateUserPassword";
    public static final String USER_SEND_CONTACTMSG = BASE_USER_URL + "addContactUs";
    public static final String CHANGEUSERSPASSWORD = BASE_USER_URL +"changeUserPassword";
    public static final String USEREDITPROFILE = BASE_USER_URL +"updateUserProfile";
    public static final String GET_BUSINESSLIST = BASE_URL + "businessCategory?";
    public static final String GET_SERVICELIST = BASE_SERVICE_URL+"serviceCategory?";
    public static final String GET_SUBSERVICELIST = BASE_SERVICE_URL +"ServiceSubCategory";
    public static final String GETUSERPROFILE = BASE_USER_URL + "getUserProfile";
    public static final String ADD_REVIEWS_SERVICES =BASE_USER_URL + "addServiceReview" ;
    public static final String ADD_REVIEWS_BUSINESS = BASE_USER_URL + "addBusinessReview";
    public static final String ADD_RATING_SERVICES = BASE_USER_URL + "giveServiceRating";
    public static final String ADD_RATING_BUSINESS = BASE_USER_URL +"giveBusinessRating";
    public static final String SHOW_REVIEW_SERVICES = BASE_USER_URL + "getServiceAllReview";
    public static final String FAV_BUSINESS = BASE_USER_URL + "addBusinessToFav";
    public static final String UNFAV_BUSINESS = BASE_USER_URL + "removeBusinessFromFav";
    public static final String FAV_SERVICES = BASE_USER_URL + "addServiceToFav";
    public static final String UNFAV_SERVICES = BASE_USER_URL +"removeServiceFromFav";
    public static final String USER_FAVOURITE = BASE_USER_URL+"getAllFavList";
    public static final String USER_BUSINESS_MYTRANSACTION = BASE_USER_URL + "addBusinessToTransaction";
    public static final String USER_BUSINESS_UNMYTRANSACTION = BASE_USER_URL +"addBusinessToTransaction";
    public static final String USER_SERVICES_UNMYTRANSACTION = BASE_USER_URL + "removeServiceFromTrans";
    public static final String USER_SERVICE_MYTRANSACTION = BASE_USER_URL + "addServiceToTransaction";
    public static final String USER_TRANSACTIONLIST = BASE_USER_URL + "getAllTransList";
    public static final String REVIEW_BUSINESS = BASE_URL + "getBusinessReview";
    public static final String REVIEW_SERVICE = BASE_SERVICE_URL + "getServiceReview";
    public static final String SHOWDETAILS = BASE_USER_URL + "getPostDetailById";
    public static final String SERVICEFCMUPDATED =BASE_SERVICE_URL + "updateServiceFcmToken" ;
    public static final String BUSINESSFCMUPDATED =  BASE_URL +"updateBusinessFcmToken";
    public static final String USERFCMUPDATED = BASE_USER_URL +"updateUserFcmToken" ;
    public static final String USERNOTIFICATION = BASE_USER_URL+ "getUserNotification" ;
    public static final String USERPROFILEUPDATED = BASE_USER_URL + "updateUserProfilePhoto";
    public static final String HOMESEARCHKEY = BASE_USER_URL + "homePageSearch" ;
}
