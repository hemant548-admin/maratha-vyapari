package com.example.marathavyapari.User;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.PendingApplicationBusiness;
import com.example.marathavyapari.Activity.RegisterBusiness2;
import com.example.marathavyapari.Activity.UploadBusinessDoc;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.SearchBusinessAdapter;
import com.example.marathavyapari.Adapter.SearchServiceAdapter;
import com.example.marathavyapari.Adapter.SearchSubBusinessListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessSearchModel;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.SearchResultModel;
import com.example.marathavyapari.Model.ServiceSearchModel;
import com.example.marathavyapari.Model.SubBusinessDataModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchSreenActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back,noDataFound;
    RecyclerView recyclerView;
    EditText serachcat;
    ApiClient apiClient;
    String districtid, talukaid, DistrictsName, TalukaName, sub_category_id, token, businessId,lang,serviceid;
    Spinner type,districts, taluka;
    ArrayList<DistrictModel.DistrictsList> districtsList;
    ArrayList<TalukaDataModel.TalukaList> talukaList;
    SharedPrefManager sharedPrefManager;
    private static final String[] Types = {"Business/व्यवसाय", "Service/सेवा"};
    ArrayList<SearchResultModel.BusinessList> busineslist;
    ArrayList<SearchResultModel.ServiceList> serviceLists;
    //List<BusinessSearchModel> busineslist;
    //List<ServiceSearchModel> serviceLists;
    SearchBusinessAdapter searchBusinessAdapter;
    SearchServiceAdapter searchServiceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serachsreenactivity);

        toolbar = findViewById(R.id.contactus_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.app_name);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        apiClient = new ApiClient();
      /*  districts = findViewById(R.id.spinner_dist);
        taluka = findViewById(R.id.spinner_taluka);
        type = findViewById(R.id.spinnertype);*/
        recyclerView = findViewById(R.id.businesslist);
        noDataFound = findViewById(R.id.nodata_found);
        serachcat = findViewById(R.id.searchcategory);
        serachcat.requestFocus();
        districtsList = new ArrayList<>();
        serviceLists = new ArrayList<>();
        busineslist = new ArrayList<>();
        talukaList = new ArrayList<>();  sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getToken("Token",SearchSreenActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", SearchSreenActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        serachcat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
             //   progressBar.setVisibility(View.VISIBLE);
                String search_key = charSequence.toString();
                Log.d("TusharSearchKey", search_key);
                getSearchAllPost(search_key);
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
               // progressBar.setVisibility(View.VISIBLE);
                String search_key = serachcat.getText().toString();
                Log.e("TusharSearchKey", search_key);
                getSearchAllPost(search_key);
            }
        });
    }

    private void getSearchAllPost(String search_key) {
        apiClient.apiInterface.ShowSearchResult(search_key).enqueue(new Callback<SearchResultModel>() {
            @Override
            public void onResponse(Call<SearchResultModel> call, retrofit2.Response<SearchResultModel> response) {
                if (response.isSuccessful()) {
                    try {
                        noDataFound.setVisibility(View.GONE);
                        busineslist = response.body().getBusiness();
                        serviceLists = response.body().getService();
                        searchBusinessAdapter = new SearchBusinessAdapter(SearchSreenActivity.this, busineslist);
                        recyclerView.setAdapter(searchBusinessAdapter);
                        searchBusinessAdapter.notifyDataSetChanged();
                        searchServiceAdapter = new SearchServiceAdapter(SearchSreenActivity.this, serviceLists);
                        recyclerView.setAdapter(searchServiceAdapter);
                        searchBusinessAdapter.notifyDataSetChanged();
                        if (searchServiceAdapter.getItemCount() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else if (searchBusinessAdapter.getItemCount() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    }catch (Exception e)
                    {
                        Toast.makeText(SearchSreenActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }


                }
            }@Override
            public void onFailure(Call<SearchResultModel> call, Throwable t) {

            }
        });

      /* StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.HOMESEARCHKEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equals("true")) {
                        String message = jsonObject.getString("message");
                        JSONArray array = jsonObject.getJSONArray("business");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            busineslist.add(new BusinessSearchModel(
                                    product.getString("bus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("subcategory_id_fk"),
                                    product.getString("district_id_fk"),
                                    product.getString("taluka_id_fk"),
                                    product.getString("bus_mobile"),
                                    product.getString("bus_name"),
                                    product.getString("updated_at"),
                                    product.getString("created_at"),
                                    product.getString("owner_name"),
                                    product.getString("business_address"),
                                    product.getString("business_description"),
                                    product.getString("business_type"),
                                    product.getString("detail_info"),
                                    product.getString("licence"),
                                    product.getString("photo"),
                                    product.getString("business_latitude"),
                                    product.getString("business_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("business_status"),
                                    product.getString("business_email"),
                                    product.getString("live_status"),
                                    product.getString("language"),
                                    product.getString("fcm_token"),
                                    product.getString("bc_id"),
                                    product.getString("bc_name"),
                                    product.getString("bc_image"),
                                    product.getString("bsc_id"),
                                    product.getString("bc_id_fk"),
                                    product.getString("bsc_name"),
                                    product.getString("bsc_image"),
                                    product.getString("district_id"),
                                    product.getString("district_name"),
                                    product.getString("taluka_id"),
                                    product.getString("taluka_name")
                            ));
                        }

                        searchBusinessAdapter = new SearchBusinessAdapter(SearchSreenActivity.this, busineslist);
                        recyclerView.setAdapter(searchBusinessAdapter);
                        searchBusinessAdapter.notifyDataSetChanged();

                        JSONArray array1 = jsonObject.getJSONArray("service");

                        for (int i = 0; i < array1.length(); i++) {
                            JSONObject product = array1.getJSONObject(i);

                            //adding the product to product list
                            serviceLists.add(new ServiceSearchModel(
                                    product.getString("sus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("subcategory_id_fk"),
                                    product.getString("district_id_fk"),
                                    product.getString("taluka_id_fk"),
                                    product.getString("sus_mobile"),
                                    product.getString("sus_name"),
                                    product.getString("updated_at"),
                                    product.getString("created_at"),
                                    product.getString("name"),
                                    product.getString("service_address"),
                                    product.getString("service_description"),
                                    product.getString("service_type"),
                                    product.getString("licence"),
                                    product.getString("service_latitude"),
                                    product.getString("service_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("language"),
                                    product.getString("service_status"),
                                    product.getString("fcm_token"),
                                    product.getString("sc_id"),
                                    product.getString("sc_name"),
                                    product.getString("sc_image"),
                                    product.getString("live_status"),
                                    product.getString("ssc_id"),
                                    product.getString("sc_id_fk"),
                                    product.getString("ssc_name"),
                                    product.getString("ssc_image"),
                                    product.getString("district_id"),
                                    product.getString("district_name"),
                                    product.getString("taluka_id"),
                                    product.getString("taluka_name")
                            ));
                        }

                        searchServiceAdapter = new SearchServiceAdapter(SearchSreenActivity.this, serviceLists);
                        recyclerView.setAdapter(searchServiceAdapter);
                        searchBusinessAdapter.notifyDataSetChanged();
                        if (searchBusinessAdapter.getItemCount() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else if (searchBusinessAdapter.getItemCount() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }

                    } else {

                        String message = jsonObject.getString("message");
                        Toast.makeText(SearchSreenActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN" + message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error)
           {

               recyclerView.setVisibility(View.VISIBLE);

               Toast.makeText(SearchSreenActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

           }
       }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("search_key", search_key);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);*/
    }
}
