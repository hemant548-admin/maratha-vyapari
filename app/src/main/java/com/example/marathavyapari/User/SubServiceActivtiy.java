package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.MySubServiceListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceRegister2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class SubServiceActivtiy extends AppCompatActivity {

    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ProgressDialog progressDialog;
    ImageView noDataFound;
    ProgressBar progressBar;
    SharedPrefManager sharedPrefManager;
    RecyclerView recyclerView;
    MySubServiceListAdapter mySubServiceListAdapter;
    ArrayList<ServiceSubCatModel.SubCategoryList> subservicelist;
    String category_id, sub_category_id, lang;
    ApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businessactivity);
        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.service);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
      /*  progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);*/
        recyclerView = findViewById(R.id.businesslist);

        subservicelist = new ArrayList<>();
        category_id = getIntent().getStringExtra("category_id");
        Log.d("TusharCate", category_id);

        sharedPrefManager = new SharedPrefManager(SubServiceActivtiy.this);
        lang = sharedPrefManager.getLanguage("Lang", SubServiceActivtiy.this);
        apiClient = new ApiClient();
        getSubService(category_id, lang);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {
                        sub_category_id = subservicelist.get(position).getSsc_id();


                        Intent catlist = new Intent(SubServiceActivtiy.this, SearchSubServiceActivity.class);
                        catlist.putExtra("sub_category_id",sub_category_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
    }

    private void getSubService(String category_id, String lang) {

        apiClient.apiInterface.showSubCat(category_id, lang).enqueue(new Callback<ServiceSubCatModel>() {
            @Override
            public void onResponse(Call<ServiceSubCatModel> call, retrofit2.Response<ServiceSubCatModel> response) {
                if (response.isSuccessful()) {
                   try {

                       subservicelist = response.body().getSub_category();
                       GridLayoutManager gridLayoutManager = new GridLayoutManager(SubServiceActivtiy.this, 3);
                       recyclerView.setLayoutManager(gridLayoutManager);
                       mySubServiceListAdapter = new MySubServiceListAdapter(SubServiceActivtiy.this, subservicelist);
                       recyclerView.setAdapter(mySubServiceListAdapter);
                       mySubServiceListAdapter.notifyDataSetChanged();

                   }catch (Exception e)
                   {

                    }

                }
            }@Override
            public void onFailure(Call<ServiceSubCatModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
