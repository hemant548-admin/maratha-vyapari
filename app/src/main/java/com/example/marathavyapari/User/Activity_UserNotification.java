package com.example.marathavyapari.User;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.SearchSubServiceListAdapter;
import com.example.marathavyapari.Adapter.ShowNotifyAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.SubServiceDataModel;
import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class Activity_UserNotification extends AppCompatActivity {

    Toolbar toolbar;
    ImageView noDataFound;
    ProgressBar progressBar;
    ShowNotifyAdapter showNotifyAdapter;
    RecyclerView mypost_rv;
    String token, lang, user_id;
    SharedPrefManager sharedPrefManager;
    ApiClient apiClient;
    ArrayList<UserNotificationModel.NotificationList> notificationList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notification);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        apiClient = new ApiClient();
        sharedPrefManager = new SharedPrefManager(Activity_UserNotification.this);
        lang = sharedPrefManager.getLanguage("Lang", Activity_UserNotification.this);
        user_id = sharedPrefManager.getUserId("user_id", Activity_UserNotification.this);
        if(SharedPrefManager.getInstance(Activity_UserNotification.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(Activity_UserNotification.this, SelectAccountType.class));
            return;
        }
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        mypost_rv = findViewById(R.id.mypost_rv);
        notificationList = new ArrayList<>();
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        showUserNotification(user_id, lang);
    }

    private void showUserNotification(String user_id, String lang) {
        apiClient.apiInterface.showUserNotify(user_id,lang).enqueue(new Callback<UserNotificationModel>() {
            @Override
            public void onResponse(Call<UserNotificationModel> call, retrofit2.Response<UserNotificationModel> response) {
                if (response.isSuccessful()) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        notificationList = response.body().getNotification();
                        showNotifyAdapter = new ShowNotifyAdapter(Activity_UserNotification.this, notificationList);
                        mypost_rv.setAdapter(showNotifyAdapter);
                        showNotifyAdapter.notifyDataSetChanged();
                        if (showNotifyAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            mypost_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }@Override
            public void onFailure(Call<UserNotificationModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
