package com.example.marathavyapari.User;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.PostDetailsActivity;
import com.example.marathavyapari.Activity.RegisterBusiness2;
import com.example.marathavyapari.Adapter.BusinessListAdapter;
import com.example.marathavyapari.Adapter.MyBusinessListAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessCatModel;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceRegister2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class BusinessActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ProgressDialog progressDialog;
    ImageView noDataFound;
    ProgressBar progressBar;
    SharedPrefManager sharedPrefManager;
    RecyclerView recyclerView;
    MyBusinessListAdapter myBusinessListAdapter;
    ArrayList<BusinessCategoryModel.CategoryList> businesslist;
    String category_id, lang;
    ApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businessactivity);

        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.business);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        businesslist = new ArrayList<>();
        apiClient = new ApiClient();
        recyclerView = findViewById(R.id.businesslist);
        sharedPrefManager = new SharedPrefManager(BusinessActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", BusinessActivity.this);
       // recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBusinessCategory(lang);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {
                       category_id = businesslist.get(position).getBc_id();

                        Intent catlist = new Intent(BusinessActivity.this, SubBusinessActivity.class);
                        catlist.putExtra("category_id",category_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

    }
    private void getBusinessCategory(String lang) {

        apiClient.apiInterface.showBusinessCategory(lang).enqueue(new Callback<BusinessCategoryModel>() {
            @Override
            public void onResponse(Call<BusinessCategoryModel> call, retrofit2.Response<BusinessCategoryModel> response) {
                if (response.isSuccessful()) {

                    businesslist = response.body().getCategory();
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(BusinessActivity.this,3);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    myBusinessListAdapter = new MyBusinessListAdapter(BusinessActivity.this, businesslist);
                    recyclerView.setAdapter(myBusinessListAdapter);
                    myBusinessListAdapter.notifyDataSetChanged();

                }
            }@Override
            public void onFailure(Call<BusinessCategoryModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
