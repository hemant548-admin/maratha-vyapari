package com.example.marathavyapari.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.ContactUs;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.MyReviews;
import com.example.marathavyapari.Activity.MyShop;
import com.example.marathavyapari.Activity.Notifications;
import com.example.marathavyapari.Activity.PostDetailsActivity;
import com.example.marathavyapari.Activity.PrivacyPolicy;
import com.example.marathavyapari.Activity.RegisterBusiness2;
import com.example.marathavyapari.Activity.SelectLanguage;
import com.example.marathavyapari.Activity.TermsAndConditions;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Adapter.BusinessListAdapter;
import com.example.marathavyapari.Adapter.BusinessRelatedpostAdpater;
import com.example.marathavyapari.Adapter.MyBusinessListAdapter;
import com.example.marathavyapari.Adapter.MyFavAdapter;
import com.example.marathavyapari.Adapter.MyServiceListAdapter;
import com.example.marathavyapari.Adapter.SearchSubBusinessListAdapter;
import com.example.marathavyapari.Adapter.ServiceListAdapter;
import com.example.marathavyapari.Adapter.ServiceRelatedPostAdapter;
import com.example.marathavyapari.Adapter.ShowNotifyAdapter;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.Model.BusinessPostModel;
import com.example.marathavyapari.Model.BusinessServiceModel;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ProfileUserModel;
import com.example.marathavyapari.Model.ReadNotiModel;
import com.example.marathavyapari.Model.ResponseFCMModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.Model.ServicePostModel;
import com.example.marathavyapari.Model.ShowBusiServiceModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.Model.SubBusinessDataModel;
import com.example.marathavyapari.Model.UnreadNotificationModel;
import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceMainActivity;
import com.example.marathavyapari.Service.ServiceRegister2;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.marathavyapari.R.drawable.ic_white_color_notification;

public class UserMainActivity extends AppCompatActivity
{
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    TextView shopName, shopAddress, shopNumber;
    EditText searchcat;
    List<SliderItem> imagelist;
    SliderView sliderView;
    ImageView servicemore, businessmore, profile_image;
    SharedPrefManager sharedPrefManager;
    Intent intent;
    String Name, Email, Mobile, token, category_id, fcmregistrationId, user_id, post_id, type, lang, latitude, longitude;
    RecyclerView servicerecyclerview, businessrecyclerview, businesspost, servicepost;
    BusinessRelatedpostAdpater businessRelatedpostAdpater;
    ServiceRelatedPostAdapter serviceRelatedPostAdapter;
    BusinessListAdapter businessListAdapter;
    ServiceListAdapter serviceListAdapter;
    ArrayList<ServicePostModel.PostList> serviceonepost;
    ArrayList<BusinessServiceModel.PostList> businessonepost;
    ArrayList<BusinessCategoryModel.CategoryList> businesslist;
    ArrayList<ServiceCategoryModel.CategoryList> serviceList;
    LinearLayout laymore1, laymore2, busmore;
    ApiClient apiClient;
    private TextView notifCount;
    private String mNotifCount, Photo;
    protected boolean gps_enabled, network_enabled;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private static final int REQUEST_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        setUpToolbar();

        imagelist = new ArrayList<>();

        loadBanners();

        sharedPrefManager = new SharedPrefManager(this);
        Name = sharedPrefManager.getUserName("UserName", UserMainActivity.this);
        Email = sharedPrefManager.getEmail("Email", UserMainActivity.this);
        Mobile = sharedPrefManager.getMobile("Mobile", UserMainActivity.this);
        token = sharedPrefManager.getToken("Token", UserMainActivity.this);
        user_id = sharedPrefManager.getUserId("user_id", UserMainActivity.this);

        Log.d("Tusharuser", user_id);
        navigationView = findViewById(R.id.main_navigationview);
        //navigationView.setCheckedItem(R.id.home);
        navigationView.setItemIconTintList(null);
        serviceList = new ArrayList<>();
        businesslist = new ArrayList<>();
        serviceonepost = new ArrayList<>();
        businessonepost = new ArrayList<>();
        apiClient = new ApiClient();
        businessrecyclerview = findViewById(R.id.businesslist);
        lang = sharedPrefManager.getLanguage("Lang", UserMainActivity.this);
        servicerecyclerview = findViewById(R.id.servicelist);
        servicemore = findViewById(R.id.servicemore);
        businessmore = findViewById(R.id.businessmore);
        businesspost = findViewById(R.id.businespostlist);
        laymore1 = findViewById(R.id.lay_morebusiness);
        searchcat = findViewById(R.id.searchcategory);
        businesspost.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getUnReadNotification(user_id);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmregistrationId = instanceIdResult.getToken();
                Log.d("TUSHARFCMREGISTRATION", fcmregistrationId);

                getSaveData(user_id, fcmregistrationId, lang, latitude, longitude);
            }
        });
        searchcat.setKeyListener(null);
        searchcat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                {
                    searchcat.callOnClick();
                }
            }
        });
        searchcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserMainActivity.this,SearchSreenActivity.class);
                Pair[] pairs = new Pair[1];
                pairs[0] = new Pair<View,String>(searchcat,"first");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(UserMainActivity.this,pairs);
                startActivity(intent,options.toBundle());
            }
        });
        laymore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserMainActivity.this, AllServiceBusinessActivity.class);
                startActivity(intent);
            }
        });
        businesspost.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), businesspost, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {

                        post_id = businessonepost.get(position).getPost_id();
                        type = businessonepost.get(position).getType();
                            Intent catlist = new Intent(UserMainActivity.this, ActivityShowDiscount.class);
                            catlist.putExtra("Type", type);
                            catlist.putExtra("post_id", post_id);
                            startActivity(catlist);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

        getBusinessCategory(lang);
        getServiceCategory(lang);
        getBusinessOnePost();
        servicemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserMainActivity.this,ServiceActivity.class);
                startActivity(intent);
            }
        });
        businessmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserMainActivity.this,BusinessActivity.class);
                startActivity(intent);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {

                    case R.id.my_profile:
                        intent = new Intent(UserMainActivity.this, UserProfileActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.my_transactions:
                         intent = new Intent(UserMainActivity.this, MyTransactionActivity.class);
                         startActivity(intent);
                        break;

                    case R.id.my_favorites:

                        intent = new Intent(UserMainActivity.this, UserFavouriteActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.settings:

                        intent = new Intent(UserMainActivity.this,UserSetting.class);
                        startActivity(intent);
                        break;

                    case R.id.about_us:
                        Intent myshop = new Intent(UserMainActivity.this, AboutUsActivity.class);
                        startActivity(myshop);

                        break;

                    case R.id.notifications:
                        getReadNotification(user_id);
                        Intent notify = new Intent(UserMainActivity.this, Activity_UserNotification.class);
                        startActivity(notify);

                        break;

                    case R.id.contactus:

                        intent = new Intent(UserMainActivity.this, UserContactus.class);
                        startActivity(intent);
                        break;

                    case R.id.privacy:

                        Intent i = new Intent(UserMainActivity.this, UsrePolicy.class);
                        startActivity(i);

                        break;

                    case R.id.rateus:
                        reviewOnApp();

                        break;

                    case R.id.logout:
//                        tokenLogout();
//
//                        finish();
                        new AlertDialog.Builder(UserMainActivity.this)
                                .setTitle("Logout") .setMessage("Do you really want to Logout?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                                        { public void onClick(DialogInterface dialog, int which)
                                        {
                                            SharedPrefManager.getInstance(UserMainActivity.this).logout();
                                            intent = new Intent(UserMainActivity.this, SelectLanguage.class);
                                            intent.putExtra("account_type","user");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                            Toast.makeText(UserMainActivity.this, "Logout successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                        }
                                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss(); } })
                                .setIcon(android.R.drawable.ic_dialog_alert) .show();
                }


                return false;
            }
        });
        View headerView = navigationView.getHeaderView(0);
        profile_image = headerView.findViewById(R.id.profile_image);
        shopName = headerView.findViewById(R.id.user_businessname);
        shopAddress = headerView.findViewById(R.id.user_address);
        shopNumber = headerView.findViewById(R.id.user_contact);
        shopName.setText(Name);
        shopAddress.setText(Email);
        shopNumber.setText(Mobile);

        UserProfileModel userModel = SharedPrefManager.getInstance(this).getuserLogin();
        String imagepath2  = Base_Url.imagepath + userModel.getUser_photo();
        Glide.with(getApplicationContext())
                .load(imagepath2)
                .into(profile_image);
    }

    private void getUnReadNotification(String user_id) {
        apiClient.apiInterface.showUserCount(user_id).enqueue(new Callback<UnreadNotificationModel>() {
            @Override
            public void onResponse(Call<UnreadNotificationModel> call, retrofit2.Response<UnreadNotificationModel> response) {
                if (response.isSuccessful()) {
                    mNotifCount = response.body().getCount();
                    notifCount.setText(mNotifCount);
                }
            }@Override
            public void onFailure(Call<UnreadNotificationModel> call, Throwable t) {

            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                UserMainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                UserMainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.d("TAG", "onResponseTUSHARLATI: " + latitude);
                Log.d("TAG", "onResponseTUSHARLong: " + longitude);

            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void OnGPS() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void getSaveData(String user_id, String fcmregistrationId, String lang, String latitude, String longitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USERFCMUPDATED, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");

                    if(jsonObject.getString("status").equals("true"))
                    {

                        String message = jsonObject.getString("message");
                        //Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+fcmregistrationId);
                        Log.i("TAG", "onResponse: MADAN12"+message);

                    }else
                    {

                        String message = jsonObject.getString("message");
                        // Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                       // Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

             //   Toast.makeText(UserMainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", user_id);
                params.put("fcm_token", fcmregistrationId);
                params.put("language", lang);
                params.put("latitude", latitude);
                params.put("longitude", longitude);


                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void getBusinessOnePost() {
        apiClient.apiInterface.showBusinessServicePost().enqueue(new Callback<BusinessServiceModel>() {
            @Override
            public void onResponse(Call<BusinessServiceModel> call, retrofit2.Response<BusinessServiceModel> response) {
                if (response.isSuccessful()) {

                    businessonepost = response.body().getPost();
                    businessRelatedpostAdpater = new BusinessRelatedpostAdpater(UserMainActivity.this, businessonepost);
                    businesspost.setAdapter(businessRelatedpostAdpater);
                    businessRelatedpostAdpater .notifyDataSetChanged();

                }
            }@Override
            public void onFailure(Call<BusinessServiceModel> call, Throwable t) {

            }
        });
    }


    private void getServiceCategory(String lang) {

        apiClient.apiInterface.showServiceCate(lang).enqueue(new Callback<ServiceCategoryModel>() {
            @Override
            public void onResponse(Call<ServiceCategoryModel> call, retrofit2.Response<ServiceCategoryModel> response) {
                if (response.isSuccessful()) {

                    serviceList = response.body().getCategory();
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(UserMainActivity.this,4);
                    servicerecyclerview.setLayoutManager(gridLayoutManager);
                    serviceListAdapter = new ServiceListAdapter(UserMainActivity.this, serviceList);
                    servicerecyclerview.setAdapter(serviceListAdapter);
                    serviceListAdapter.notifyDataSetChanged();

                }
            }@Override
            public void onFailure(Call<ServiceCategoryModel> call, Throwable t) {

            }
        });
    }

    private void getBusinessCategory(String lang) {

        apiClient.apiInterface.showBusinessCategory(lang).enqueue(new Callback<BusinessCategoryModel>() {
            @Override
            public void onResponse(Call<BusinessCategoryModel> call, retrofit2.Response<BusinessCategoryModel> response) {
                if (response.isSuccessful()) {

                    businesslist = response.body().getCategory();
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(UserMainActivity.this,4);
                    businessrecyclerview.setLayoutManager(gridLayoutManager);
                    businessListAdapter = new BusinessListAdapter(UserMainActivity.this, businesslist);
                    businessrecyclerview.setAdapter(businessListAdapter);
                    businessListAdapter.notifyDataSetChanged();

                }
            }@Override
            public void onFailure(Call<BusinessCategoryModel> call, Throwable t) {

            }
        });
    }
    private void loadBanners()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_BANNER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("banner");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            imagelist.add(new SliderItem(
                                    product.getString("banner_id"),
                                    product.getString("banner_title"),
                                    product.getString("banner_image")
                            ));
                        }

                        SliderAdapterExample adapter = new SliderAdapterExample(UserMainActivity.this,imagelist);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();

                    }
                    else
                    {

                        String message = jsonObject.getString("message");
//                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                Toast.makeText(UserMainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) ;

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


    private void setUpToolbar()
    {
        drawerLayout = findViewById(R.id.main_drawer_layout);
        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle(R.string.app_name);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        sliderView = findViewById(R.id.imageSlider);

    }
    public void reviewOnApp()
    {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent gotorate = new Intent(Intent.ACTION_VIEW, uri);
        gotorate.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(gotorate);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }
    @Override public void onBackPressed() {
        new AlertDialog.Builder(UserMainActivity.this)
                .setTitle("Exit Page") .setMessage("Do you really want to exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                        { public void onClick(DialogInterface dialog, int which)
                        { ActivityCompat.finishAffinity(UserMainActivity.this);
                            finish();
                        }
                        }
                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss(); } })
                .setIcon(android.R.drawable.ic_dialog_alert) .show();

    }
    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_home_menu,menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        notifCount = (TextView) MenuItemCompat.getActionView(item);

        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReadNotification(user_id);
                finish();
                Intent notify = new Intent(UserMainActivity.this, Activity_UserNotification.class);
                startActivity(notify);
                        }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void getReadNotification(String user_id) {
        apiClient.apiInterface.showreadnoti(user_id).enqueue(new Callback<ReadNotiModel>() {
            @Override
            public void onResponse(Call<ReadNotiModel> call, retrofit2.Response<ReadNotiModel> response) {
                if (response.isSuccessful()) {

                }
            }@Override
            public void onFailure(Call<ReadNotiModel> call, Throwable t) {

            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.notification:
                getReadNotification(user_id);
                finish();
                Intent notify = new Intent(UserMainActivity.this, Activity_UserNotification.class);
                startActivity(notify);
                break;
        }
        return true;

    }

}