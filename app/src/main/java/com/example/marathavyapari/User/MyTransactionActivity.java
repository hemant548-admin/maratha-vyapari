package com.example.marathavyapari.User;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Adapter.MyFavAdapter;
import com.example.marathavyapari.Adapter.MyTransAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowFavouriteModel;
import com.example.marathavyapari.Model.ShowTransactionModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTransactionActivity  extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ImageView noDataFound;
    List<ShowTransactionModel> showTransactionList;
    MyTransAdapter myTransAdapter;
    SharedPrefManager sharedPrefManager;
    ProgressBar progressBar;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_transaction);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPrefManager = new SharedPrefManager(MyTransactionActivity.this);
        token = sharedPrefManager.getToken("Token", MyTransactionActivity.this);
        if(SharedPrefManager.getInstance(MyTransactionActivity.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(MyTransactionActivity.this, SelectAccountType.class));
            return;
        }
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        recyclerView = findViewById(R.id.translist);
        showTransactionList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        getTransactionList();

    }

    private void getTransactionList() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.USER_TRANSACTIONLIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        showTransactionList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("transaction");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            showTransactionList.add(new ShowTransactionModel(
                                    product.getString("type"),
                                    product.getString("trans_id"),
                                    product.getString("user_id_fk"),
                                    product.getString("business_id_fk"),
                                    product.getString("service_id_fk"),
                                    product.getString("bus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("bus_mobile"),
                                    product.getString("bus_name"),
                                    product.getString("owner_name"),
                                    product.getString("business_address"),
                                    product.getString("business_description"),
                                    product.getString("business_type"),
                                    product.getString("detail_info"),
                                    product.getString("licence"),
                                    product.getString("photo"),
                                    product.getString("business_latitude"),
                                    product.getString("business_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("business_status"),
                                    product.getString("business_email"),
                                    product.getString("sus_id"),
                                    product.getString("sus_mobile"),
                                    product.getString("sus_name"),
                                    product.getString("name"),
                                    product.getString("service_address"),
                                    product.getString("service_description"),
                                    product.getString("service_type"),
                                    product.getString("service_latitude"),
                                    product.getString("service_longitude"),
                                    product.getString("service_status")
                            ));
                        }

                        myTransAdapter = new MyTransAdapter(MyTransactionActivity.this, showTransactionList);
                        recyclerView.setAdapter(myTransAdapter);
                        myTransAdapter.notifyDataSetChanged();

                        if (myTransAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(MyTransactionActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                Toast.makeText(MyTransactionActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
