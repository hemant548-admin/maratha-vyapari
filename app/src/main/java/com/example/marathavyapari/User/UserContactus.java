package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ProfileUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceContactUs;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.CALL_PHONE;

public class UserContactus extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;
    String token, Name, Address, Number, Email;
    ProgressDialog progressDialog;
    EditText getName,getEmail,getDesc;
    LinearLayout sendLayout;
    LinearLayout callText;
    TextView number, address;
    SharedPrefManager sharedPrefManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        toolbar = findViewById(R.id.contactus_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getToken("Token", UserContactus.this);
        if(SharedPrefManager.getInstance(UserContactus.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(UserContactus.this, SelectAccountType.class));
            return;
        }
        getProfile();
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.usercontactus);
        progressDialog = new ProgressDialog(this);
        number = findViewById(R.id.text_numbar);
        address = findViewById(R.id.text_address);
        number.setText("9890374498");
        address.setText(R.string.adressuser);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getName = findViewById(R.id.contactus_name);
        getEmail = findViewById(R.id.contactus_email);
        getDesc = findViewById(R.id.contactus_desc);
        sendLayout = findViewById(R.id.contactus_send);
        callText = findViewById(R.id.contactus_call_txt);
        callText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (checkPermission())
                {
                    String Number1 = "+919890374498";
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + Number1));
                    startActivity(intentcall);
                }
                else
                {
                    requestPermission();
                }
            }
        });

        sendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail();
            }
        });
    }

    private void getProfile() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GETUSERPROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(UserContactus.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        ProfileUserModel userProfileModel = new ProfileUserModel(
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("district_id_fk"),
                                obj.getString("taluka_id_fk"),
                                obj.getString("user_latitude"),
                                obj.getString("user_longitude"),
                                obj.getString("user_photo"),
                                obj.getString("updated_at"),
                                obj.getString("created_at"),
                                obj.getString("district_id"),
                                obj.getString("district_name"),
                                obj.getString("live_status"),
                                obj.getString("taluka_id"),
                                obj.getString("taluka_name")
                        );

                        Name = userProfileModel.getUser_name();
                        Email = userProfileModel.getUser_email();
                        Number = userProfileModel.getUus_mobile();
                        getName.setText(Name);
                        getEmail.setText(Email);



                    }else
                    {
                        String message = jsonObject.getString("message");
                        //Toast.makeText(UserContactus.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(UserContactus.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private boolean checkPermission()
    {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);

        return result == PackageManager.PERMISSION_GRANTED ;
    }
    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, 200);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 200)
        {
            if (grantResults.length > 0) {
                boolean cameraaccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (cameraaccepted)
                {
                    String number = "+91" +Number;
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + number));
                    startActivity(intentcall);

                }
                else
                {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    private void sendmail()
    {
        String name,email,desc;

        name = getName.getText().toString();
        email = getEmail.getText().toString();
        desc = getDesc.getText().toString();

        if (name.isEmpty())
        {
            getName.setError("Please enter your name");
            getName.requestFocus();
        }
        else if (email.isEmpty())
        {
            getEmail.setError("Please enter your gmail id");
            getEmail.requestFocus();
        } else if (!isEmailValid(email)){
            getEmail.setError("please enter valid email address");
            getEmail.requestFocus();
        }

        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else
        {
            sendMessage(name,email,desc);
        }

    }

    private boolean isEmailValid(String email) {
        Boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void sendMessage(String name, String email, String desc) {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USER_SEND_CONTACTMSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                       // Toast.makeText(UserContactus.this, message, Toast.LENGTH_SHORT).show();
                        getName.getText().clear();
                        getEmail.getText().clear();
                        getDesc.getText().clear();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UserContactus.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UserContactus.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("contact_name", name);
                params.put("contact_email", email);
                params.put("contact_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}
