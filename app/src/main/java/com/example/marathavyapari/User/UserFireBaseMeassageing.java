package com.example.marathavyapari.User;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.example.marathavyapari.R;
import com.example.marathavyapari.Service.ServiceFirebaseMessageing;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;

public class UserFireBaseMeassageing extends FirebaseMessagingService {

    String title,body, sound;
    String imageurl;
    Bitmap bitmap;
    PendingIntent pendingIntent;
    private static final String TAG = "FCM Service";
    private NotificationManager notificationManager;
    private String TRANSACTION_CHANNEL_ID = "transaction_channel_id";
    private static final String TAG1 = UserFireBaseMeassageing.class.getSimpleName();
    int notificationID = 1;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("gcmid", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {

            Map data = remoteMessage.getData();
            title = (String) data.get("title");
            body = (String) data.get("message");
            imageurl = (String) data.get("image_url");

            bitmap = getBitmapfromUrl(imageurl);
            if (imageurl == null) {
                showNotification(title, body);
            }else
            {
                showNotification(title, body , bitmap);
            }



        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.toString();
        }
    }

    private void showNotification(String title, String body) {
        try {

            Intent intent = new Intent(this, UserFireBaseMeassageing.class);
            intent.putExtra("notificationID", notificationID);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(), intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationChannel mChannel = null;
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            }

// Create a notification and set the notification channel.
            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification = new Notification.Builder(this)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSmallIcon(R.drawable.logo)
                        .setChannelId(CHANNEL_ID)
                        .setColor(ContextCompat.getColor(this, R.color.yellow))
                        .setContentIntent(pendingIntent)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setVibrate(new long[] {2000})
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setLights(Color.RED, 3000, 3000)
                        .build();
            }

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
// Issue the notification.
            mNotificationManager.notify(notificationID, notification);
            notificationID++;
        }catch (Exception e){
            e.toString();
        }
    }


    private void showNotification(String title, String body ,  Bitmap bitmap) {
        try {

            Intent intent = new Intent(this, UserFireBaseMeassageing.class);
            intent.putExtra("notificationID", notificationID);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(), intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationChannel mChannel = null;
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            }

// Create a notification and set the notification channel.
            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification = new Notification.Builder(this)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSmallIcon(R.drawable.logo)
                        .setChannelId(CHANNEL_ID)
                        .setStyle(new Notification.BigPictureStyle().bigPicture(bitmap))
                        .setColor(ContextCompat.getColor(this, R.color.yellow))
                        .setContentIntent(pendingIntent)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setVibrate(new long[] {2000})
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setLights(Color.RED, 3000, 3000)
                        .build();
            }

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
// Issue the notification.
            mNotificationManager.notify(notificationID, notification);
            notificationID++;
        }catch (Exception e){
            e.toString();
        }
    }

    public Bitmap getBitmapfromUrl(String imageurl) {
        try {
            URL url = new URL(imageurl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
}
