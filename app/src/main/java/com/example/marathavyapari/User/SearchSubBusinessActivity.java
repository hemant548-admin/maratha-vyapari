package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Adapter.MySubServiceListAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.SearchSubBusinessListAdapter;
import com.example.marathavyapari.Adapter.ShowAdvertiseAdapter;
import com.example.marathavyapari.Adapter.SliderAdvertiseAdpter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessAdvertiseModel;
import com.example.marathavyapari.Model.DistrictDataModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.Model.SubBusinessDataModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceChangePassword;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchSubBusinessActivity extends AppCompatActivity {

    Spinner districts, taluka;
    ArrayList<DistrictModel.DistrictsList> districtsList;
    ArrayList<TalukaDataModel.TalukaList> talukaList;
    ArrayList<SubBusinessDataModel.BusinessListModel> subbusinesslistdata;
    RecyclerView recyclerView;
    //RecyclerView advartise;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    TextView toolbar_title;
    SearchSubBusinessListAdapter searchSubBusinessListAdapter;
    ImageView noDataFound;
   // ImageView nofound;
    Toolbar toolbar;
    EditText serachcat;
    ProgressBar progressBar;
    ImageView back;
    ApiClient apiClient;
    String districtid, talukaid, DistrictsName, TalukaName, sub_category_id, token, businessId, lang;
    ArrayList<BusinessAdvertiseModel.AdvertiseList> advertiseLists;
    ShowAdvertiseAdapter showAdvertiseAdapter;

    SliderView sliderView;
    Runnable runnable;
    Handler handler;

    public static final String LOGTAG = "slider";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serachsubbusinessactivity);

        toolbar = findViewById(R.id.contactus_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.business);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        districts = findViewById(R.id.spinner_dist);
        taluka = findViewById(R.id.spinner_taluka);
        apiClient = new ApiClient();
        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.businesslist);
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
      //  nofound = findViewById(R.id.no_found);
        serachcat = findViewById(R.id.searchcategory);
        serachcat.requestFocus();
        districtsList = new ArrayList<>();
        talukaList = new ArrayList<>();
        sub_category_id = getIntent().getStringExtra("sub_category_id");
        Log.d("TUSHARSUBCATE12", sub_category_id);
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getToken("Token", SearchSubBusinessActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", SearchSubBusinessActivity.this);
        subbusinesslistdata = new ArrayList<>();
        advertiseLists = new ArrayList<>();
        recyclerView = findViewById(R.id.businesslist);

        sliderView = findViewById(R.id.imageSlider);
     //   advartise = findViewById(R.id.advlist);
        if(SharedPrefManager.getInstance(SearchSubBusinessActivity.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(SearchSubBusinessActivity.this, SelectAccountType.class));
            return;
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
       // advartise.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        getSubAllList(sub_category_id);
        getDistricts(lang);
        getAdvertise(sub_category_id);
        serachcat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                progressBar.setVisibility(View.VISIBLE);
                String text = charSequence.toString();
                getSearchAllPost(text);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                progressBar.setVisibility(View.VISIBLE);
                String text = serachcat.getText().toString();
                getSearchAllPost(text);
            }
        });


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        businessId = subbusinesslistdata.get(position).getBus_id();
                        sharedPrefManager.setBusinessId("BusinessId", businessId, SearchSubBusinessActivity.this);
                        Intent catlist = new Intent(SearchSubBusinessActivity.this, Activity_ShopDetails.class);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

        districts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                districtid = districtsList.get(i).getDistrict_id();
                Log.d("TusharDistrictId", districtid);
                getTaluka(districtid, lang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        taluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                talukaid = talukaList.get(i).getTaluka_id();
                getsearchSubBusinesd(sub_category_id, districtid, talukaid);
                Log.d("TUSHARTALULAID", talukaid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getSearchAllPost(String text) {
        apiClient.apiInterface.showsearchbuslist(token, sub_category_id, districtid, talukaid, text).enqueue(new Callback<SubBusinessDataModel>() {
            @Override
            public void onResponse(Call<SubBusinessDataModel> call, retrofit2.Response<SubBusinessDataModel> response) {
                if (response.isSuccessful()) {

                    try {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        subbusinesslistdata = response.body().getBusiness();
                        searchSubBusinessListAdapter = new SearchSubBusinessListAdapter(SearchSubBusinessActivity.this, subbusinesslistdata);
                        recyclerView.setAdapter(searchSubBusinessListAdapter);
                        searchSubBusinessListAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                       noDataFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<SubBusinessDataModel> call, Throwable t) {

            }
        });
    }

    private void getAdvertise(String sub_category_id) {
        apiClient.apiInterface.showBusinssAdv(sub_category_id).enqueue(new Callback<BusinessAdvertiseModel>() {
            @Override
            public void onResponse(Call<BusinessAdvertiseModel> call, retrofit2.Response<BusinessAdvertiseModel> response) {
                if (response.isSuccessful()) {

                    try {

                        advertiseLists = response.body().getAdvs();
                        SliderAdvertiseAdpter adapter = new  SliderAdvertiseAdpter(SearchSubBusinessActivity.this,advertiseLists);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();
                       /* showAdvertiseAdapter = new ShowAdvertiseAdapter(SearchSubBusinessActivity.this, advertiseLists);
                        advartise.setAdapter(showAdvertiseAdapter);
                        showAdvertiseAdapter.notifyDataSetChanged();
                        advartise.setLayoutManager(new ScrollingLinearLayoutManager(SearchSubBusinessActivity.this, LinearLayoutManager.HORIZONTAL, true, 1000));

                        if (showAdvertiseAdapter.getItemCount() == 0) {
                            nofound.setVisibility(View.VISIBLE);
                            advartise.setVisibility(View.GONE);
                        }*/
                    } catch (Exception e) {
                       /* nofound.setVisibility(View.VISIBLE);
                        advartise.setVisibility(View.GONE);*/
                    }

                }
            }

            @Override
            public void onFailure(Call<BusinessAdvertiseModel> call, Throwable t) {

            }
        });
    }

    private void getsearchSubBusinesd(String sub_category_id, String districtid, String talukaid) {
        apiClient.apiInterface.showSubbusincatdisttalwise(token, sub_category_id, districtid, talukaid).enqueue(new Callback<SubBusinessDataModel>() {
            @Override
            public void onResponse(Call<SubBusinessDataModel> call, retrofit2.Response<SubBusinessDataModel> response) {
                if (response.isSuccessful()) {

                    try {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        subbusinesslistdata = response.body().getBusiness();
                        searchSubBusinessListAdapter = new SearchSubBusinessListAdapter(SearchSubBusinessActivity.this, subbusinesslistdata);
                        recyclerView.setAdapter(searchSubBusinessListAdapter);
                        searchSubBusinessListAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        noDataFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
            @Override
            public void onFailure(Call<SubBusinessDataModel> call, Throwable t) {
            }
        });
    }

    private void getTaluka(String districtid, String lang) {
        apiClient.apiInterface.talukalist(districtid, lang).enqueue(new Callback<TalukaDataModel>() {
            @Override
            public void onResponse(Call<TalukaDataModel> call, retrofit2.Response<TalukaDataModel> response) {
                if (response.isSuccessful()) {
                    talukaList = response.body().getTaluka();

                    if (talukaList.size() >= 0) {

                        List<String> usernamearry = new ArrayList<String>();
                        if (usernamearry.size() > 0) {
                            usernamearry.clear();
                        }
                        for (int i = 0; i < talukaList.size(); i++) {
                            String username = talukaList.get(i).getTaluka_name();

                            Log.e("TAG", "onResponse: username  " + username);
                            usernamearry.add(username);
                        }

                        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(SearchSubBusinessActivity.this, R.layout.checked_text_black, usernamearry);
                        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        taluka.setAdapter(dataAdapter2);
                    }

                }
            }

            @Override
            public void onFailure(Call<TalukaDataModel> call, Throwable t) {

            }
        });
    }

    private void getSubAllList(String sub_category_id) {
        apiClient.apiInterface.showSubbusinessCat(token, sub_category_id).enqueue(new Callback<SubBusinessDataModel>() {
            @Override
            public void onResponse(Call<SubBusinessDataModel> call, retrofit2.Response<SubBusinessDataModel> response) {
                if (response.isSuccessful()) {
                    try {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        subbusinesslistdata = response.body().getBusiness();
                        searchSubBusinessListAdapter = new SearchSubBusinessListAdapter(SearchSubBusinessActivity.this, subbusinesslistdata);
                        recyclerView.setAdapter(searchSubBusinessListAdapter);
                        searchSubBusinessListAdapter.notifyDataSetChanged();
                        if (searchSubBusinessListAdapter.getItemCount() == 0) {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<SubBusinessDataModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getDistricts(String lang) {
        apiClient.apiInterface.districtlist(lang).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, retrofit2.Response<DistrictModel> response) {
                if (response.isSuccessful()) {
                    districtsList = response.body().getDistricts();

                    if (districtsList.size() >= 0) {

                        List<String> districtlistarray = new ArrayList<String>();
                        if (districtlistarray.size() > 0) {
                            districtlistarray.clear();
                        }
//
                        for (int i = 0; i < districtsList.size(); i++) {
                            String distlist = districtsList.get(i).getDistrict_name();
                            districtlistarray.add(distlist);
                        }
                        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(SearchSubBusinessActivity.this, R.layout.checked_text_black, districtlistarray);
                        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        districts.setAdapter(dataAdapter3);
                    }

                }
            }

            @Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {

            }
        });
    }

    public class ScrollingLinearLayoutManager extends LinearLayoutManager {
        private final int duration;

        public ScrollingLinearLayoutManager(Context context, int orientation, boolean reverseLayout, int duration) {
            super(context, orientation, reverseLayout);
            this.duration = duration;
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                           int position) {
            View firstVisibleChild = recyclerView.getChildAt(0);
            int itemHeight = firstVisibleChild.getHeight();
            int currentPosition = recyclerView.getChildLayoutPosition(firstVisibleChild);
            int distanceInPixels = Math.abs((currentPosition - position) * itemHeight);
            if (distanceInPixels == 0) {
                distanceInPixels = (int) Math.abs(firstVisibleChild.getY());
            }
            SmoothScroller smoothScroller = new SmoothScroller(recyclerView.getContext(), distanceInPixels, duration);
            smoothScroller.setTargetPosition(position);
            startSmoothScroll(smoothScroller);
        }

        private class SmoothScroller extends LinearSmoothScroller {
            private final float distanceInPixels;
            private final float duration;

            public SmoothScroller(Context context, int distanceInPixels, int duration) {
                super(context);
                this.distanceInPixels = distanceInPixels;
                this.duration = duration;
            }

            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return ScrollingLinearLayoutManager.this
                        .computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                float proportion = (float) dx / distanceInPixels;
                return (int) (duration * proportion);
            }
        }
    }
}

