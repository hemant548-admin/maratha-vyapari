package com.example.marathavyapari.User;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ProfileUserModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class UserProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    LinearLayout lay_changpass, lay_edit;
    TextView name, email, mobile;
    SharedPrefManager sharedPrefManager;
    String Name, Email, Mobile, token;
    ProgressDialog progressDialog;
    ImageView rule_image;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 101;
    ProfileUserModel userProfileModel;
    Bitmap bitmap;
    Uri filepath;
    String postImage= "", imagepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__profile);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressDialog = new ProgressDialog(this);
       sharedPrefManager = new SharedPrefManager(this);
        /*Name = sharedPrefManager.getUserName("UserName", UserProfileActivity.this);
        Email = sharedPrefManager.getEmail("Email", UserProfileActivity.this);
        Mobile = sharedPrefManager.getMobile("Mobile", UserProfileActivity.this);*/
        
        token = sharedPrefManager.getToken("Token", UserProfileActivity.this);
        if(SharedPrefManager.getInstance(UserProfileActivity.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(UserProfileActivity.this, SelectAccountType.class));
            return;
        }

        name = findViewById(R.id.textname);
        email = findViewById(R.id.textemail);
        mobile = findViewById(R.id.textmobile);
        lay_changpass = findViewById(R.id.lay_changepass);
        lay_edit = findViewById(R.id.lay_edit);
        rule_image = findViewById(R.id.rule_image);

       getProfile();


        lay_changpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserProfileActivity.this, UserChangePassword.class);
                startActivity(i);
            }
        });
        lay_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagepath = userProfileModel.getUser_photo();
                Log.d("TusharImagepath", imagepath);
                Intent i = new Intent(UserProfileActivity.this, Edit_Profile.class);
                i.putExtra("Name", Name);
                i.putExtra("Email", Email);
                i.putExtra("Mobile", Mobile);
                i.putExtra("post_image",imagepath);
             /*  Pair[] pairs = new Pair[1];
                pairs[0] = new Pair<View,String>(view.findViewById(R.id.rule_image),"postimage");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(UserProfileActivity.this,pairs);*/
                startActivity(i);
            }
        });

    }




    private void getProfile() {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GETUSERPROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(UserProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                      userProfileModel  = new ProfileUserModel(
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("district_id_fk"),
                                obj.getString("taluka_id_fk"),
                                obj.getString("user_latitude"),
                                obj.getString("user_longitude"),
                                obj.getString("user_photo"),
                                obj.getString("updated_at"),
                                obj.getString("created_at"),
                                obj.getString("district_id"),
                                obj.getString("district_name"),
                                obj.getString("live_status"),
                                obj.getString("taluka_id"),
                                obj.getString("taluka_name")
                        );

                        Name = userProfileModel.getUser_name();
                        Email = userProfileModel.getUser_email();
                        Mobile = userProfileModel.getUus_mobile();
                        name.setText(Name);
                        email.setText(Email);
                        mobile.setText(Mobile);

                        imagepath = userProfileModel.getUser_photo();
                        String imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(rule_image);
                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                       // Toast.makeText(UserProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
               // Toast.makeText(UserProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
