package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.AddPostBusiness;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Adapter.BusinessRulesAdapter;
import com.example.marathavyapari.Adapter.BusinessSliderAapter;
import com.example.marathavyapari.Adapter.SearchSubServiceListAdapter;
import com.example.marathavyapari.Adapter.ShowImageAdapter;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowBusineePostModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.Model.SubServiceDataModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServieTermsConditions;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class Activity_ShopDetails extends AppCompatActivity {

    Toolbar toolbar;

    SharedPrefManager sharedPrefManager;
    String businessid, token, Addrating;
    ApiClient apiClient;
    Boolean clicked = true;
    ImageView imageView, imagefav;
    TextView name, address, number, showaddress, countrating;
    RatingBar showrating, addrating;
    LinearLayout  showreviews;
    ProgressDialog progressDialog;
    AlertDialog alertDialog;
    String Number;
    LinearLayout lay_trans,lay_call, lay_share, lay_loc;
    ArrayList<ShowBusineePostModel.PhotoList> imagelist;
    ArrayList<ShowBusineePostModel.RuleList> rulelists;
    //SliderView sliderView;
    RecyclerView recyclerView, showrecycler;
    BusinessRulesAdapter businessRulesAdapter;
    ShowImageAdapter showImageAdapter;
    String latitude, longitude, isclicked;
    String imagepath2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imagelist = new ArrayList<>();
        rulelists = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        imageView = findViewById(R.id.post_image);
        imagefav = findViewById(R.id.showfav);
        name = findViewById(R.id.shopname);
        address = findViewById(R.id.address);
        showrating = findViewById(R.id.show_rating);
       // sliderView = findViewById(R.id.imageSlider);
        number = findViewById(R.id.textnumber);
        showaddress = findViewById(R.id.text_address);
        addrating = findViewById(R.id.add_rating);
        countrating = findViewById(R.id.ratingcount);
        showreviews = findViewById(R.id.lay_reviews);
        lay_trans = findViewById(R.id.lay_trans);
        lay_call = findViewById(R.id.lay_call);
        lay_share = findViewById(R.id.lay_share);
        lay_loc = findViewById(R.id.lay_location);
        showrecycler = findViewById(R.id.recylshowimage);
        recyclerView = findViewById(R.id.rulelist);


        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        showrecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getToken("Token", this);
        businessid = sharedPrefManager.getBusinessId("BusinessId", this);
        apiClient = new ApiClient();
        lay_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + latitude + "," + longitude;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });
        lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mSource="";
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plan");
                i.putExtra(Intent.EXTRA_SUBJECT,mSource);
               // String body = BaseURL;
                //i.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(i, "Share with"));
            }
        });
        lay_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+Number));
                startActivity(intent);
            }
        });
         lay_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clicked)
                {
                    clicked = false;
                    getMyTransaction(businessid);
                }else
                {
                    clicked = true;
                    getUnTransaction(businessid);
                }
            }
        });
        imagefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clicked)
                {
                    clicked = false;
                    imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.yellow));
                    getFavourite(businessid);
                }else
                {
                    clicked = true;
                    imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.black));
                    getUnfavourite(businessid);
                }
            }
        });
        showreviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Activity_ShopDetails.this, ShowBusinessReviewActivity.class);
                startActivity(i);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Activity_ShopDetails.this, ShowImageActivity.class);
                intent.putExtra("imagepath",imagepath2);
                startActivity(intent);
            }
        });
        addrating.setClickable(true);
        addrating.setEnabled(true);

        addrating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Addrating = String.valueOf(addrating.getRating());
                getAddRating(Addrating);
                opendailog();
                Toast.makeText(getApplicationContext(), Addrating, Toast.LENGTH_LONG).show();
            }
        });

        getShowDetails(token, businessid);
    }

    private void opendailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Activity_ShopDetails.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.content_addreviews, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();

        EditText getdesc;
        TextView okay,cancel;
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String desc;
                desc = getdesc.getText().toString();

                if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    AddReview( desc);
                }
            }
        });

        alertDialog.show();
    }

    private void AddReview(String desc) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_REVIEWS_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);
                params.put("review_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getUnTransaction(String businessid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USER_BUSINESS_UNMYTRANSACTION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);



                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);

                return params;

            }
        };

        RequestHandlerNew.getInstance(Activity_ShopDetails.this).addToRequestQueue(stringRequest);
    }

    private void getMyTransaction(String businessid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USER_BUSINESS_MYTRANSACTION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        String contact = "91" +Number ; // use country code with your phone number
                        String url = "https://api.whatsapp.com/send?phone=" + contact;
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        startActivity(intent);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws
                    AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);

                return params;

            }
        };

        RequestHandlerNew.getInstance(Activity_ShopDetails.this).addToRequestQueue(stringRequest);
    }

    private void getUnfavourite(String businessid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UNFAV_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getFavourite(String businessid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.FAV_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


    private void getAddRating(String addrating) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_RATING_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Activity_ShopDetails.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Activity_ShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);
                params.put("rating", addrating);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }





    private void getShowDetails(String token, String businessid) {
        apiClient.apiInterface.showShopdetailbuswise(token,businessid).enqueue(new Callback<ShowBusineePostModel>() {
            @Override
            public void onResponse(Call<ShowBusineePostModel> call, retrofit2.Response<ShowBusineePostModel> response) {
                if (response.isSuccessful()) {

                    rulelists = response.body().getRule();
                    imagelist = response.body().getPhoto();
                   showImageAdapter = new ShowImageAdapter(Activity_ShopDetails.this, imagelist);
                   showrecycler.setAdapter(showImageAdapter);
                   isclicked = response.body().getIs_favourite();
                   if (isclicked.equals("1"))
                   {
                       imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.yellow));

                   }else
                   {
                       imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.black));

                   }
                    Number = response.body().getBusiness().getBus_mobile();
                    toolbar.setTitle(response.body().getBusiness().getBus_name());
                    String imagepath = response.body().getBusiness().getPhoto();
                    imagepath2  = Base_Url.imagepath + imagepath; ;
                    Glide.with(getApplication())
                            .load(imagepath2)
                            .into(imageView);
                    name.setText(response.body().getBusiness().getBus_name());
                    address.setText(response.body().getBusiness().getBusiness_address());
                    showrating.setRating(Float.parseFloat(response.body().getBusiness().getAvrage_rating()));
                    number.setText(response.body().getBusiness().getBus_mobile());
                    showaddress.setText(response.body().getBusiness().getBusiness_address());
                    countrating.setText(response.body().getBusiness().getNo_of_rating());
                    businessRulesAdapter = new BusinessRulesAdapter(Activity_ShopDetails.this, rulelists);
                    recyclerView.setAdapter(businessRulesAdapter);
                    latitude = response.body().getBusiness().getBusiness_latitude();
                    longitude = response.body().getBusiness().getBusiness_longitude();

                }
            }@Override
            public void onFailure(Call<ShowBusineePostModel> call, Throwable t) {

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
