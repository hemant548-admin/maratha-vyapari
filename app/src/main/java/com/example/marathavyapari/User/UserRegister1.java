package com.example.marathavyapari.User;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.DistrictDataModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.Service.ServiceRegister2;
import com.example.marathavyapari.Service.UploadServiceDoc;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class UserRegister1 extends AppCompatActivity {

    LinearLayout nextBtn,back_layout;
    Spinner district, taluka;
    ArrayList<DistrictModel.DistrictsList> districtsList;
    ArrayList<TalukaDataModel.TalukaList> talukaList;
    EditText name, emailid,whatsappNumber,getPassword,getconfirmPassword;
    String Name, EmailId,districtname, districtid, talukaname, talukaid,number,whnumber,password,cnfpassword;
    ApiClient apiClient;
    ImageView visiblePass,visiblePass2;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    String lang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_1);

        apiClient = new ApiClient();
        progressDialog = new ProgressDialog(this);
        nextBtn = findViewById(R.id.tv_service_next1);
        back_layout = findViewById(R.id.back_layout);
        name = findViewById(R.id.edit_name);
        whatsappNumber = findViewById(R.id.edit_number);
        emailid = findViewById(R.id.edit_email);
        getPassword = findViewById(R.id.register_business_getpassword);
        getconfirmPassword = findViewById(R.id.register_business_getpasswordcnf);
        visiblePass = findViewById(R.id.visibility);
        visiblePass2 = findViewById(R.id.visibility2);
        district = findViewById(R.id.register_districts);
        taluka = findViewById(R.id.register_taluka);

        sharedPrefManager = new SharedPrefManager(UserRegister1.this);
        lang = sharedPrefManager.getLanguage("Lang", UserRegister1.this);
        Log.d("TUSHARLANG", lang);
        districtsList = new ArrayList<>();
        talukaList = new ArrayList<>();
        number = getIntent().getStringExtra("number");
        whatsappNumber.setText(number);
        getDistricts(lang);
        district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                districtid = districtsList.get(i).getDistrict_id();
                Log.d("TusharDistrictId", districtid);
                getTaluka(districtid, lang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        taluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                talukaid = talukaList.get(i).getTaluka_id();
                Log.d("TUSHARTALULAID", talukaid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        visiblePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ShowHidePass(v);
            }
        });

        visiblePass2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass2(v);
            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                register2();
            }
        });

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void ShowHidePass2(View view) {
        if(view.getId()==R.id.visibility2){
            if(getconfirmPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getconfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getconfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    private void ShowHidePass(View view) {
        if(view.getId()==R.id.visibility){
            if(getPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    private void register2() {

        Name = name.getText().toString();
        EmailId = emailid.getText().toString();
        whnumber = whatsappNumber.getText().toString();
        password = getPassword.getText().toString();
        cnfpassword = getconfirmPassword.getText().toString();

        if (Name.isEmpty())
        {
            name.setError("Please enter your name");
            name.requestFocus();
        }else if (EmailId.isEmpty())
        {
            emailid.setError("Please enter your email address");
            emailid.requestFocus();
        }else if (!isEmailValid(EmailId)){
            emailid.setError("please enter valid email address");
            emailid.requestFocus();
        } else if (whnumber.isEmpty())
        {
            whatsappNumber.setError("Please enter your whatsapp number");
            whatsappNumber.requestFocus();
        }
        else if (whnumber.length() != 10)
        {
            whatsappNumber.setError("Please enter 10 digits number");
            whatsappNumber.requestFocus();
        }
        else if (password.isEmpty())
        {
            getPassword.setError("Please enter password");
            getPassword.requestFocus();
        }
        else if (password.length() < 6)
        {
            getPassword.setError("Please enter password more than 6 char");
            getPassword.requestFocus();
        }
        else if (!isValidPassword(password))
        {
            getPassword.setError("Password must contain upper letters,numbers and special symbols");
            getPassword.requestFocus();
        }
        else if (cnfpassword.isEmpty())
        {
            getconfirmPassword.setError("Please enter your password");
            getconfirmPassword.requestFocus();
        }
        else if (!password.equals(cnfpassword))
        {
            getconfirmPassword.setError("Password didn't matched");
            getconfirmPassword.requestFocus();
        }
        else
        {
            registerUser();
        }
    }

    private boolean isEmailValid(String emailId) {
        Boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = emailId;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void registerUser() {
        progressDialog.setTitle("Register");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.GET_USER_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
//                        Toast.makeText(UploadBusinessDoc.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        UserProfileModel userProfileModel = new UserProfileModel(
                                jsonObject.getString("token"),
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("user_photo"),
                                obj.getString("district_name"),
                                obj.getString("taluka_name")
                        );
                        SharedPrefManager.getInstance(getApplicationContext()).userProfilelogin(userProfileModel);
                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), UserRegister1.this);
                        sharedPrefManager.setToken("Token", jsonObject.getString("token"), UserRegister1.this);
                        sharedPrefManager.setMobile("Mobile", obj.getString("uus_mobile"), UserRegister1.this);
                        sharedPrefManager.setEmail("Email", obj.getString("user_email"), UserRegister1.this);
                        sharedPrefManager.setUserName("UserName", obj.getString("user_name"), UserRegister1.this);
                        sharedPrefManager.setUserId("user_id", obj.getString("uus_id"), UserRegister1.this);
                        sharedPrefManager.setUserPhoto("userphoto", obj.getString("user_photo"), UserRegister1.this);
                        final Dialog dialog = new Dialog(UserRegister1.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.register_successffully_dialog);

                        Button dialogButton = (Button) dialog.findViewById(R.id.okay_btn);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                                Intent login = new Intent(UserRegister1.this, UserMainActivity.class);
                                login.putExtra("account_type","user");
                                login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(login);
                                finish();

                            }
                        });

                        dialog.show();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UserRegister1.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UserRegister1.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_name", Name);
                params.put("mobile", whnumber );
                params.put("email", EmailId);
                params.put("password", password);
                params.put("district_id",districtid);
                params.put("taluka_id", talukaid);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getTaluka(String districtid, String lang) {
        apiClient.apiInterface.talukalist(districtid, lang).enqueue(new Callback<TalukaDataModel>() {
            @Override
            public void onResponse(Call<TalukaDataModel> call, retrofit2.Response<TalukaDataModel> response) {
                if (response.isSuccessful()) {
                    talukaList = response.body().getTaluka();

                    if (talukaList.size() >= 0) {

                        List<String> usernamearry = new ArrayList<String>();
                        if (usernamearry.size() > 0) {
                            usernamearry.clear();
                        }
                        for (int i = 0; i < talukaList.size(); i++) {
                            String username = talukaList.get(i).getTaluka_name();

                            Log.e("TAG", "onResponse: username  " + username);
                            usernamearry.add(username);
                        }

                        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(UserRegister1.this, R.layout.checked_text_white, usernamearry);
                        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        taluka.setAdapter(dataAdapter2);
                    }

                }
            }@Override
            public void onFailure(Call<TalukaDataModel> call, Throwable t) {

            }
        });
    }

    private void getDistricts(String lang) {

        apiClient.apiInterface.districtlist(lang).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, retrofit2.Response<DistrictModel> response) {
                if (response.isSuccessful()) {
                   districtsList = response.body().getDistricts();

                    if (districtsList.size() >= 0) {

                        List<String> districtlistarray = new ArrayList<String>();
                        if (districtlistarray.size() > 0) {
                            districtlistarray.clear();
                        }
//
                        for (int i = 0; i < districtsList.size() ; i++)
                        {
                            String distlist = districtsList.get(i).getDistrict_name();
                            districtlistarray.add(distlist);
                        }
                        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(UserRegister1.this, R.layout.checked_text_white, districtlistarray);
                        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        district.setAdapter(dataAdapter3);
                    }

                }
            }@Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {

            }
        });
    }
    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return PASSWORD_PATTERN.matcher(s).matches();
    }
}
