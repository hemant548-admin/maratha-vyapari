package com.example.marathavyapari.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.BusinessRelatedpostAdpater;
import com.example.marathavyapari.Adapter.ShowAllPostAdapetr;
import com.example.marathavyapari.Model.BusinessPostModel;
import com.example.marathavyapari.Model.BusinessServiceModel;
import com.example.marathavyapari.Model.ShowBusiServiceModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class AllServiceBusinessActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ShowAllPostAdapetr showAllPostAdapetr;
    ImageView noDataFound;
    ProgressBar progressBar;
    RecyclerView mypost_rv;
    ArrayList<BusinessServiceModel.PostList> businessonepost;
    ApiClient apiClient;
    String post_id, type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allbusinessservicepost);
        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.businessservicepost);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        apiClient = new ApiClient();

        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        mypost_rv = findViewById(R.id.allpost);
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        getBusinessServicePost();
        mypost_rv.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mypost_rv, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {

                        post_id = businessonepost.get(position).getPost_id();
                        type = businessonepost.get(position).getType();

                        Intent catlist = new Intent(AllServiceBusinessActivity.this,ActivityShowDiscount.class);
                        catlist.putExtra("Type", type);
                        catlist.putExtra("post_id", post_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
    }

    private void getBusinessServicePost() {
        apiClient.apiInterface.showBusinessServicePost().enqueue(new Callback<BusinessServiceModel>() {
            @Override
            public void onResponse(Call<BusinessServiceModel> call, retrofit2.Response<BusinessServiceModel> response) {
                if (response.isSuccessful()) {

                    try {
                        progressBar.setVisibility(View.GONE);
                        businessonepost = response.body().getPost();
                        showAllPostAdapetr = new ShowAllPostAdapetr(AllServiceBusinessActivity.this, businessonepost);
                        mypost_rv.setAdapter(showAllPostAdapetr);
                        showAllPostAdapetr.notifyDataSetChanged();
                    }catch (Exception e)
                    {
                        noDataFound.setVisibility(View.VISIBLE);
                        mypost_rv.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }

                }
            }@Override
            public void onFailure(Call<BusinessServiceModel> call, Throwable t) {

            }
        });
    }
}
