package com.example.marathavyapari.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.marathavyapari.Activity.ChangeLanguage;
import com.example.marathavyapari.R;
import com.example.marathavyapari.Service.ServiceBusinessSetting;
import com.example.marathavyapari.Service.ServiceChangePassword;

import java.util.Objects;

public class UserSetting extends AppCompatActivity
{
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    LinearLayout changePass,changeLang, profileedit, avaruti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.settings);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        changeLang = findViewById(R.id.change_language);
        changePass = findViewById(R.id.change_password);
        profileedit = findViewById(R.id.profile);
        avaruti = findViewById(R.id.avruti);
        profileedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserSetting.this, UserProfileActivity.class);
                startActivity(i);
            }
        });

        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserSetting.this, UserChangePassword.class);
                startActivity(intent);
            }
        });

        changeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent change = new Intent(UserSetting.this, UserLangaugeChange.class);
                startActivity(change);
            }
        });

        avaruti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserSetting.this,AboutUsActivity.class);
                startActivity(intent);
            }
        });


    }
}
