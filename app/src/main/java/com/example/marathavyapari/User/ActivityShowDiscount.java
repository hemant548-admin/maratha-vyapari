package com.example.marathavyapari.User;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessDetailsModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ServiceDetailsModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityShowDiscount extends AppCompatActivity {

    Toolbar toolbar;
    String post_id, type, token, Number;
    SharedPrefManager sharedPrefManager;
    TextView showdiscount, title, desc, time, shtitle,address, norating;
    ImageView image;
    RatingBar ratingBar;
    LinearLayout lay_whats;
    String imagepath2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_discount);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPrefManager = new SharedPrefManager(ActivityShowDiscount.this);
        token = sharedPrefManager.getToken("Token", ActivityShowDiscount.this);
        post_id = getIntent().getStringExtra("post_id");
        type = getIntent().getStringExtra("Type");


        Log.d("Tushar", post_id);
        Log.d("Tushar", type);
        showdiscount = findViewById(R.id.textdiscount);
        title = findViewById(R.id.shopname);
        image = findViewById(R.id.post_image);
        desc = findViewById(R.id.desc);
        time = findViewById(R.id.post_time);
        shtitle = findViewById(R.id.text_title);
        address = findViewById(R.id.text_address);
        norating = findViewById(R.id.ratingcount);
        ratingBar = findViewById(R.id.show_rating);
        lay_whats = findViewById(R.id.lay_what);

        lay_whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPrefManager.getInstance(ActivityShowDiscount.this).isLoggedIn2()){
                    finish();
                    startActivity(new Intent(ActivityShowDiscount.this, SelectAccountType.class));
                    return;
                }else {
                    String contact = "91" + Number; // use country code with your phone number
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ActivityShowDiscount.this, ShowImageActivity.class);
                intent.putExtra("imagepath",imagepath2);
                startActivity(intent);
            }
        });


        if (type.equals("business"))
        {
           getBusinessDetails(post_id, type);

        }else if (type.equals("service"))
        {
            getServiceDetails(post_id, type);
        }
    }

    private void getServiceDetails(String post_id, String type) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SHOWDETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {

                        JSONObject obj = jsonObject.getJSONObject("post");
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        //adding the product to product list
                        ServiceDetailsModel serviceDetailsModel= new ServiceDetailsModel(
                                obj.getString("type"),
                                obj.getString("post_id"),
                                obj.getString("post_title"),
                                obj.getString("post_description"),
                                obj.getString("post_image"),
                                obj.getString("post_date"),
                                obj.getString("post_time"),
                                obj.getString("sus_id"),
                                obj.getString("sus_mobile"),
                                obj.getString("sus_name"),
                                obj.getString("name"),
                                obj.getString("service_address"),
                                obj.getString("service_description"),
                                obj.getString("service_type"),
                                obj.getString("licence"),
                                obj.getString("service_latitude"),
                                obj.getString("service_longitude"),
                                obj.getString("avrage_rating"),
                                obj.getString("no_of_rating")
                        );

                        Number = serviceDetailsModel.getSus_mobile();
                        toolbar.setTitle(serviceDetailsModel.getSus_name());
                        title.setText(serviceDetailsModel.getSus_name());
                        String imagepath = serviceDetailsModel.getPost_image();
                        imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        desc.setText(serviceDetailsModel.getService_description());
                        time.setText(serviceDetailsModel.getPost_time());
                        shtitle.setText(serviceDetailsModel.getSus_name());
                        address.setText(serviceDetailsModel.getService_address());
                        norating.setText(serviceDetailsModel.getNo_of_rating());
                        ratingBar.setRating(Float.parseFloat(serviceDetailsModel.getAvrage_rating()));
                        showdiscount.setText(serviceDetailsModel.getPost_title());
                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(ActivityShowDiscount.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityShowDiscount.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("post_id", post_id);
                params.put("type", type);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getBusinessDetails(String post_id, String type) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SHOWDETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {

                        JSONObject obj = jsonObject.getJSONObject("post");
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);




                            //adding the product to product list
                        BusinessDetailsModel businessDetail= new BusinessDetailsModel(
                                obj.getString("type"),
                                obj.getString("post_id"),
                                obj.getString("business_id_fk"),
                                obj.getString("post_title"),
                                obj.getString("post_description"),
                                obj.getString("post_image"),
                                obj.getString("post_date"),
                                obj.getString("post_time"),
                                obj.getString("bus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("bus_mobile"),
                                obj.getString("bus_name"),
                                obj.getString("owner_name"),
                                obj.getString("business_address"),
                                obj.getString("business_description"),
                                obj.getString("business_type"),
                                obj.getString("detail_info"),
                                obj.getString("licence"),
                                obj.getString("photo"),
                                obj.getString("business_latitude"),
                                obj.getString("business_longitude"),
                                obj.getString("avrage_rating"),
                                obj.getString("no_of_rating"),
                                obj.getString("business_email")
                        );

                        toolbar.setTitle(businessDetail.getBus_name());
                        title.setText(businessDetail.getBus_name());
                        String imagepath = businessDetail.getPost_image();
                        imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        desc.setText(businessDetail.getBusiness_description());
                        time.setText(businessDetail.getPost_time());
                        shtitle.setText(businessDetail.getBus_name());
                        address.setText(businessDetail.getBusiness_address());
                        norating.setText(businessDetail.getNo_of_rating());
                        ratingBar.setRating(Float.parseFloat(businessDetail.getAvrage_rating()));
                        showdiscount.setText(businessDetail.getPost_title());
                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(ActivityShowDiscount.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityShowDiscount.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("post_id", post_id);
                params.put("type", type);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
