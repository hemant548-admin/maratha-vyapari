package com.example.marathavyapari.User;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ProfileUserModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Edit_Profile extends AppCompatActivity {

    Toolbar toolbar;
    EditText name, email, mobile;
    LinearLayout save;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    String Name, Email, Mobile, Proname, Proemail, token;
    ImageView rule_image;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 101;

    Bitmap bitmap;
    Uri filepath;
    String postImage= "";
    String image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPrefManager = new SharedPrefManager(this);
        Name = getIntent().getStringExtra("Name");
        Email = getIntent().getStringExtra("Email");
        Mobile = getIntent().getStringExtra("Mobile");
        token = sharedPrefManager.getToken("Token", Edit_Profile.this);
        Mobile = sharedPrefManager.getMobile("Mobile", Edit_Profile.this);
        Name = sharedPrefManager.getUserName("UserName", Edit_Profile.this);
        Email = sharedPrefManager.getEmail("Email", Edit_Profile.this);
        image = getIntent().getStringExtra("post_image");
        name = findViewById(R.id.edit_name);
        email = findViewById(R.id.edit_emailid);
        mobile = findViewById(R.id.editmobile);
        save = findViewById(R.id.lay_save);
        rule_image = findViewById(R.id.rule_image);

        name.setText(Name);
        email.setText(Email);
        mobile.setText(Mobile);
        String imagepath2  = Base_Url.imagepath + image; ;
        Glide.with(this)
                .load(imagepath2)
                .into(rule_image);

        progressDialog = new ProgressDialog(this);
        rule_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Proname = name.getText().toString();
                Proemail = email.getText().toString();

                if (Proname.isEmpty())
                {
                    name.setError("Enter the Your Name");
                    name.requestFocus();
                }else if (Proemail.isEmpty())
                {
                    email.setError("Enter the email address");
                    email.requestFocus();
                } else if (!Proemail.contains("@")){
                    email.setError("please enter valid email address");
                    email.requestFocus();
                }
                else if (!Proemail.contains(".")){
                    email.setError("please enter valid email address");
                    email.requestFocus();
                } else if(postImage.equals(""))
                {
                    Toast.makeText(Edit_Profile.this, "Please select image", Toast.LENGTH_SHORT).show();
                }else {
                    uploadImage();
                    editProfile();
                }
            }
        });
    }

    private void uploadImage() {
        progressDialog.setTitle("Uploading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USERPROFILEUPDATED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(Edit_Profile.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        ProfileUserModel userProfileModel = new ProfileUserModel(
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("district_id_fk"),
                                obj.getString("taluka_id_fk"),
                                obj.getString("user_latitude"),
                                obj.getString("user_longitude"),
                                obj.getString("user_photo"),
                                obj.getString("updated_at"),
                                obj.getString("created_at"),
                                obj.getString("district_id"),
                                obj.getString("district_name"),
                                obj.getString("live_status"),
                                obj.getString("taluka_id"),
                                obj.getString("taluka_name")
                        );
                        Log.i("TAG", "onResponse: MADAN"+message);
                        Toast.makeText(Edit_Profile.this, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Edit_Profile.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                Toast.makeText(Edit_Profile.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("image_file", postImage);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Edit_Profile.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_camera_dialog, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        LinearLayout camera,device;
        camera = dialogView.findViewById(R.id.with_camera);
        device = dialogView.findViewById(R.id.with_device);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1);
                        alertDialog.dismiss();

                    }
                }
            }
        });

        device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_DEVICE_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent,"Select Image"),2);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        alertDialog.show();
    }

    private void editProfile() {
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USEREDITPROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: LOGIN = "+response);
                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();

                        JSONObject obj = jsonObject.getJSONObject("profile");
                        String message = jsonObject.getString("message");
                        Toast.makeText(Edit_Profile.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        UserProfileModel userProfileModel = new UserProfileModel(
                                token,
                                obj.getString("uus_id"),
                                obj.getString("uus_mobile"),
                                obj.getString("user_email"),
                                obj.getString("user_name"),
                                obj.getString("user_photo"),
                                obj.getString("district_name"),
                                obj.getString("taluka_name")
                        );

                        Intent servicelogin = new Intent(Edit_Profile.this,UserProfileActivity.class);
                        servicelogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(servicelogin);
                        finish();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(Edit_Profile.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Edit_Profile.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_name", Proname);
                params.put("email", Proemail);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            imagestore(bitmap);
            rule_image.setImageBitmap(bitmap);
            rule_image.setEnabled(false);

        }
        else if (requestCode==2 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imagestore(bitmap);
                rule_image.setImageBitmap(bitmap);
                rule_image.setEnabled(false);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void imagestore(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        postImage = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
