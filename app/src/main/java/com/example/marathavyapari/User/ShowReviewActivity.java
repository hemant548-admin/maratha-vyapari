package com.example.marathavyapari.User;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.PostDetailsActivity;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.SearchSubServiceListAdapter;
import com.example.marathavyapari.Adapter.ShowServiceReviewAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ServiceRevioewModelData;
import com.example.marathavyapari.Model.ShowReViewServiceModel;
import com.example.marathavyapari.Model.SubServiceDataModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class ShowReviewActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView back, image;
    TextView toolbar_title;
    RatingBar ratingBar;
    TextView noDataFound;
    ProgressBar progressBar;
    AlertDialog alertDialog;
    ProgressDialog progressDialog;
    RecyclerView mypost_rv;
    SharedPrefManager sharedPrefManager;
   ArrayList<ServiceRevioewModelData.ServiceList> showReviewServiceList;
    ShowServiceReviewAdapter showServiceReviewAdapter;
    String token, serviceId;
    ApiClient apiClient;
    TextView title, address, norating;
   // ImageView addreview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_review);

        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.reviews);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        sharedPrefManager = new SharedPrefManager(ShowReviewActivity.this);
        title = findViewById(R.id.texttitle);
        address = findViewById(R.id.text_address);
        norating = findViewById(R.id.ratingcount);
        ratingBar = findViewById(R.id.show_rating);
        image = findViewById(R.id.post_image);
        progressDialog = new ProgressDialog(this);
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        //addreview = findViewById(R.id.add_review);
        mypost_rv = findViewById(R.id.mypost_rv);
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        apiClient = new ApiClient();
        showReviewServiceList = new ArrayList<>();
       token = sharedPrefManager.getToken("Token", ShowReviewActivity.this);
       serviceId = sharedPrefManager.getServiceID("ServiceId", this);

       /* addreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opendailog();
            }
        });*/
    }

    private void opendailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowReviewActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.content_addreviews, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();

        EditText getdesc;
        TextView okay,cancel;
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String desc;
                desc = getdesc.getText().toString();

                if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    AddReview( desc);
                }
            }
        });

        alertDialog.show();
    }

    private void AddReview(String desc) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_REVIEWS_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowReviewActivity.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowReviewActivity.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowReviewActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                params.put("review_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onStart() {
        loadPost();
        super.onStart();
    }

    private void loadPost() {
        apiClient.apiInterface.showServiceReview(token,serviceId).enqueue(new Callback<ServiceRevioewModelData>() {
            @Override
            public void onResponse(Call<ServiceRevioewModelData> call, retrofit2.Response<ServiceRevioewModelData> response) {
                if (response.isSuccessful()) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        showReviewServiceList = response.body().getReview();
                        title.setText(showReviewServiceList.get(0).getSus_name());
                        address.setText(showReviewServiceList.get(0).getService_address());
                        ratingBar.setRating(Float.parseFloat(showReviewServiceList.get(0).getAvrage_rating()));
                        norating.setText(showReviewServiceList.get(0).getNo_of_rating());
                        String imagepath = showReviewServiceList.get(0).getLicence();
                        String imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        showServiceReviewAdapter = new ShowServiceReviewAdapter(ShowReviewActivity.this, showReviewServiceList);
                      mypost_rv.setAdapter(showServiceReviewAdapter);
                        showServiceReviewAdapter.notifyDataSetChanged();
                    }catch (Exception e)
                    {
                        noDataFound.setVisibility(View.VISIBLE);
                           mypost_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                    }

                }
            }@Override
            public void onFailure(Call<ServiceRevioewModelData> call, Throwable t) {

            }
        });

    }
}
