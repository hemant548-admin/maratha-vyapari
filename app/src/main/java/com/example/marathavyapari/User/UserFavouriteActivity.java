package com.example.marathavyapari.User;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.SelectAccountType;
import com.example.marathavyapari.Adapter.MyFavAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Adapter.ServiceListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.Model.ShowFavouriteModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserFavouriteActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView noDataFound;
    List<ShowFavouriteModel> showFavouriteList;
    MyFavAdapter myFavAdapter;
    SharedPrefManager sharedPrefManager;
    ProgressBar progressBar;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_favourite);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
       toolbar.setTitle(R.string.myfavorites);

        sharedPrefManager = new SharedPrefManager(UserFavouriteActivity.this);
        token = sharedPrefManager.getToken("Token", UserFavouriteActivity.this);
        if(SharedPrefManager.getInstance(UserFavouriteActivity.this).isLoggedIn2()){
            finish();
            startActivity(new Intent(UserFavouriteActivity.this, SelectAccountType.class));
            return;
        }
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        recyclerView = findViewById(R.id.favouritelist);
        showFavouriteList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getAllFavourite();

    }

    private void getAllFavourite() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.USER_FAVOURITE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        showFavouriteList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("favourite");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            showFavouriteList.add(new ShowFavouriteModel(
                                    product.getString("type"),
                                    product.getString("fav_id"),
                                    product.getString("user_id_fk"),
                                    product.getString("business_id_fk"),
                                    product.getString("service_id_fk"),
                                    product.getString("created_at"),
                                    product.getString("bus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("subcategory_id_fk"),
                                    product.getString("district_id_fk"),
                                    product.getString("taluka_id_fk"),
                                    product.getString("bus_mobile"),
                                    product.getString("bus_name"),
                                    product.getString("updated_at"),
                                    product.getString("owner_name"),
                                    product.getString("business_address"),
                                    product.getString("business_description"),
                                    product.getString("business_type"),
                                    product.getString("detail_info"),
                                    product.getString("licence"),
                                    product.getString("photo"),
                                    product.getString("business_latitude"),
                                    product.getString("business_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("business_status"),
                                    product.getString("business_email"),
                                    product.getString("live_status"),
                                    product.getString("language"),
                                    product.getString("fcm_token"),
                                    product.getString("sus_id"),
                                    product.getString("sus_mobile"),
                                    product.getString("sus_name"),
                                    product.getString("name"),
                                    product.getString("service_address"),
                                    product.getString("service_description"),
                                    product.getString("service_type"),
                                    product.getString("service_latitude"),
                                    product.getString("service_longitude"),
                                    product.getString("service_status")
                            ));
                        }

                        myFavAdapter = new MyFavAdapter(UserFavouriteActivity.this, showFavouriteList);
                        recyclerView.setAdapter(myFavAdapter);
                        myFavAdapter.notifyDataSetChanged();

                        if (myFavAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(UserFavouriteActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                Toast.makeText(UserFavouriteActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
