package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Adapter.ServiceRuleAdapter;
import com.example.marathavyapari.Adapter.ServiceSliderAapter;
import com.example.marathavyapari.Adapter.ShowImageAdapter;
import com.example.marathavyapari.Adapter.ShowServiceImageAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowServicePostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class ShowServiceShopDetails extends AppCompatActivity {

    Toolbar toolbar;

    SharedPrefManager sharedPrefManager;
    String serviceId, token,Addrating,Number;
    ApiClient apiClient;
    boolean clicked = true;
    ImageView imageView, imagefav;
    TextView name, address, number, showaddress, countrating;
    RatingBar showrating, addrating;
    LinearLayout  showreviews;
    AlertDialog alertDialog;
    ProgressDialog progressDialog;
    LinearLayout lay_trans,lay_call, lay_share, lay_loc;
    ArrayList<ShowServicePostModel.PhotoList> imagelist;
    ArrayList<ShowServicePostModel.RuleList> ruleLists;
   // SliderView sliderView;
    RecyclerView recyclerView, showrecycler;
    ServiceRuleAdapter serviceRuleAdapter;
    ShowServiceImageAdapter showServiceImageAdapter;
    String latitude, longitude, isclicked;
    String imagepath2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        progressDialog = new ProgressDialog(this);
        imageView = findViewById(R.id.post_image);
        name = findViewById(R.id.shopname);
        address = findViewById(R.id.address);
        showrating = findViewById(R.id.show_rating);
        number = findViewById(R.id.textnumber);
        showaddress = findViewById(R.id.text_address);
        addrating = findViewById(R.id.add_rating);
        showreviews = findViewById(R.id.lay_reviews);
        countrating = findViewById(R.id.ratingcount);
        imagefav = findViewById(R.id.showfav);
        lay_trans = findViewById(R.id.lay_trans);
        lay_call = findViewById(R.id.lay_call);
        //sliderView = findViewById(R.id.imageSlider);
        lay_share = findViewById(R.id.lay_share);
        lay_loc = findViewById(R.id.lay_location);
        recyclerView = findViewById(R.id.rulelist);
        showrecycler = findViewById(R.id.recylshowimage);
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getToken("Token", this);
        serviceId = sharedPrefManager.getServiceID("ServiceId", this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        showrecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        Log.d("TUSHARSERVICEID", serviceId);
        apiClient = new ApiClient();
        imagelist = new ArrayList<>();
        ruleLists = new ArrayList<>();


        lay_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + latitude + "," + longitude;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });
        lay_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+Number));
                startActivity(intent);
            }
        });
        lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mSource="";
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plan");
                i.putExtra(Intent.EXTRA_SUBJECT,mSource);
                // String body = BaseURL;
                //i.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(i, "Share with"));
            }
        });
        lay_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clicked)
                {
                    clicked = false;
                    getMyTransaction(serviceId);
                }else
                {
                    clicked = true;
                    getUnTransaction(serviceId);
                }
            }
        });
        imagefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clicked)
                {
                    clicked = false;
                    imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.yellow));
                    getFavourite(serviceId);
                }else
                {
                    clicked = true;
                    imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.black));
                    getUnfavourite(serviceId);
                }
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ShowServiceShopDetails.this, ShowImageActivity.class);
                intent.putExtra("imagepath",imagepath2);
                startActivity(intent);
            }
        });
        showreviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(ShowServiceShopDetails.this, ShowReviewActivity.class);
               startActivity(i);
            }
        });
        addrating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Addrating = String.valueOf(addrating.getRating());
                getAddRating(Addrating);
                opendailog();
                Toast.makeText(getApplicationContext(), Addrating, Toast.LENGTH_LONG).show();
            }
        });

        getShowDetails(token, serviceId);


    }

    private void opendailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowServiceShopDetails.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.content_addreviews, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();

        EditText getdesc;
        TextView okay,cancel;
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String desc;
                desc = getdesc.getText().toString();

                if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    AddReview( desc);
                }
            }
        });

        alertDialog.show();
    }

    private void AddReview(String desc) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_REVIEWS_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                params.put("review_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getUnTransaction(String serviceId) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USER_SERVICES_UNMYTRANSACTION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);

                return params;

            }
        };

        RequestHandlerNew.getInstance(ShowServiceShopDetails.this).addToRequestQueue(stringRequest);
    }

    private void getMyTransaction(String serviceId) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.USER_SERVICE_MYTRANSACTION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                        String contact = "918888445128" ; // use country code with your phone number
                        String url = "https://api.whatsapp.com/send?phone=" + contact;
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        startActivity(intent);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);

                return params;

            }
        };

        RequestHandlerNew.getInstance(ShowServiceShopDetails.this).addToRequestQueue(stringRequest);
    }

    private void getUnfavourite(String serviceId) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UNFAV_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getFavourite(String serviceId) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.FAV_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


    private void getAddRating(String addrating) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_RATING_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                     //   alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowServiceShopDetails.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowServiceShopDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                params.put("rating", addrating);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }



    private void getShowDetails(String token, String serviceId) {
        apiClient.apiInterface.showShopdetailservicewise(this.token, serviceId).enqueue(new Callback<ShowServicePostModel>() {
            @Override
            public void onResponse(Call<ShowServicePostModel> call, retrofit2.Response<ShowServicePostModel> response) {
                if (response.isSuccessful()) {

                    imagelist = response.body().getPhoto();
                    ruleLists = response.body().getRule();
                    isclicked = response.body().getIs_favourite();
                    if (isclicked.equals("1"))
                    {
                        imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.yellow));

                    }else
                    {
                        imagefav.setColorFilter(ContextCompat.getColor(getApplication(),R.color.black));

                    }
                    showServiceImageAdapter = new ShowServiceImageAdapter(ShowServiceShopDetails.this, imagelist);
                    showrecycler.setAdapter(showServiceImageAdapter);
                    toolbar.setTitle(response.body().getService().getSus_name());
                    Number = response.body().getService().getSus_mobile();
                    String imagepath = response.body().getService().getLicence();
                    imagepath2 = Base_Url.imagepath + imagepath;

                    Glide.with(getApplication())
                            .load(imagepath2)
                            .into(imageView);
                    name.setText(response.body().getService().getSus_name());
                    address.setText(response.body().getService().getService_address());
                    showrating.setRating(Float.parseFloat(response.body().getService().getAvrage_rating()));
                    number.setText(response.body().getService().getSus_mobile());
                    showaddress.setText(response.body().getService().getService_address());
                    countrating.setText(response.body().getService().getNo_of_rating());
                    serviceRuleAdapter = new ServiceRuleAdapter(ShowServiceShopDetails.this, ruleLists);
                    recyclerView.setAdapter(serviceRuleAdapter);
                    latitude = response.body().getService().getService_latitude();
                    longitude = response.body().getService().getService_longitude();
                }
            }
            @Override
            public void onFailure(Call<ShowServicePostModel> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
