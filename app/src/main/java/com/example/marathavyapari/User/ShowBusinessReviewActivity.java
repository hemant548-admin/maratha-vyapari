package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.ShowBusinessReviewAdapter;
import com.example.marathavyapari.Adapter.ShowServiceReviewAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowBusinessReviewModel;
import com.example.marathavyapari.Model.ShowReViewServiceModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class ShowBusinessReviewActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView back,image;
    TextView toolbar_title;
    RatingBar ratingBar;
    ProgressDialog progressDialog;
    TextView noDataFound;
    ProgressBar progressBar;
    AlertDialog alertDialog;
    RecyclerView mypost_rv;
    SharedPrefManager sharedPrefManager;
    ArrayList<ShowBusinessReviewModel.ReviewList> showBusinessReviewList;
    ShowBusinessReviewAdapter showBusinessReviewAdapter;
    String token, BusinessId;
    ApiClient apiClient;
    TextView title, address, norating;
  //  ImageView addreview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_review);

        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.reviews);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        title = findViewById(R.id.texttitle);
        address = findViewById(R.id.text_address);
        norating = findViewById(R.id.ratingcount);
        ratingBar = findViewById(R.id.show_rating);
        image = findViewById(R.id.post_image);
       // addreview = findViewById(R.id.add_review);
        sharedPrefManager = new SharedPrefManager(ShowBusinessReviewActivity.this);
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        mypost_rv = findViewById(R.id.mypost_rv);
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        apiClient = new ApiClient();
        showBusinessReviewList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        token = sharedPrefManager.getToken("Token", ShowBusinessReviewActivity.this);
        BusinessId = sharedPrefManager.getServiceID("BusinessId", this);
    /*    addreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opendailog();
            }
        });*/
    }

    private void opendailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowBusinessReviewActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.content_addreviews, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();

        EditText getdesc;
        TextView okay,cancel;
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String desc;
                desc = getdesc.getText().toString();

                if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    AddReview( desc);
                }
            }
        });

        alertDialog.show();
    }

    private void AddReview(String desc) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_REVIEWS_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowBusinessReviewActivity.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ShowBusinessReviewActivity.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ShowBusinessReviewActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", BusinessId);
                params.put("review_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onStart() {
        loadPost();
        super.onStart();
    }

    private void loadPost() {
        apiClient.apiInterface.showBusinessReview(token, BusinessId).enqueue(new Callback<ShowBusinessReviewModel>() {
            @Override
            public void onResponse(Call<ShowBusinessReviewModel> call, retrofit2.Response<ShowBusinessReviewModel> response) {
                if (response.isSuccessful()) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        showBusinessReviewList = response.body().getReview();
                        title.setText(showBusinessReviewList.get(0).getBus_name());
                        address.setText(showBusinessReviewList.get(0).getBusiness_address());
                        ratingBar.setRating(Float.parseFloat(showBusinessReviewList.get(0).getAvrage_rating()));
                        norating.setText(showBusinessReviewList.get(0).getNo_of_rating());
                        String imagepath = showBusinessReviewList.get(0).getPhoto();
                        String imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        showBusinessReviewAdapter= new ShowBusinessReviewAdapter(ShowBusinessReviewActivity.this,showBusinessReviewList);
                        mypost_rv.setAdapter(showBusinessReviewAdapter);
                        showBusinessReviewAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        noDataFound.setVisibility(View.VISIBLE);
                        mypost_rv.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<ShowBusinessReviewModel> call, Throwable t) {

            }
        });
    }

    }
