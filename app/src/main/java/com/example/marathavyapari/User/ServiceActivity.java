package com.example.marathavyapari.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Adapter.MyBusinessListAdapter;
import com.example.marathavyapari.Adapter.MyServiceListAdapter;
import com.example.marathavyapari.Adapter.ServiceListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class ServiceActivity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ProgressDialog progressDialog;
    ImageView noDataFound;
    ProgressBar progressBar;
    SharedPrefManager sharedPrefManager;
    RecyclerView recyclerView;
    MyServiceListAdapter myServiceListAdapter;
    ArrayList<ServiceCategoryModel.CategoryList> serviceList;
    ApiClient apiClient;
    String category_id, lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businessactivity);

        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.service);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        serviceList = new ArrayList<>();
        recyclerView = findViewById(R.id.businesslist);
        sharedPrefManager = new SharedPrefManager(ServiceActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", ServiceActivity.this);
        apiClient = new ApiClient();
        // recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getServiceCategory(lang);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {
                        category_id =serviceList.get(position).getSc_id();

                        Intent catlist = new Intent(ServiceActivity.this, SubServiceActivtiy.class);
                        catlist.putExtra("category_id",category_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

    }
    private void getServiceCategory(String lang) {
        apiClient.apiInterface.showServiceCate(lang).enqueue(new Callback<ServiceCategoryModel>() {
            @Override
            public void onResponse(Call<ServiceCategoryModel> call, retrofit2.Response<ServiceCategoryModel> response) {
                if (response.isSuccessful()) {

                    serviceList = response.body().getCategory();
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(ServiceActivity.this,3);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    myServiceListAdapter = new MyServiceListAdapter(ServiceActivity.this, serviceList);
                    recyclerView.setAdapter(myServiceListAdapter);
                    myServiceListAdapter.notifyDataSetChanged();


                }
            }@Override
            public void onFailure(Call<ServiceCategoryModel> call, Throwable t) {

            }
        });
        
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
