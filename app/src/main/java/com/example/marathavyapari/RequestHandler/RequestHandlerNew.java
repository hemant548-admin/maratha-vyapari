package com.example.marathavyapari.RequestHandler;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestHandlerNew
{
    private static com.example.marathavyapari.RequestHandler.RequestHandlerNew mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private RequestHandlerNew(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized com.example.marathavyapari.RequestHandler.RequestHandlerNew getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new com.example.marathavyapari.RequestHandler.RequestHandlerNew(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
