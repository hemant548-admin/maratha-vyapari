package com.example.marathavyapari.Service;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marathavyapari.Activity.RegisterBusiness;
import com.example.marathavyapari.Activity.RegisterBusiness2;
import com.example.marathavyapari.R;

public class ServiceRegister extends AppCompatActivity
{
    LinearLayout register_Btn,back_layout;
    EditText ownerName,serviceName,getAddress,getDesc,getLastname;
    TextView firstname,type;
    String number;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_business);

        register_Btn = findViewById(R.id.business_register);
        back_layout = findViewById(R.id.back_layout);
        ownerName = findViewById(R.id.register_business_getname);
        serviceName = findViewById(R.id.register_business_getbusinessname);
        getAddress = findViewById(R.id.register_business_getaddress);
        getDesc = findViewById(R.id.register_business_getdesc);
        getLastname = findViewById(R.id.register_business_getlastname);

        firstname = findViewById(R.id.firstname);
        type = findViewById(R.id.servicename);

        number = getIntent().getStringExtra("number");
        firstname.setText(R.string.servicename);
        ownerName.setHint(R.string.servicename);

        type.setText(R.string.servicenamee);
        serviceName.setHint(R.string.servicenamee);


        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        register_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                registerservice();
            }
        });

    }

    private void registerservice()
    {
        String name,servicename,address,desc,lastname;

        name = ownerName.getText().toString();
        servicename = serviceName.getText().toString();
        address = getAddress.getText().toString();
        desc = getDesc.getText().toString();
        lastname = getLastname.getText().toString();

        if (name.isEmpty())
        {
            ownerName.setError("Please enter your name");
            ownerName.requestFocus();
        }
        else if (lastname.isEmpty())
        {
            getLastname.setError("Please enter your lastname");
            getLastname.requestFocus();
        }
        else if (servicename.isEmpty())
        {
            serviceName.setError("Please enter your service name");
            serviceName.requestFocus();
        }
        else if (address.isEmpty())
        {
            getAddress.setError("Please enter your service address");
            getAddress.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description of your service");
            getDesc.requestFocus();
        }
        else
        {
            Intent intent = new Intent(ServiceRegister.this, ServiceRegister2.class);
            intent.putExtra("name",name + " " + lastname);
            intent.putExtra("servicename",servicename);
            intent.putExtra("address",address);
            intent.putExtra("desc",desc);
            intent.putExtra("number",number);
            startActivity(intent);
            finish();
        }

    }

}
