package com.example.marathavyapari.Service;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.EditPostActivity;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.ServiceUpdatedModel;
import com.example.marathavyapari.Model.UpdatedServiceModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EditServicePostActivity extends AppCompatActivity
{

    ImageView addpost_image;
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;

    SharedPrefManager sharedPrefManager;
    Bitmap bitmap;
    String postImage= "";

    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 101;

    LinearLayout postBtn,changeImage;
    EditText getTitle,getDesc;
    Uri filepath;
    ProgressDialog progressDialog;
    String token;

    String post_id,title1,image,desc1, title,desc;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        toolbar = findViewById(R.id.add_post_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        sharedPrefManager = new SharedPrefManager(EditServicePostActivity.this);

        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        postBtn = findViewById(R.id.post_layout);
        getTitle = findViewById(R.id.get_post_title);
        getDesc = findViewById(R.id.get_post_desc);
        progressDialog = new ProgressDialog(this);
        addpost_image = findViewById(R.id.addpost_image);
        changeImage = findViewById(R.id.changeimage_layout);

        token = sharedPrefManager.getToken("Token", EditServicePostActivity.this);

        post_id = getIntent().getStringExtra("post_id");
        title1 = getIntent().getStringExtra("post_title");
        getTitle.setText(title1);
        desc1 = getIntent().getStringExtra("post_desc");
        getDesc.setText(desc1);
        image = getIntent().getStringExtra("post_image");

        String imagepath2  = Base_Url.imagepath + image;
        Glide.with(this)
                .load(imagepath2)
                .centerCrop()
                .into(addpost_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar_title.setText(R.string.edit_post);

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                opendialog();
            }
        });

        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post();
            }
        });

    }

    private void post()
    {
        title = getTitle.getText().toString();
        desc = getDesc.getText().toString();

        if (title.isEmpty())
        {
            getTitle.setError("Please enter title");
            getTitle.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else if (postImage.equals(""))
        {
            Toast.makeText(this, "Upload image first", Toast.LENGTH_SHORT).show();
        }
        else
        {
            PostBusinessPost(title,desc);
        }

    }

    private void opendialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditServicePostActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_camera_dialog, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        LinearLayout camera,device;
        camera = dialogView.findViewById(R.id.with_camera);
        device = dialogView.findViewById(R.id.with_device);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_DEVICE_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent,"Select Image"),2);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            imagestore(bitmap);
            addpost_image.setImageBitmap(bitmap);
            addpost_image.setEnabled(false);

        }
        else if (requestCode==2 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imagestore(bitmap);
                addpost_image.setImageBitmap(bitmap);
                addpost_image.setEnabled(false);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void imagestore(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        postImage = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }

    private void PostBusinessPost(String title, String desc)
    {
        progressDialog.setTitle("Posting");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Log.i("TAG", "PostBusinessPost: postImage =  "+postImage);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.POST_SERVICE_EDIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        JSONObject obj = jsonObject.getJSONObject("post");
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN14"+message);
                        UpdatedServiceModel serviceUpdatedModel= new UpdatedServiceModel(
                                obj.getString("post_id"),
                                obj.getString("service_id_fk"),
                                obj.getString("post_title"),
                                obj.getString("post_description"),
                                obj.getString("post_image"),
                                obj.getString("updated_at"),
                                obj.getString("created_at"),
                                obj.getString("is_live")
                        );
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(EditServicePostActivity.this);
                        builder1.setMessage("Your post updated successfully");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(EditServicePostActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(EditServicePostActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("post_image", postImage);
                params.put("post_title", title);
                params.put("post_description", desc);
                params.put("post_id", post_id);


                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}
