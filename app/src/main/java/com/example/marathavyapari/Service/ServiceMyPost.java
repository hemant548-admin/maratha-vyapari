package com.example.marathavyapari.Service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.PostDetailsActivity;
import com.example.marathavyapari.Adapter.MySerViceAdapter;
import com.example.marathavyapari.Adapter.MypostAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.Model.ServiceAllPost;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ServiceMyPost extends AppCompatActivity
{
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    ImageView noDataFound;
    ProgressBar progressBar;
    RecyclerView mypost_rv;
    List<ServiceAllPost> myPostModelList;
    String Token;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_my_post);

        toolbar = findViewById(R.id.mypost_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        sharedPrefManager = new SharedPrefManager(ServiceMyPost.this);
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.mypost);
        /*ServiceLoginModel userModel = SharedPrefManager.getInstance(this).getServiceUser();
        Token = userModel.getToken();*/
        Token = sharedPrefManager.getToken("Token",ServiceMyPost.this);
        Log.d("TUSHARTOKEN", Token);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        mypost_rv = findViewById(R.id.mypost_rv);


        myPostModelList = new ArrayList<>();
        mypost_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        mypost_rv.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mypost_rv, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {

                        String imagepath,title,desc,time,post_id;
                        imagepath = myPostModelList.get(position).getPost_image();
                        title = myPostModelList.get(position).getPost_title();
                        desc = myPostModelList.get(position).getPost_description();
                        time = myPostModelList.get(position).getUpdated_at();
                        post_id = myPostModelList.get(position).getPost_id();

                        Intent catlist = new Intent(ServiceMyPost.this, ServicePostDetails.class);
                        catlist.putExtra("post_image",imagepath);
                        catlist.putExtra("post_title",title);
                        catlist.putExtra("post_time",time);
                        catlist.putExtra("post_desc",desc);
                        catlist.putExtra("post_id",post_id);
//                        startActivity(catlist);

                        Pair[] pairs = new Pair[1];
                        pairs[0] = new Pair<View,String>(view.findViewById(R.id.post_image),"postimage");
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ServiceMyPost.this,pairs);
                        startActivity(catlist,options.toBundle());

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));




    }

    @Override
    protected void onStart()
    {
        loadPost();
        super.onStart();
    }

    private void loadPost()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.ALL_POST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                mypost_rv.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        myPostModelList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("post");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            myPostModelList.add(new ServiceAllPost(
                                    product.getString("post_id"),
                                    product.getString("service_id_fk"),
                                    product.getString("post_title"),
                                    product.getString("post_description"),
                                    product.getString("post_date"),
                                    product.getString("post_time"),
                                    product.getString("post_image"),
                                    product.getString("updated_at"),
                                    product.getString("created_at"),
                                    product.getString("is_live")
                            ));
                        }

                        MySerViceAdapter adapter = new MySerViceAdapter(ServiceMyPost.this, myPostModelList);
                        mypost_rv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        if (adapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            mypost_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(ServiceMyPost.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressBar.setVisibility(View.GONE);
                    mypost_rv.setVisibility(View.VISIBLE);

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                mypost_rv.setVisibility(View.VISIBLE);

                Toast.makeText(ServiceMyPost.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", Token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

}