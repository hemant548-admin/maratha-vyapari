package com.example.marathavyapari.Service;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.PendingApplicationBusiness;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PendingApplicationBusiness2 extends AppCompatActivity
{

    TextView numer;
    String status,token;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_application_business2);


        ServiceLoginModel serviceuser = SharedPrefManager.getInstance(this).getServiceUser();
        status =serviceuser.getService_status();
        token = serviceuser.getToken();
        progressDialog = new ProgressDialog(this);

      if (!status.equals("0"))
        {
            Intent intent = new Intent(PendingApplicationBusiness2.this, ServiceMainActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            checkStatus();
        }

        numer = findViewById(R.id.number);

        progressDialog = new ProgressDialog(this);
     /*   numer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PendingApplicationBusiness2.this, ServiceMainActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

    }

    private void checkStatus() {
        progressDialog.setTitle("Checking");
        progressDialog.setMessage("Please wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_SERVICE_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    progressDialog.dismiss();

                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject object = jsonObject.getJSONObject("profile");

                    String status = object.getString("service_status");
                    if (status.equals("1"))
                    {
                        Intent intent = new Intent(PendingApplicationBusiness2.this, ServiceMainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(PendingApplicationBusiness2.this, "Your Application Is Pending For Approval", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(PendingApplicationBusiness2.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token",token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

}