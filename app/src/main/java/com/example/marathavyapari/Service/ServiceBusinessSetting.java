package com.example.marathavyapari.Service;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.marathavyapari.Activity.BusinessSettings;
import com.example.marathavyapari.Activity.ChangeLanguage;
import com.example.marathavyapari.Activity.ChangePassword;
import com.example.marathavyapari.R;

import java.util.Objects;

public class ServiceBusinessSetting extends AppCompatActivity
{
    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    LinearLayout changePass,changeLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_settings);

        toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.settings);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        changeLang = findViewById(R.id.change_language);
        changePass = findViewById(R.id.change_password);

        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ServiceBusinessSetting.this, ServiceChangePassword.class);
                startActivity(intent);
            }
        });

        changeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent change = new Intent(ServiceBusinessSetting.this,ServiceChangeLanguge.class);
                startActivity(change);
            }
        });




    }
}
