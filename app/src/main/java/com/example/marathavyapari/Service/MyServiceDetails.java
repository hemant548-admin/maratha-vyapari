package com.example.marathavyapari.Service;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.MyShop;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.ServiceUpdatedModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MyServiceDetails extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;
    EditText getName,getBusinessname,getWhatsapp,getAddress,getDesc;
    Spinner getBusinessType;
    String token, Name, ServiceName, Address, WhatNumber, description, photo,name,businessname,type,address,number,desc;
    private static final String[] paths = {"Select/निवडा", "Automobile / वाहन विषयक", "Insurance/विमा", "Real estate / रिअल इस्टेट",
            "Travel / प्रवास", "Health services/आरोग्य सेवा", "Educational services/ शिक्षण सेवा ", "Financial services/आर्थिक सेवा", "Other services/इतर सेवा"};
    LinearLayout saveDetails,uploadShopImg;

    SliderView sliderView;
    List<SliderItem> imagelist;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_service_details);

        toolbar = findViewById(R.id.mtshop_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        sliderView = findViewById(R.id.imageSlider);
       ServiceLoginModel userModel = SharedPrefManager.getInstance(this).getServiceUser();
        token = userModel.getToken();
        Name = userModel.getName();
        ServiceName = userModel.getSc_name();
        Address = userModel.getService_address();
        WhatNumber = userModel.getSus_mobile();
        description = userModel.getService_description();
        photo = userModel.getLicence();
        progressDialog = new ProgressDialog(this);
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.myservice);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getName = findViewById(R.id.ed_owner_name);
        getName.setText(Name);
        getBusinessname = findViewById(R.id.ed_business_name);
        getBusinessname.setEnabled(false);
        getBusinessname.setText(ServiceName);
        getBusinessType = findViewById(R.id.ed_business_type);
        getAddress = findViewById(R.id.ed_address);
        getAddress.setText(Address);
        getWhatsapp = findViewById(R.id.ed_whatsapp_number);
        getWhatsapp.setText(WhatNumber);
        getWhatsapp.setEnabled(false);
        getDesc = findViewById(R.id.ed_description);
        getDesc.setText(description);
        saveDetails = findViewById(R.id.save_details);
        uploadShopImg = findViewById(R.id.upload_shop_img);
        imagelist = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyServiceDetails.this, android.R.layout.simple_spinner_item,paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getBusinessType.setAdapter(adapter);

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updatedetails();
            }
        });

        uploadShopImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MyServiceDetails.this, UploadeddServiceImage.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onStart()
    {
        super.onStart();
        loadBanners();
    }

    private void loadBanners() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_SERVICE_SHOPIMAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("TAG", "onResponse: photos "+response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        imagelist.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("image");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            imagelist.add(new SliderItem(
                                    product.getString("image_id"),
                                    "",
                                    product.getString("service_image")
                            ));
                        }

                        SliderAdapterExample adapter = new SliderAdapterExample(MyServiceDetails.this,imagelist);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();

                    }
                    else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(MyServiceDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                Toast.makeText(MyServiceDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void updatedetails()
    {

        name = getName.getText().toString();
        businessname = getBusinessname.getText().toString();
        type = getBusinessType.getSelectedItem().toString();
        Toast.makeText(this, type, Toast.LENGTH_SHORT).show();
        address = getAddress.getText().toString();
        number = getWhatsapp.getText().toString();
        desc = getDesc.getText().toString();

        if (name.isEmpty())
        {
            getName.setError("Please enter your name");
            getName.requestFocus();
        }
        else if (businessname.isEmpty())
        {
            getBusinessname.setError("Please enter your service name");
            getBusinessname.requestFocus();
        }
        else if (address.isEmpty())
        {
            getAddress.setError("Please enter your service address");
            getAddress.requestFocus();
        }
        else if (number.isEmpty())
        {
            getWhatsapp.setError("Please enter your whatsapp mobile number");
            getWhatsapp.requestFocus();
        }
        else if (number.length() != 10)
        {
            getWhatsapp.setError("Please enter 10 digit number");
            getWhatsapp.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else
        {
            SaveServiceDetails();
        }

    }

    private void SaveServiceDetails() {
        progressDialog.setTitle("Posting");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SERVICEUPDATED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        JSONObject obj = jsonObject.getJSONObject("profile");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        ServiceLoginModel serviceLoginModel= new ServiceLoginModel(
                                token,
                                obj.getString("sus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("sus_mobile"),
                                obj.getString("sus_name"),
                                obj.getString("name"),
                                obj.getString("service_address"),
                                obj.getString("service_description"),
                                obj.getString("service_type"),
                                obj.getString("licence"),
                                obj.getString("service_status"),
                                obj.getString("sc_id"),
                                obj.getString("sc_name"),
                                obj.getString("live_status"),
                                obj.getString("ssc_name")
                        );
                        Log.i("TAG", "onResponse: MADAN12"+message);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MyServiceDetails.this);
                        builder1.setMessage("Your Service updated successfully");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent login = new Intent(MyServiceDetails.this, ServiceMainActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();


                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(MyServiceDetails.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MyServiceDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("service_name", businessname);
                params.put("service_address", address);
                params.put("service_description",desc);
                params.put("service_type", type);


                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }


}