package com.example.marathavyapari.Service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.ContactUs;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ServiceContactUs extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;
    String token, Name, Address, Number;
    ProgressDialog progressDialog;
    EditText getName,getEmail,getDesc;
    LinearLayout sendLayout;
    LinearLayout callText;
    TextView number, address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        toolbar = findViewById(R.id.contactus_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        ServiceLoginModel serviceuser = SharedPrefManager.getInstance(this).getServiceUser();
        token = serviceuser.getToken();
        Name = serviceuser.getName();
        Address = serviceuser.getService_address();
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.contact_us);
        progressDialog = new ProgressDialog(this);
        number = findViewById(R.id.text_numbar);
        address = findViewById(R.id.text_address);
        number.setText("9890374498");
        address.setText(R.string.adressuser);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getName = findViewById(R.id.contactus_name);
        getEmail = findViewById(R.id.contactus_email);
        getDesc = findViewById(R.id.contactus_desc);
        sendLayout = findViewById(R.id.contactus_send);
        callText = findViewById(R.id.contactus_call_txt);
        getName.setText(Name);

        callText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (checkPermission())
                {
                    String number = "+919890374498";
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + number));
                    startActivity(intentcall);
                }
                else
                {
                    requestPermission();
                }
            }
        });

        sendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail();
            }
        });
    }

    private boolean checkPermission()
    {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);

        return result == PackageManager.PERMISSION_GRANTED ;
    }
    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, 200);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 200)
        {
            if (grantResults.length > 0) {
                boolean cameraaccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (cameraaccepted)
                {
                    String number = "+919172063211";
                    Intent intentcall = new Intent(Intent.ACTION_CALL);
                    intentcall.setData(Uri.parse("tel:" + number));
                    startActivity(intentcall);

                }
                else
                {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    private void sendmail()
    {
        String name,email,desc;

        name = getName.getText().toString();
        email = getEmail.getText().toString();
        desc = getDesc.getText().toString();

        if (name.isEmpty())
        {
            getName.setError("Please enter your name");
            getName.requestFocus();
        }
        else if (email.isEmpty())
        {
            getEmail.setError("Please enter your gmail id");
            getEmail.requestFocus();
        } else if (!isEmailValid(email)){
            getEmail.setError("please enter valid email address");
            getEmail.requestFocus();
        }
        else if (desc.isEmpty())
        {
            getDesc.setError("Please enter description");
            getDesc.requestFocus();
        }
        else
        {
            sendMessage(name,email,desc);
        }

    }

    private boolean isEmailValid(String email) {
        Boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void sendMessage(String name, String email, String desc) {
        progressDialog.setTitle("Sending");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SERVICE_SEND_CONTACTMSG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServiceContactUs.this, message, Toast.LENGTH_SHORT).show();
                        getName.getText().clear();
                        getEmail.getText().clear();
                        getDesc.getText().clear();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServiceContactUs.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ServiceContactUs.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("service_email", email);
                params.put("contact_message", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}