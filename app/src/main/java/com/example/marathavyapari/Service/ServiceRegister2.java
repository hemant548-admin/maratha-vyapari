package com.example.marathavyapari.Service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.RegisterBusiness2;
import com.example.marathavyapari.Activity.UploadBusinessDoc;
import com.example.marathavyapari.Adapter.MyServiceListAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessCatModel;
import com.example.marathavyapari.Model.BusinessSubCatModel;
import com.example.marathavyapari.Model.DistrictDataModel;
import com.example.marathavyapari.Model.DistrictModel;
import com.example.marathavyapari.Model.ServiceCatModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.Model.TalukaDataModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.ServiceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class ServiceRegister2 extends AppCompatActivity
{
    LinearLayout nextBtn,back_layout;
    EditText whatsappNumber,getPassword,getconfirmPassword;
    String name,servicename,address,desc, serviceCatId, serviceSubCatId, districtname, districtid, talukaname, talukaid, lang;
    Spinner businessType, servicecatType, serviceSubCatType;
    ImageView visiblePass,visiblePass2;
    private static final String[] paths = {"Select/निवडा", "Automobile / वाहन विषयक", "Insurance/विमा", "Real estate / रिअल इस्टेट",
            "Travel / प्रवास", "Health services/आरोग्य सेवा", "Educational services/ शिक्षण सेवा ", "Financial services/आर्थिक सेवा", "Other services/इतर सेवा"};
    TextView servieType;
    ApiClient apiClient;
    ArrayList<ServiceCategoryModel.CategoryList> servicecatList;
    ArrayList<DistrictModel.DistrictsList> districtsList;
    ArrayList<TalukaDataModel.TalukaList> talukaList;
    ArrayList<ServiceSubCatModel.SubCategoryList> serviceSubCatlists;
    String number;
    protected boolean gps_enabled, network_enabled;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private static final int REQUEST_LOCATION = 1;
    private String latitude, longitude;
    Spinner district, taluka;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_service2);

        nextBtn = findViewById(R.id.business_register2);
        back_layout = findViewById(R.id.back_layout);
        businessType = findViewById(R.id.register_business_gettype);
        whatsappNumber = findViewById(R.id.register_business_whtsnumber);
        getPassword = findViewById(R.id.register_business_getpassword);
        getconfirmPassword = findViewById(R.id.register_business_getpasswordcnf);
        visiblePass = findViewById(R.id.visibility);
        visiblePass2 = findViewById(R.id.visibility2);
        servieType = findViewById(R.id.service_type);
        servicecatType = findViewById(R.id.register_business_getcat);
        serviceSubCatType = findViewById(R.id.register_business_getsubcat);
        district = findViewById(R.id.register_districts);
        taluka = findViewById(R.id.register_taluka);
        servieType.setText(R.string.servicetype);

        sharedPrefManager = new SharedPrefManager(ServiceRegister2.this);
        apiClient = new ApiClient();

        servicecatList = new ArrayList<>();
        serviceSubCatlists = new ArrayList<>();
        districtsList = new ArrayList<>();
        talukaList = new ArrayList<>();
        name = getIntent().getStringExtra("name");
        servicename = getIntent().getStringExtra("servicename");
        address = getIntent().getStringExtra("address");
        desc = getIntent().getStringExtra("desc");

        lang = sharedPrefManager.getLanguage("Lang", ServiceRegister2.this);
        number = getIntent().getStringExtra("number");
        whatsappNumber.setText(number);

        getDistricts(lang);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ServiceRegister2.this, R.layout.checked_text_white,paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        businessType.setAdapter(adapter);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }

        getServiceCategory(lang);

        servicecatType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent1, View view1, int position, long id1) {

               serviceCatId = servicecatList.get(position).getSc_id();
                getSubCatList(serviceCatId,lang);
                Log.i("TAG", "onItemSelected: Service CategoryId = " +serviceCatId);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                districtid = districtsList.get(i).getDistrict_id();
                Log.d("TusharDistrictId", districtid);
                getTaluka(districtid, lang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        taluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                talukaid = talukaList.get(i).getTaluka_id();
                Log.d("TUSHARTALULAID", talukaid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        serviceSubCatType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent1, View view1, int position, long id1) {

                serviceSubCatId = serviceSubCatlists.get(position).getSsc_id();
                Log.i("TAG", "onItemSelected: SubCategoryId = " + serviceSubCatId);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                register2();
            }
        });

        visiblePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ShowHidePass(v);
            }
        });

        visiblePass2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePass2(v);
            }
        });

    }

    private void getDistricts(String lang) {
        apiClient.apiInterface.districtlist(lang).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, retrofit2.Response<DistrictModel> response) {
                if (response.isSuccessful()) {
                    districtsList = response.body().getDistricts();

                    if (districtsList.size() >= 0) {

                        List<String> districtlistarray = new ArrayList<String>();
                        if (districtlistarray.size() > 0) {
                            districtlistarray.clear();
                        }
//
                        for (int i = 0; i < districtsList.size() ; i++)
                        {
                            String distlist = districtsList.get(i).getDistrict_name();
                            districtlistarray.add(distlist);
                        }
                        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(ServiceRegister2.this, R.layout.checked_text_white, districtlistarray);
                        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        district.setAdapter(dataAdapter3);
                    }

                }
            }@Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {

            }
        });
    }

    private void getTaluka(String districtid, String lang) {

        apiClient.apiInterface.talukalist(districtid, lang).enqueue(new Callback<TalukaDataModel>() {
            @Override
            public void onResponse(Call<TalukaDataModel> call, retrofit2.Response<TalukaDataModel> response) {
                if (response.isSuccessful()) {
                    talukaList = response.body().getTaluka();

                    if (talukaList.size() >= 0) {

                        List<String> usernamearry = new ArrayList<String>();
                        if (usernamearry.size() > 0) {
                            usernamearry.clear();
                        }
                        for (int i = 0; i < talukaList.size(); i++) {
                            String username = talukaList.get(i).getTaluka_name();

                            Log.e("TAG", "onResponse: username  " + username);
                            usernamearry.add(username);
                        }

                        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(ServiceRegister2.this, R.layout.checked_text_white, usernamearry);
                        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        taluka.setAdapter(dataAdapter2);
                    }

                }
            }@Override
            public void onFailure(Call<TalukaDataModel> call, Throwable t) {

            }
        });

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                ServiceRegister2.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                ServiceRegister2.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.d("TAG", "onResponse: " + latitude);
                Log.d("TAG", "onResponse: " + longitude);

            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getSubCatList(String serviceCatId, String lang) {
        apiClient.apiInterface.showSubCat(serviceCatId, lang).enqueue(new Callback<ServiceSubCatModel>() {
            @Override
            public void onResponse(Call<ServiceSubCatModel> call, retrofit2.Response<ServiceSubCatModel> response) {
                if (response.isSuccessful()) {
                    serviceSubCatlists = response.body().getSub_category();

                    if (serviceSubCatlists.size() >= 0) {

                        List<String> usernamearry = new ArrayList<String>();
                        if (usernamearry.size() > 0) {
                            usernamearry.clear();
                        }
                        for (int i = 0; i < serviceSubCatlists.size(); i++) {
                            String username = serviceSubCatlists.get(i).getSsc_name();

                            Log.e("TAG", "onResponse: username  " + username);
                            usernamearry.add(username);
                        }

                        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(ServiceRegister2.this, R.layout.checked_text_white, usernamearry);
                        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        serviceSubCatType.setAdapter(dataAdapter1);
                    }

                }
                }@Override
            public void onFailure(Call<ServiceSubCatModel> call, Throwable t) {

            }
        });

    }

    private void getServiceCategory(String lang) {
        apiClient.apiInterface.showServiceCate(lang).enqueue(new Callback<ServiceCategoryModel>() {
            @Override
            public void onResponse(Call<ServiceCategoryModel> call, retrofit2.Response<ServiceCategoryModel> response) {
                if (response.isSuccessful()) {

                    servicecatList = response.body().getCategory();
                    List<String> catlistarray = new ArrayList<String>();
                    if (catlistarray.size() > 0) {
                        catlistarray.clear();
                    }
//                        catlistarray.add(0, "Select/निवडा");
                    for (int i = 0; i < servicecatList.size() ; i++)
                    {
                        String catlist = servicecatList.get(i).getSc_name();
                        catlistarray.add(catlist);
                    }

                    ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(ServiceRegister2.this,R.layout.checked_text_white, catlistarray);
                    dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    servicecatType.setAdapter(dataAdapter5);



                }
            }@Override
            public void onFailure(Call<ServiceCategoryModel> call, Throwable t) {

            }
        });

    }

    public void ShowHidePass(View view) {

        if(view.getId()==R.id.visibility){
            if(getPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public void ShowHidePass2(View view) {

        if(view.getId()==R.id.visibility2){
            if(getconfirmPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_off_24);
                //Show Password
                getconfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_baseline_visibility_24);
                //Hide Password
                getconfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }



    private void register2()
    {
        String type,whnumber,password,cnfpassword;

        type = businessType.getSelectedItem().toString();
        whnumber = whatsappNumber.getText().toString();
        password = getPassword.getText().toString();
        cnfpassword = getconfirmPassword.getText().toString();

        if (type.equals("Select"))
        {
            Toast.makeText(this, "Select Service Type", Toast.LENGTH_SHORT).show();
        }
        else if (whnumber.isEmpty())
        {
            whatsappNumber.setError("Please enter your whatsapp number");
            whatsappNumber.requestFocus();
        }
        else if (whnumber.length() != 10)
        {
            whatsappNumber.setError("Please enter 10 digits number");
            whatsappNumber.requestFocus();
        }
        else if (password.isEmpty())
        {
            getPassword.setError("Please enter password");
            getPassword.requestFocus();
        }
        else if (password.length() < 6)
        {
            getPassword.setError("Please enter password more than 6 char");
            getPassword.requestFocus();
        }
        else if (!isValidPassword(password))
        {
            getPassword.setError("Password must contain upper letters,numbers and special symbols");
            getPassword.requestFocus();
        }
        else if (cnfpassword.isEmpty())
        {
            getconfirmPassword.setError("Please enter your password");
            getconfirmPassword.requestFocus();
        }
        else if (!password.equals(cnfpassword))
        {
            getconfirmPassword.setError("Password didn't matched");
            getconfirmPassword.requestFocus();
        }
        else
        {
            Intent intent = new Intent(ServiceRegister2.this, UploadServiceDoc.class);
            intent.putExtra("type",type);
            intent.putExtra("whatsapp_number",whnumber);
            intent.putExtra("password",password);
            intent.putExtra("name",name);
            intent.putExtra("servicename",servicename);
            intent.putExtra("address",address);
            intent.putExtra("desc",desc);
            intent.putExtra("ServiceCatId", serviceCatId);
            intent.putExtra("ServiceSubCatId", serviceSubCatId);
            intent.putExtra("DistrictId", districtid);
            intent.putExtra("TalukaId", talukaid);
            intent.putExtra("Latitude", latitude);
            intent.putExtra("Longitude", longitude);
            startActivity(intent);
            finish();
        }

    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return PASSWORD_PATTERN.matcher(s).matches();
    }
}