package com.example.marathavyapari.Service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.AddPostBusiness;
import com.example.marathavyapari.Activity.BusinessSettings;
import com.example.marathavyapari.Activity.ContactUs;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.MyPosts;
import com.example.marathavyapari.Activity.MyReviews;
import com.example.marathavyapari.Activity.MyShop;
import com.example.marathavyapari.Activity.Notifications;
import com.example.marathavyapari.Activity.PrivacyPolicy;
import com.example.marathavyapari.Activity.SelectLanguage;
import com.example.marathavyapari.Activity.TermsAndConditions;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Adapter.ShowNotifyAdapter;
import com.example.marathavyapari.Adapter.SliderAdapterExample;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.MainActivity;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ReadNotiModel;
import com.example.marathavyapari.Model.ResponseFCMModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.Model.UnreadNotificationModel;
import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.ServiceActivity;
import com.example.marathavyapari.User.UserMainActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ServiceMainActivity extends AppCompatActivity
{
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    LinearLayout mainLayout,addPost;

    CircleImageView userPhoto;
    TextView shopName, shopAddress, shopNumber;
    String fcmregistrationId;
    List<SliderItem> imagelist;
    String serviceId,lang, latitude, longitude;
    ApiClient apiClient;

    protected boolean gps_enabled, network_enabled;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private static final int REQUEST_LOCATION = 1;

    LinearLayout myShop,Privacy,Terms,Notifications,MyPost,MyReviews;
    SliderView sliderView;
    private TextView notifCount;
    String mNotifCount;
    SharedPrefManager sharedPrefManager;
    ArrayList<UserNotificationModel.NotificationList> notificationList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_main);

        setUpToolbar();

        ServiceLoginModel userModel = SharedPrefManager.getInstance(this).getServiceUser();
        serviceId = userModel.getSus_id();
        Log.d("TUSHARUSERID", serviceId);
        addPost = findViewById(R.id.add_post_home);
        apiClient = new ApiClient();
        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent post = new Intent(ServiceMainActivity.this, AddPostService.class);
                startActivity(post);
            }
        });

        imagelist = new ArrayList<>();
        sharedPrefManager = new SharedPrefManager(ServiceMainActivity.this);
        lang = sharedPrefManager.getLanguage("Lang", ServiceMainActivity.this);
        loadBanners();
       // getNotification(serviceId, lang);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmregistrationId = instanceIdResult.getToken();
                Log.d("TUSHARFCMREGISTRATION", fcmregistrationId);

                 getSaveData(serviceId, fcmregistrationId, lang, latitude, longitude);
            }
        });
        navigationView = findViewById(R.id.main_navigationview);
        //navigationView.setCheckedItem(R.id.home);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {

                    case R.id.myservice:

                        Intent myshop = new Intent(ServiceMainActivity.this, MyServiceDetails.class);
                        startActivity(myshop);

                        break;


                    case R.id.contactus:

                        Intent contact = new Intent(ServiceMainActivity.this, ServiceContactUs.class);
                        startActivity(contact);


                        break;

                    case R.id.rules:

                        Intent rules = new Intent(ServiceMainActivity.this, ServieTermsConditions.class);
                        startActivity(rules);

                        break;

                    case R.id.notifications:
                        getReadNotification(serviceId);
                        Intent noti = new Intent(ServiceMainActivity.this, ServiceNotifications.class);
                        startActivity(noti);

                        break;

                    case R.id.settings:

                        Intent settings = new Intent(ServiceMainActivity.this, ServiceBusinessSetting.class);

                        startActivity(settings);

                        break;

                    case R.id.my_review:

                        Intent review = new Intent(ServiceMainActivity.this, ServiceMyReviews.class);
                        startActivity(review);

                        break;

                    case R.id.privacy:

                        Intent privacyy = new Intent(ServiceMainActivity.this, ServicePrivacyPolicy.class);
                        startActivity(privacyy);

                        break;

                    case R.id.my_post:

                        Intent post = new Intent(ServiceMainActivity.this, ServiceMyPost.class);
                        startActivity(post);

                        break;

                    case R.id.rateus:
                        reviewOnApp();

                        break;

                    case R.id.logout:
//                        tokenLogout();
//                        SharedPrefManager.getInstance(MainActivity.this).logout();
//                        finish();

                        new AlertDialog.Builder(ServiceMainActivity.this)
                                .setTitle("Logout") .setMessage("Do you really want to Logout?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                                        { public void onClick(DialogInterface dialog, int which)
                                        {
                                            SharedPrefManager.getInstance(ServiceMainActivity.this).logout();
                                            Intent intent = new Intent(ServiceMainActivity.this, SelectLanguage.class);
                                            intent.putExtra("account_type","service");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                            Toast.makeText(ServiceMainActivity.this, "Logout successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                        }
                                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss(); } })
                                .setIcon(android.R.drawable.ic_dialog_alert) .show();
                }


                return false;
            }
        });

        View headerView = navigationView.getHeaderView(0);

        shopName = headerView.findViewById(R.id.user_businessname);
        shopAddress = headerView.findViewById(R.id.user_address);
        shopNumber = headerView.findViewById(R.id.user_contact);
        userPhoto = headerView.findViewById(R.id.profile_image);


        shopName.setText(userModel.getSsc_name());
        shopAddress.setText(userModel.getService_address());
        shopNumber.setText(userModel.getSus_mobile());
        String imagepath = Base_Url.imagepath + userModel.getLicence();
        Glide.with(this)
                .load(imagepath)
                .into(userPhoto);

        myShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myshop = new Intent(ServiceMainActivity.this, MyServiceDetails.class);
                startActivity(myshop);
            }
        });

        Privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent privacy = new Intent(ServiceMainActivity.this, ServicePrivacyPolicy.class);
                startActivity(privacy);
            }
        });

        Terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rules = new Intent(ServiceMainActivity.this, ServieTermsConditions.class);
                startActivity(rules);
            }
        });

        Notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReadNotification(serviceId);
                Intent noti = new Intent(ServiceMainActivity.this, ServiceNotifications.class);
                startActivity(noti);
            }
        });

        MyPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent post = new Intent(ServiceMainActivity.this, ServiceMyPost.class);
                startActivity(post);
            }
        });

        MyReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(ServiceMainActivity.this, ServiceMyReviews.class);
                startActivity(review);
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                ServiceMainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                ServiceMainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.d("TAG", "onResponseTUSHARLATI: " + latitude);
                Log.d("TAG", "onResponseTUSHARLong: " + longitude);

            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void OnGPS() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getNotification(String serviceId, String lang) {
        apiClient.apiInterface.showServiceNotify(serviceId, lang).enqueue(new Callback<UserNotificationModel>() {
            @Override
            public void onResponse(Call<UserNotificationModel> call, retrofit2.Response<UserNotificationModel> response) {
                if (response.isSuccessful()) {
                    mNotifCount = response.body().getCount();
                    notifCount.setText(mNotifCount);
                    Log.d("Tusharcount", mNotifCount);
                }
            }@Override
            public void onFailure(Call<UserNotificationModel> call, Throwable t) {

            }
        });
    }

    private void getSaveData(String serviceId, String fcmregistrationId, String lang, String latitude, String longitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.SERVICEFCMUPDATED, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");

                    if(jsonObject.getString("status").equals("true"))
                    {

                        String message = jsonObject.getString("message");
                        //Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN13"+fcmregistrationId);
                        Log.i("TAG", "onResponse: MADAN15"+message);

                    }else
                    {

                        String message = jsonObject.getString("message");
                        // Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN14"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               // Toast.makeText(ServiceMainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceId);
                params.put("fcm_token", fcmregistrationId);
                params.put("language", lang);
                params.put("latitude", latitude);
                params.put("longitude", longitude);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void loadBanners()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.GET_BANNER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("banner");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            imagelist.add(new SliderItem(
                                    product.getString("banner_id"),
                                    product.getString("banner_title"),
                                    product.getString("banner_image")
                            ));
                        }

                        SliderAdapterExample adapter = new SliderAdapterExample(ServiceMainActivity.this,imagelist);
                        sliderView.setSliderAdapter(adapter);

                        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        sliderView.startAutoCycle();

                    }
                    else
                    {

                        String message = jsonObject.getString("message");
//                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                Toast.makeText(ServiceMainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) ;

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }



    private void setUpToolbar()
    {
        drawerLayout = findViewById(R.id.main_drawer_layout);
        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle(R.string.app_name);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        myShop = findViewById(R.id.myshop_main);
        Privacy = findViewById(R.id.privacy_main);
        Terms = findViewById(R.id.terms_main);
        Notifications = findViewById(R.id.notifications_main);
        MyPost = findViewById(R.id.mypost_main);
        MyReviews = findViewById(R.id.review_main);
        sliderView = findViewById(R.id.imageSlider);

    }

    public void reviewOnApp()
    {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent gotorate = new Intent(Intent.ACTION_VIEW, uri);
        gotorate.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(gotorate);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_home_menu,menu);
        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        notifCount = (TextView) MenuItemCompat.getActionView(item);
        getServiceUnreadNoti(serviceId);
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReadNotification(serviceId);
                Intent noti = new Intent(ServiceMainActivity.this, ServiceNotifications.class);
                startActivity(noti);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void getServiceUnreadNoti(String serviceId) {
        apiClient.apiInterface.showServiceUnreadNoti(serviceId).enqueue(new Callback<UnreadNotificationModel>() {
            @Override
            public void onResponse(Call<UnreadNotificationModel> call, retrofit2.Response<UnreadNotificationModel> response) {
                if (response.isSuccessful()) {
                    mNotifCount = response.body().getCount();
                    notifCount.setText(mNotifCount);
                    Log.d("Tusharcount", mNotifCount);
                }
            }@Override
            public void onFailure(Call<UnreadNotificationModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.notification:
                getReadNotification(serviceId);
                Intent notify = new Intent(ServiceMainActivity.this, ServiceNotifications.class);
                startActivity(notify);
                break;
        }
        return true;
    }

    private void getReadNotification(String serviceId) {
        apiClient.apiInterface.showServicereadnoti(serviceId).enqueue(new Callback<ReadNotiModel>() {
            @Override
            public void onResponse(Call<ReadNotiModel> call, retrofit2.Response<ReadNotiModel> response) {
                if (response.isSuccessful()) {

                }
            }@Override
            public void onFailure(Call<ReadNotiModel> call, Throwable t) {

            }
        });
    }

    @Override public void onBackPressed() {
        new AlertDialog.Builder(ServiceMainActivity.this)
                .setTitle("Exit Page") .setMessage("Do you really want to exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                        { public void onClick(DialogInterface dialog, int which)
                        { ActivityCompat.finishAffinity(ServiceMainActivity.this);
                            finish();
                        }
                        }
                ) .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss(); } })
                .setIcon(android.R.drawable.ic_dialog_alert) .show();

    }
}