package com.example.marathavyapari.Service;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.Activity.EditRuleActivity;
import com.example.marathavyapari.Activity.TermsAndConditions;
import com.example.marathavyapari.Adapter.MyServiceRulesAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceDataModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ServieTermsConditions extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView back;

    LinearLayout addTerms;

    ProgressDialog progressDialog;
    String token;

    SharedPrefManager sharedPrefManager;
    ImageView noDataFound;
    ProgressBar progressBar;
    RecyclerView myrules_rv;
    List<ServiceDataModel> rulesModelBusinessList;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servie_terms_conditions);

        toolbar = findViewById(R.id.terms_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        ServiceLoginModel userModel = SharedPrefManager.getInstance(this).getServiceUser();
        token = userModel.getToken();
        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.termsandcond);
        progressDialog = new ProgressDialog(this);
        addTerms = findViewById(R.id.add_terms_conditions);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addTerms = findViewById(R.id.add_terms_conditions);

        progressBar = findViewById(R.id.rules_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        myrules_rv = findViewById(R.id.myrules_rv);

        rulesModelBusinessList = new ArrayList<>();
        myrules_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        myrules_rv.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), myrules_rv, new
                RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position)
                    {

                        String imagepath,title,desc,time,rule_id;

                        title = rulesModelBusinessList.get(position).getRule_title();
                        desc = rulesModelBusinessList.get(position).getRule_description();
                        time = rulesModelBusinessList.get(position).getCreated_at();
                        rule_id = rulesModelBusinessList.get(position).getSr_id();

                        Intent catlist = new Intent(ServieTermsConditions.this, EditSerVcceRuleActivity.class);
                        catlist.putExtra("rule_title",title);
                        catlist.putExtra("rule_time",time);
                        catlist.putExtra("rule_desc",desc);
                        catlist.putExtra("rule_id",rule_id);
                        startActivity(catlist);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

        addTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                opendialog();
            }
        });
    }  
    @Override
protected void onStart()
{
    loadRules();
    super.onStart();
}

    private void loadRules() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Base_Url.ADD_SERVICESRULE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                myrules_rv.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        rulesModelBusinessList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("rule");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            rulesModelBusinessList.add(new ServiceDataModel(
                                    product.getString("sr_id"),
                                    product.getString("service_id_fk"),
                                    product.getString("rule_title"),
                                    product.getString("rule_description"),
                                    product.getString("created_at"),
                                    product.getString("live_status")
                            ));
                        }

                        MyServiceRulesAdapter adapter = new MyServiceRulesAdapter(ServieTermsConditions.this, rulesModelBusinessList);
                        myrules_rv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        if (adapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            myrules_rv.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {

                        String message = jsonObject.getString("message");
                        Toast.makeText(ServieTermsConditions.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e)
                {
                    progressBar.setVisibility(View.GONE);
                    myrules_rv.setVisibility(View.VISIBLE);

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                myrules_rv.setVisibility(View.VISIBLE);

                Toast.makeText(ServieTermsConditions.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void opendialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ServieTermsConditions.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.addterms_conditions, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();

        EditText gettitle,getdesc;
        TextView okay,cancel;
        gettitle = dialogView.findViewById(R.id.terms_title);
        getdesc = dialogView.findViewById(R.id.terms_desc);
        okay = dialogView.findViewById(R.id.okay_terms);
        cancel = dialogView.findViewById(R.id.cancel_terms);



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String title,desc;
                title = gettitle.getText().toString();
                desc = getdesc.getText().toString();

                if (title.isEmpty())
                {
                    gettitle.setError("Please enter title");
                    gettitle.requestFocus();
                }
                else if (desc.isEmpty())
                {
                    getdesc.setError("Please enter description");
                    getdesc.requestFocus();
                }
                else
                {
                    AddRules(title, desc);
                }
            }
        });

        alertDialog.show();
    }

    private void AddRules(String title, String desc) {
        progressDialog.setTitle("Adding");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.ADD_RULES_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServieTermsConditions.this, message, Toast.LENGTH_SHORT).show();
                        onStart();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServieTermsConditions.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ServieTermsConditions.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("rule_title", title);
                params.put("rule_description", desc);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

}