package com.example.marathavyapari.Service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.MyReviews;
import com.example.marathavyapari.Adapter.BusinessReviewAdapter;
import com.example.marathavyapari.Adapter.ServiceReviewAdapter;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessReviewModel;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.ServiceReviewModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ServiceMyReviews extends AppCompatActivity
{
    Toolbar toolbar;
    TextView toolbar_title;
    List<ServiceReviewModel> serviceReviewList;
    RecyclerView recyclerView;
    TextView title, address, norating;
    ServiceReviewAdapter serviceReviewAdapter;
    RatingBar ratingBar;
    ImageView image,noDataFound,back;
    String token, serviceid, Title, Address, NoRating;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_my_reviews);

        toolbar = findViewById(R.id.review_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);


        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        toolbar_title.setText(R.string.myreview);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title = findViewById(R.id.texttitle);
        address = findViewById(R.id.text_address);
        norating = findViewById(R.id.ratingcount);
        ratingBar = findViewById(R.id.show_rating);
        image = findViewById(R.id.post_image);
        progressBar = findViewById(R.id.mypost_progressbar);
        noDataFound = findViewById(R.id.nodata_found);
        recyclerView = findViewById(R.id.myreview_rv);
        serviceReviewList = new ArrayList<>();
        ServiceLoginModel serviceuser = SharedPrefManager.getInstance(this).getServiceUser();
        token = serviceuser.getToken();
        serviceid = serviceuser.getSus_id();
        Log.d("TusharSu", serviceid);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getServiceReview(serviceid);
    }

    private void getServiceReview(String serviceid) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.REVIEW_SERVICE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        serviceReviewList.clear();
                        String message = jsonObject.getString("message");
                        Log.i("TAG", "onResponse: MADAN"+message);
                        JSONArray array = jsonObject.getJSONArray("review");

                        for (int i = 0;i<array.length();i++)
                        {
                            JSONObject product = array.getJSONObject(i);

                            //adding the product to product list
                            serviceReviewList.add(new ServiceReviewModel(
                                    product.getString("sr_id"),
                                    product.getString("user_id_fk"),
                                    product.getString("service_id_fk"),
                                    product.getString("review_message"),
                                    product.getString("review_date"),
                                    product.getString("review_time"),
                                    product.getString("sus_id"),
                                    product.getString("category_id_fk"),
                                    product.getString("sus_mobile"),
                                    product.getString("sus_name"),
                                    product.getString("name"),
                                    product.getString("service_address"),
                                    product.getString("service_description"),
                                    product.getString("service_type"),
                                    product.getString("licence"),
                                    product.getString("service_latitude"),
                                    product.getString("service_longitude"),
                                    product.getString("avrage_rating"),
                                    product.getString("no_of_rating"),
                                    product.getString("uus_id"),
                                    product.getString("uus_mobile"),
                                    product.getString("user_email"),
                                    product.getString("user_name"),
                                    product.getString("user_photo")
                            ));
                        }
                        String imagepath = serviceReviewList.get(0).getLicence();

                        //loading the image
                        String imagepath2  = Base_Url.imagepath + imagepath; ;
                        Glide.with(getApplication())
                                .load(imagepath2)
                                .into(image);
                        title.setText(serviceReviewList.get(0).getSus_name());
                        address.setText(serviceReviewList.get(0).getService_description());
                        ratingBar.setRating(Float.parseFloat(serviceReviewList.get(0).getAvrage_rating()));
                        norating.setText(serviceReviewList.get(0).getNo_of_rating());
                        serviceReviewAdapter = new ServiceReviewAdapter(ServiceMyReviews.this,serviceReviewList);
                        recyclerView.setAdapter(serviceReviewAdapter);
                        serviceReviewAdapter.notifyDataSetChanged();

                        if (serviceReviewAdapter.getItemCount() == 0)
                        {
                            noDataFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                    }else
                    {
                        noDataFound.setVisibility(View.VISIBLE);
                        String message = jsonObject.getString("message");
                        //Toast.makeText(ServiceMyReviews.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }

                } catch (JSONException e) {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                Toast.makeText(ServiceMyReviews.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceid);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }
}