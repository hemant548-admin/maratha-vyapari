package com.example.marathavyapari.Service;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.UploadShopImages;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class UploadServiceDoc extends AppCompatActivity
{
    LinearLayout upload_doc,back_layout;
    ImageView doc1;
    String name,servicename,address,desc,type,whatsappnumber,password, tapshil, districtId, talukaId, latitude, longitude;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int MY_DEVICE_CODE = 200;
    Bitmap bitmap;
    SharedPrefManager sharedPrefManager;
    String parwana = "",photo="";
    ApiClient apiClient;
    Uri filepath;
    String categoryId,SubCatId;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_service_doc);

        back_layout = findViewById(R.id.back_layout);
        upload_doc = findViewById(R.id.upload_doc);
        doc1 = findViewById(R.id.business_document_1);

        progressDialog = new ProgressDialog(this);

        sharedPrefManager = new SharedPrefManager(this);
        name = getIntent().getStringExtra("name");
        servicename = getIntent().getStringExtra("servicename");
        address = getIntent().getStringExtra("address");
        desc = getIntent().getStringExtra("desc");
        type = getIntent().getStringExtra("type");
        whatsappnumber = getIntent().getStringExtra("whatsapp_number");
        password = getIntent().getStringExtra("password");
        categoryId = getIntent().getStringExtra("ServiceCatId");
        SubCatId = getIntent().getStringExtra("ServiceSubCatId");
        latitude = getIntent().getStringExtra("Latitude");
        longitude = getIntent().getStringExtra("Longitude");
        districtId = getIntent().getStringExtra("DistrictId");
        talukaId = getIntent().getStringExtra("TalukaId");

        apiClient = new ApiClient();
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        doc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                showdialog();
            }
        });


        upload_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (parwana.equals(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(UploadServiceDoc.this);
                    builder1.setMessage("Please Upload All Documents.");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else
                {
                    registeruser();

                }
            }
        });


    }


    private void registeruser()
    {

        progressDialog.setTitle("Register");
        progressDialog.setMessage("Please wait...!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.GET_SERVICE_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        JSONObject obj = jsonObject.getJSONObject("profile");
//                        Toast.makeText(UploadBusinessDoc.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                        ServiceLoginModel serviceuser= new ServiceLoginModel(
                                jsonObject.getString("token"),
                                obj.getString("sus_id"),
                                obj.getString("category_id_fk"),
                                obj.getString("subcategory_id_fk"),
                                obj.getString("sus_mobile"),
                                obj.getString("sus_name"),
                                obj.getString("name"),
                                obj.getString("service_address"),
                                obj.getString("service_description"),
                                obj.getString("service_type"),
                                obj.getString("licence"),
                                obj.getString("service_status"),
                                obj.getString("sc_id"),
                                obj.getString("sc_name"),
                                obj.getString("live_status"),
                                obj.getString("ssc_name")
                        );
                        SharedPrefManager.getInstance(getApplicationContext()).userserviceLogin(serviceuser);
                        sharedPrefManager.setUserType("UserType", jsonObject.getString("user_type"), UploadServiceDoc.this);
                        final Dialog dialog = new Dialog(UploadServiceDoc.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.register_successffully_dialog);

                        Button dialogButton = (Button) dialog.findViewById(R.id.okay_btn);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                               Intent login = new Intent(UploadServiceDoc.this,PendingApplicationBusiness2.class);
                                login.putExtra("account_type","business");
                                login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(login);
                                finish();

                            }
                        });

                        dialog.show();

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(UploadServiceDoc.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UploadServiceDoc.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("owner_name", name);
                params.put("mobile", whatsappnumber);
                params.put("password", password);
                params.put("service_name", servicename);
                params.put("service_address", address);
                params.put("service_description", desc);
                params.put("licence", parwana);
                params.put("service_type", type);
                params.put("category_id", categoryId);
                params.put("subcategory_id", SubCatId);
                params.put("district_id", districtId);
                params.put("taluka_id", talukaId);
                return params;
            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showdialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(UploadServiceDoc.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_camera_dialog, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        LinearLayout camera,device;
        camera = dialogView.findViewById(R.id.with_camera);
        device = dialogView.findViewById(R.id.with_device);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_DEVICE_CODE);
                        alertDialog.dismiss();
                    }
                    else
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent,"Select Image"),2);
                        alertDialog.dismiss();
                    }
                }
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1 && resultCode == RESULT_OK && data!= null)
        {
            bitmap = (Bitmap)data.getExtras().get("data");
            imagestore(bitmap);
            doc1.setImageBitmap(bitmap);
            doc1.setEnabled(false);

        }
        else if (requestCode==2 && resultCode == RESULT_OK && data!= null)
        {
            filepath = data.getData();

            try
            {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imagestore(bitmap);
                doc1.setImageBitmap(bitmap);
                doc1.setEnabled(false);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void imagestore(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        byte[] imageBytes = stream.toByteArray();

        parwana = android.util.Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }
}