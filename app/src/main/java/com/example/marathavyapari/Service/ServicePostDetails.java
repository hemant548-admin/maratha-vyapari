package com.example.marathavyapari.Service;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.EditPostActivity;
import com.example.marathavyapari.Activity.PostDetailsActivity;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ServicePostDetails extends AppCompatActivity
{
    TextView postTitle,postBusinessName,postBusinessName2,postDesc,postTime,postBusinessAddress;
    ImageView postImage;

    Toolbar toolbar;
    ImageView back;
    TextView toolbar_title;
    SharedPrefManager sharedPrefManager;
    String title,desc,time,image,businessname,address,post_id;
    ProgressDialog progressDialog;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);

        toolbar = findViewById(R.id.postdetails_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        back = findViewById(R.id.backbtn);
        toolbar_title = findViewById(R.id.custom_toolbat_title);
        sharedPrefManager = new SharedPrefManager(ServicePostDetails.this);
        token = sharedPrefManager.getToken("Token", ServicePostDetails.this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        postTitle = findViewById(R.id.post_details_post_title);
        postDesc = findViewById(R.id.post_desc);
        postTime = findViewById(R.id.post_time);
        postImage = findViewById(R.id.post_image);
        postBusinessName2 = findViewById(R.id.post_details_businessname2);
        postBusinessName = findViewById(R.id.post_details_businessname);
        postBusinessAddress = findViewById(R.id.post_details_address);
        progressDialog = new ProgressDialog(this);

        title = getIntent().getStringExtra("post_title");
        desc = getIntent().getStringExtra("post_desc");
        time = getIntent().getStringExtra("post_time");
        image = getIntent().getStringExtra("post_image");
        post_id = getIntent().getStringExtra("post_id");

       /* BusinessUserModel userModel = SharedPrefManager.getInstance(this).getUser();

        businessname = userModel.getBus_name();
        address = userModel.getBusiness_address();
        token = userModel.getToken();*/

        toolbar_title.setText(title);
        postTitle.setText(title);
        postDesc.setText(desc);
        postTime.setText(time);
        postBusinessAddress.setText(address);
        postBusinessName.setText(businessname);
        postBusinessName2.setText(businessname);

        String imagepath2  = Base_Url.imagepath + image; ;
        Glide.with(this)
                .load(imagepath2)
                .into(postImage);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_detail_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.edit_post:

//                Toast.makeText(this, "edit", Toast.LENGTH_SHORT).show();
                EditPost();

                break;

            case R.id.delete_post:

                AlertDialog.Builder builder1 = new AlertDialog.Builder(ServicePostDetails.this);
                builder1.setTitle("Delete Post");
                builder1.setMessage("Are you sure you want to delete this post ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.cancel();
                                DeletePost();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                break;
        }
        return true;
    }

    private void DeletePost()
    {
        progressDialog.setTitle("Deleting");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.POST_SERVICE_DELETE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServicePostDetails.this, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }
                    else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(ServicePostDetails.this, message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ServicePostDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("post_id", post_id);

                return params;

            }
        };

        RequestHandlerNew.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void EditPost()
    {
        Intent intent = new Intent(ServicePostDetails.this, EditServicePostActivity.class);
        intent.putExtra("post_image",image);
        intent.putExtra("post_title",title);
        intent.putExtra("post_desc",desc);
        intent.putExtra("post_id",post_id);

        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View,String>(postImage,"postimage");
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ServicePostDetails.this,pairs);
        startActivity(intent,options.toBundle());
        finish();

    }
}
