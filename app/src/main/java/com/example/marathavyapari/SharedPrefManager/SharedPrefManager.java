package com.example.marathavyapari.SharedPrefManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.marathavyapari.Activity.LoginActivity;
import com.example.marathavyapari.Activity.SelectLanguage;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.Model.UserProfileModel;
import com.example.marathavyapari.Other.RecyclerTouchListener;
import com.example.marathavyapari.User.SearchSubBusinessActivity;

import java.sql.Statement;

public class SharedPrefManager
{
    private static final String SHARED_PREF_NAME = "marathaappsharepref";

    private static final String KEY_TOKEN = "keytoken";
    private static final String KEY_ID = "keyid";
    private static final String KEY_CATID = "keycatid";
    private static final String KEY_SUBID = "keysubid";
    private static final String KEY_MOBILE = "keymobile";
    private static final String KEY_BUSINESS_NAME = "keybusinessname";
    private static final String KEY_NAME = "keyname";
    private static final String KEY_ADDRESS = "keyaddress";
    private static final String KEY_DESC = "keydesc";
    private static final String KEY_BUSINESS_TYPE = "keybusinesstype";
    private static final String KEY_DETAIL_INFO = "keydetailinfo";
    private static final String KEY_LICENCE = "keylicence";
    private static final String KEY_PHOTO = "keyphoto";
    private static final String KEY_STATUS = "keystatus";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_BC_NAME = "keybcname";
    private static final String KEY_LIVE_STATUS = "keylivestatus";
    private static final String KEY_BSC_NAME = "keybscname";
    private static final String KEY_SCNAME = "keyservicename";
    private static final String KEY_SSC_NAME = "keysscname";
    private static final String KEY_SERVINAME = "keyservicename";
    private static final String KEY_SERVICETYPE = "keyservicetype";
    private static final String KEY_USERID = "keyuserid";
    private static final String KEY_EMAILID = "keyemailid";
    private static final String KEY_DIST = "keydist";
    private static final String KEY_TAL = "keytal";


    private static SharedPrefManager mInstance;
    private static Context mCtx;

    public SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void userLogin(BusinessUserModel user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_TOKEN,user.getToken());
        editor.putString(KEY_ID,user.getBus_id());
        editor.putString(KEY_CATID,user.getCategory_id_fk());
        editor.putString(KEY_SUBID,user.getSubcategory_id_fk());
        editor.putString(KEY_MOBILE,user.getBus_mobile());
        editor.putString(KEY_BUSINESS_NAME,user.getBus_name());
        editor.putString(KEY_NAME,user.getOwner_name());
        editor.putString(KEY_ADDRESS,user.getBusiness_address());
        editor.putString(KEY_DESC,user.getBusiness_description());
        editor.putString(KEY_BUSINESS_TYPE,user.getBusiness_type());
        editor.putString(KEY_DETAIL_INFO,user.getDetail_info());
        editor.putString(KEY_LICENCE,user.getLicence());
        editor.putString(KEY_PHOTO,user.getPhoto());
        editor.putString(KEY_STATUS,user.getBusiness_status());
        editor.putString(KEY_EMAIL,user.getBusiness_email());
        editor.putString(KEY_BC_NAME,user.getBc_name());
        editor.putString(KEY_LIVE_STATUS,user.getLive_status());
        editor.putString(KEY_BSC_NAME,user.getBsc_name());

        editor.apply();
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if(sharedPreferences.getString(KEY_NAME, null) != null){
            return true;
        }
        return false;
    }

    public BusinessUserModel getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new BusinessUserModel(
                sharedPreferences.getString(KEY_TOKEN, null),
                sharedPreferences.getString(KEY_ID, null),
                sharedPreferences.getString(KEY_CATID, null),
                sharedPreferences.getString(KEY_SUBID, null),
                sharedPreferences.getString(KEY_MOBILE, null),
                sharedPreferences.getString(KEY_BUSINESS_NAME, null),
                sharedPreferences.getString(KEY_NAME, null),
                sharedPreferences.getString(KEY_ADDRESS, null),
                sharedPreferences.getString(KEY_DESC,null),
                sharedPreferences.getString(KEY_BUSINESS_TYPE,null),
                sharedPreferences.getString(KEY_DETAIL_INFO,null),
                sharedPreferences.getString(KEY_LICENCE, null),
                sharedPreferences.getString(KEY_PHOTO, null),
                sharedPreferences.getString(KEY_STATUS, null),
                sharedPreferences.getString(KEY_EMAIL, null),
                sharedPreferences.getString(KEY_BC_NAME, null),
                sharedPreferences.getString(KEY_LIVE_STATUS, null),
                sharedPreferences.getString(KEY_BSC_NAME, null)

        );
    }

    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, LoginActivity.class));
    }


    public String setToken(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getToken(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public void userserviceLogin(ServiceLoginModel serviceuser) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_TOKEN,serviceuser.getToken());
        editor.putString(KEY_ID,serviceuser.getSus_id());
        editor.putString(KEY_CATID,serviceuser.getCategory_id_fk());
        editor.putString(KEY_SUBID,serviceuser.getSubcategory_id_fk());
        editor.putString(KEY_MOBILE,serviceuser.getSus_mobile());
        editor.putString(KEY_SSC_NAME,serviceuser.getSus_name());
        editor.putString(KEY_NAME,serviceuser.getName());
        editor.putString(KEY_ADDRESS,serviceuser.getService_address());
        editor.putString(KEY_DESC,serviceuser.getService_description());
        editor.putString(KEY_SERVICETYPE,serviceuser.getService_type());
        editor.putString(KEY_STATUS, serviceuser.getService_status());
        editor.putString(KEY_LICENCE,serviceuser.getLicence());
        editor.putString(KEY_SCNAME,serviceuser.getSc_name());
        editor.putString(KEY_LIVE_STATUS,serviceuser.getLive_status());
        editor.putString(KEY_SSC_NAME,serviceuser.getSsc_name());

        editor.apply();
    }
    public ServiceLoginModel getServiceUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new ServiceLoginModel(
                sharedPreferences.getString(KEY_TOKEN, null),
                sharedPreferences.getString(KEY_ID, null),
                sharedPreferences.getString(KEY_CATID, null),
                sharedPreferences.getString(KEY_SUBID, null),
                sharedPreferences.getString(KEY_MOBILE, null),
                sharedPreferences.getString(KEY_SSC_NAME, null),
                sharedPreferences.getString(KEY_NAME, null),
                sharedPreferences.getString(KEY_ADDRESS, null),
                sharedPreferences.getString(KEY_DESC,null),
                sharedPreferences.getString(KEY_SERVICETYPE,null),
                sharedPreferences.getString(KEY_LICENCE, null),
                sharedPreferences.getString(KEY_STATUS, null),
                sharedPreferences.getString(KEY_EMAIL, null),
                sharedPreferences.getString(KEY_SCNAME, null),
                sharedPreferences.getString(KEY_LIVE_STATUS, null),
                sharedPreferences.getString(KEY_SSC_NAME, null)

        );
    }

    public void userProfilelogin(UserProfileModel userProfileModel) {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_TOKEN,userProfileModel.getToken());
        editor.putString(KEY_ID,userProfileModel.getUus_id());
        editor.putString(KEY_MOBILE, userProfileModel.getUus_mobile());
        editor.putString(KEY_EMAILID, userProfileModel.getUser_email());
        editor.putString(KEY_NAME, userProfileModel.getUser_name());
        editor.putString(KEY_PHOTO, userProfileModel.getUser_photo());
        editor.putString(KEY_DIST, userProfileModel.getDistrict_name());
        editor.putString(KEY_TAL, userProfileModel.getTaluka_name());
        editor.apply();
    }

    public UserProfileModel getuserLogin() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new UserProfileModel(
                sharedPreferences.getString(KEY_TOKEN, null),
                sharedPreferences.getString(KEY_ID, null),
                sharedPreferences.getString(KEY_MOBILE, null),
                sharedPreferences.getString(KEY_EMAILID, null),
                sharedPreferences.getString(KEY_NAME, null),
                sharedPreferences.getString(KEY_PHOTO, null),
                sharedPreferences.getString(KEY_DIST, null),
                sharedPreferences.getString(KEY_TAL, null)
        );
    }

    public String setMobile(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getMobile(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String setEmail(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getEmail(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String setUserName(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getUserName(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String setBusinessId(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getBusinessId(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String getServiceID(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getServiceID(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String setUserType(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getUserType(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }

    public String setLangauge(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getLanguage(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }
    public String setUserId(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getUserId(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }
    public boolean isLoggedIn2(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if(sharedPreferences.getString(KEY_NAME, null) == null){
            return true;
        }
        return false;
    }

    public String setUserPhoto(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
        return key;
    }
    public String getUserPhoto(String key,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key,"");
    }
}
