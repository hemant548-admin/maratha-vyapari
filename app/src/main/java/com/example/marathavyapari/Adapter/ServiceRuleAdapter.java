package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marathavyapari.Model.ShowServicePostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.ShowServiceShopDetails;

import java.util.ArrayList;

public class ServiceRuleAdapter extends RecyclerView.Adapter<ServiceRuleAdapter.ViewHolder> {

    Context context;
    ArrayList<ShowServicePostModel.RuleList> ruleLists;

    public ServiceRuleAdapter(Context context, ArrayList<ShowServicePostModel.RuleList> ruleLists) {

        this.context = context;
        this.ruleLists = ruleLists;
    }

    @NonNull
    @Override
    public ServiceRuleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_rulelist, null);
        return new ServiceRuleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceRuleAdapter.ViewHolder holder, int position) {

        holder.title.setText(ruleLists.get(position).getRule_title());
        holder.desc.setText(ruleLists.get(position).getRule_description());
    }

    @Override
    public int getItemCount() {
        return ruleLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, desc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.rule_title);
            desc = itemView.findViewById(R.id.rule_desc);
        }
    }
}
