package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.BusinessActivity;

import java.util.ArrayList;
import java.util.List;

public class MyBusinessListAdapter extends RecyclerView.Adapter<MyBusinessListAdapter.ViewHolder> {

    Context context;
    ArrayList<BusinessCategoryModel.CategoryList> businesslist;

    public MyBusinessListAdapter(Context context, ArrayList<BusinessCategoryModel.CategoryList> businesslist) {
        this.context = context;
        this.businesslist = businesslist;
    }

    @NonNull
    @Override
    public MyBusinessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businesslist, null);
        return new MyBusinessListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBusinessListAdapter.ViewHolder holder, int position) {


        holder.name.setText(businesslist.get(position).getBc_name());
        String imagepath = businesslist.get(position).getBc_image();
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        if (businesslist == null)
        {
            return 0;
        }else {
            return businesslist.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
