package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.Model.SubBusinessListModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.SubBusinessActivity;

import java.util.ArrayList;
import java.util.List;

public class MySubBusinessListAdapter extends RecyclerView.Adapter<MySubBusinessListAdapter.ViewHolder> {

    Context context;
    ArrayList<SubBusinessListModel.SubBusinessCategoryList> subbusinesslist;

    public MySubBusinessListAdapter(Context context, ArrayList<SubBusinessListModel.SubBusinessCategoryList> subbusinesslist) {

        this.context = context;
        this.subbusinesslist = subbusinesslist;
    }

    @NonNull
    @Override
    public MySubBusinessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businesslist, null);
        return new MySubBusinessListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubBusinessListAdapter.ViewHolder holder, int position) {

        holder.name.setText(subbusinesslist.get(position).getBsc_name());
        String imagepath = subbusinesslist.get(position).getBsc_image();
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        if (subbusinesslist == null)
        {
            return 0;
        }else {
            return subbusinesslist.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
