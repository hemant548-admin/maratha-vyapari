package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceRevioewModelData;
import com.example.marathavyapari.Model.ShowReViewServiceModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.ShowReviewActivity;

import java.util.ArrayList;
import java.util.List;

public class ShowServiceReviewAdapter extends RecyclerView.Adapter<ShowServiceReviewAdapter.ViewHolder> {

    Context context;
    ArrayList<ServiceRevioewModelData.ServiceList> showReviewServiceList;


    public ShowServiceReviewAdapter(Context context, ArrayList<ServiceRevioewModelData.ServiceList> showReviewServiceList) {
        this.context = context;
        this.showReviewServiceList = showReviewServiceList;
    }

    @NonNull
    @Override
    public ShowServiceReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businessreview, null);
        return new ShowServiceReviewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowServiceReviewAdapter.ViewHolder holder, int position) {

        String imagepath = showReviewServiceList.get(position).getUser_photo();
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.desc.setText(showReviewServiceList.get(position).getReview_message());
        holder.ratingBar.setRating(Float.parseFloat(showReviewServiceList.get(position).getAvrage_rating()));
        holder.name.setText(showReviewServiceList.get(position).getUser_name());
        holder.mobile.setText(showReviewServiceList.get(position).getUus_mobile());
        holder.time.setText(showReviewServiceList.get(position).getReview_time());
    }

    @Override
    public int getItemCount() {

        if (showReviewServiceList == null)
        {
            return 0;
        }else {
            return showReviewServiceList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView desc,time, name, mobile;
        public RatingBar ratingBar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            desc = itemView.findViewById(R.id.rule_title);
            ratingBar = itemView.findViewById(R.id.show_rating);
            time = itemView.findViewById(R.id.textdate);
            name = itemView.findViewById(R.id.text_username);
            mobile = itemView.findViewById(R.id.text_number);
        }
    }
}
