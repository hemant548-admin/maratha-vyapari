package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.ServiceActivity;
import com.example.marathavyapari.User.SubServiceActivtiy;
import com.example.marathavyapari.User.UserMainActivity;

import java.util.ArrayList;
import java.util.List;

public class ServiceListAdapter extends RecyclerView.Adapter <ServiceListAdapter.ViewHolder>{

    Context context;
    ArrayList<ServiceCategoryModel.CategoryList> serviceList;

    public ServiceListAdapter(Context context, ArrayList<ServiceCategoryModel.CategoryList> serviceList) {
        this.context = context;;
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public ServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businessservicelist, null);
        return new ServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceListAdapter.ViewHolder holder, int position) {
        if (position == getItemCount() - 1)
        {
            holder.image.setImageResource(R.drawable.ic_morebutton);
            holder.name.setText(R.string.more);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(context, ServiceActivity.class);
                    context.startActivity(intent);
                }
            });
        }
        else {

            holder.name.setText(serviceList.get(position).getSc_name());
            String imagepath = serviceList.get(position).getSc_image();
            String imagepath2 = Base_Url.imagepath + imagepath;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    String category_id =serviceList.get(position).getSc_id();

                    Intent catlist = new Intent(context, SubServiceActivtiy.class);
                    catlist.putExtra("category_id",category_id);
                    context.startActivity(catlist);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        if (serviceList == null)
        {
            return 0;
        }else {
            return 8;
        }
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
