package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.SearchResultModel;
import com.example.marathavyapari.Model.ServiceSearchModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.SearchSreenActivity;
import com.example.marathavyapari.User.ShowServiceShopDetails;

import java.util.ArrayList;
import java.util.List;

public class SearchServiceAdapter extends RecyclerView.Adapter<SearchServiceAdapter.ViewHolder> {

    Context context;
    ArrayList<SearchResultModel.ServiceList> serviceLists;
  //  List<ServiceSearchModel> serviceLists;
    String serviceid;
    SharedPrefManager sharedPrefManager;

    public SearchServiceAdapter(Context context, ArrayList<SearchResultModel.ServiceList> serviceLists) {

        this.context = context;
        this.serviceLists = serviceLists;
        sharedPrefManager = new SharedPrefManager(context);
    }

    @NonNull
    @Override
    public SearchServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_subserachlist, null);
        return new SearchServiceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchServiceAdapter.ViewHolder holder, int position) {
        String photo = serviceLists.get(position).getLicence();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.title.setText(serviceLists.get(position).getSus_name());
        holder.address.setText(serviceLists.get(position).getService_description());
        holder.ratingBar.setRating(Float.parseFloat(serviceLists.get(position).getAvrage_rating()));
        holder.countrating.setText(serviceLists.get(position).getNo_of_rating());
        holder.lay_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        serviceid = serviceLists.get(position).getSus_id();
                       sharedPrefManager.getServiceID("ServiceId", serviceid, context);
                       Intent catlist = new Intent(context, ShowServiceShopDetails.class);
                       context.startActivity(catlist);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (serviceLists == null) {
            return 0;
        }
        else
        {
            return serviceLists.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,address, countrating;
        RatingBar ratingBar;
        ImageView image;
        LinearLayout lay_details;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            ratingBar = itemView.findViewById(R.id.show_rating);
            countrating = itemView.findViewById(R.id.ratingcount);
            lay_details = itemView.findViewById(R.id.lay_details);
        }
    }
}
