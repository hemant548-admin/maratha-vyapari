package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServicePostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.UserMainActivity;

import java.util.ArrayList;

public class ServiceRelatedPostAdapter extends RecyclerView.Adapter<ServiceRelatedPostAdapter.ViewHolder> {

    Context context;
    ArrayList<ServicePostModel.PostList> serviceonepost;
    public ServiceRelatedPostAdapter(Context context, ArrayList<ServicePostModel.PostList> serviceonepost) {
        this.context = context;
        this.serviceonepost = serviceonepost;
    }

    @NonNull
    @Override
    public ServiceRelatedPostAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_servicesbusinespost, null);
        return new ServiceRelatedPostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceRelatedPostAdapter.ViewHolder holder, int position) {

        String imagepath = serviceonepost.get(position).getPost_image();
        //holder.discount.setText(serviceonepost.get(position).getD);
        String imagepath2  = Base_Url.imagepath +imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.imageView);
        holder.title.setText(serviceonepost.get(position).getPost_title());
        holder.description.setText(serviceonepost.get(position).getPost_description());
        holder.time.setText(serviceonepost.get(position).getPost_time());
    }

    @Override
    public int getItemCount() {
            return serviceonepost.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView discount, title, description, time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.post_image);
            title = itemView.findViewById(R.id.post_title);
            description = itemView.findViewById(R.id.post_desc);
            time = itemView.findViewById(R.id.post_time);
        }
    }
}
