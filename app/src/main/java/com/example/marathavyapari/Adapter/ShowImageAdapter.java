package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowBusineePostModel;
import com.example.marathavyapari.Model.ShowServicePostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.ShowImageActivity;
import com.example.marathavyapari.User.ShowServiceShopDetails;

import java.util.ArrayList;

public class ShowImageAdapter extends RecyclerView.Adapter<ShowImageAdapter.ViewHolder> {

    Context context;
    ArrayList<ShowBusineePostModel.PhotoList> imagelist;

    public ShowImageAdapter(Context context, ArrayList<ShowBusineePostModel.PhotoList> imagelist) {
        this.context = context;
        this.imagelist = imagelist;
    }


    @NonNull
    @Override
    public ShowImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_imagelist, null);
        return new ShowImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowImageAdapter.ViewHolder holder, int position) {
        String imagepath = imagelist.get(position).getImage_file();

        //loading the image
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, ShowImageActivity.class);
                intent.putExtra("imagepath",imagepath2);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (imagelist == null)
        {
            return 0;
        }else {
            return imagelist.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
