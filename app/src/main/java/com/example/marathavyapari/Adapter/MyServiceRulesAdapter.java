package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.RulesModelBusiness;
import com.example.marathavyapari.Model.ServiceDataModel;
import com.example.marathavyapari.Model.ServiceLoginModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.Service.ServieTermsConditions;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import java.util.List;

public class MyServiceRulesAdapter extends RecyclerView.Adapter<MyServiceRulesAdapter.ProductViewHolder>
{
    private Context mCtx;
    private List<ServiceDataModel> productList;

    public MyServiceRulesAdapter(Context context, List<ServiceDataModel> rulesModelBusinessList) {

        this.mCtx = context;
        this.productList = rulesModelBusinessList;
    }


    @NonNull
    @Override
    public MyServiceRulesAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.my_rules_list, null);
//        LayoutInflater inflater;
//        inflater.from(parent.getContext()).inflate(R.layout.subject_list,parent,false);
        return new MyServiceRulesAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        ServiceDataModel product = productList.get(position);

        ServiceLoginModel userModel = SharedPrefManager.getInstance(mCtx).getServiceUser();
        String photo = userModel.getLicence();

//        loading the image
        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(mCtx)
                .load(imagepath2)
                .into(holder.image);

        holder.title.setText(product.getRule_title());
        holder.businessname.setText(product.getRule_description());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView title,businessname;
        ImageView image;

        public ProductViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.rule_title);
            businessname= itemView.findViewById(R.id.rule_desc);
            image = itemView.findViewById(R.id.rule_image);

        }
    }
}
