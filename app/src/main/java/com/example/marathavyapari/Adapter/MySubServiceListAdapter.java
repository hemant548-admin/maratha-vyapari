package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ServiceSubCatModel;
import com.example.marathavyapari.R;

import java.util.ArrayList;
import java.util.List;

public class MySubServiceListAdapter extends RecyclerView.Adapter<MySubServiceListAdapter.ViewHolder> {

    Context context;
    ArrayList<ServiceSubCatModel.SubCategoryList> subservicelist;

    public MySubServiceListAdapter(Context context, ArrayList<ServiceSubCatModel.SubCategoryList> subservicelist) {
        this.context = context;
        this.subservicelist = subservicelist;
    }

    @NonNull
    @Override
    public MySubServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businesslist, null);
        return new MySubServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubServiceListAdapter.ViewHolder holder, int position) {

        holder.name.setText(subservicelist.get(position).getSsc_name());
        String imagepath = subservicelist.get(position).getSsc_image();
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        if (subservicelist == null)
        {
            return 0;
        }else {
            return subservicelist.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
