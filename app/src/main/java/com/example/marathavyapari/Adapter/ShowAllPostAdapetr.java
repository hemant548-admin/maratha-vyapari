package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessPostModel;
import com.example.marathavyapari.Model.BusinessServiceModel;
import com.example.marathavyapari.Model.ShowBusiServiceModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.AllServiceBusinessActivity;

import java.util.ArrayList;

public class ShowAllPostAdapetr extends RecyclerView.Adapter<ShowAllPostAdapetr.ViewHolder> {

    Context context;
    ArrayList<BusinessServiceModel.PostList> businessonepost;
    String type;


    public ShowAllPostAdapetr(AllServiceBusinessActivity context, ArrayList<BusinessServiceModel.PostList> businessonepost) {
        this.context = context;
        this.businessonepost = businessonepost;
    }

    @NonNull
    @Override
    public ShowAllPostAdapetr.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_servicesbusinespost, null);
        return new ShowAllPostAdapetr.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowAllPostAdapetr.ViewHolder holder, int position) {

        type = businessonepost.get(position).getType();
        if (type.equals("business")) {
            holder.discount.setText(businessonepost.get(position).getPost_title());
            holder.title.setText(businessonepost.get(position).getBus_name());
            holder.description.setText(businessonepost.get(position).getBusiness_description());
            holder.time.setText(businessonepost.get(position).getPost_time());
            String imagepath = businessonepost.get(position).getPost_image();
            //holder.discount.setText(serviceonepost.get(position).getD);
            String imagepath2 = Base_Url.imagepath + imagepath;
            ;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.imageView);
        }else if (type.equals("service"))
        {
            holder.discount.setText(businessonepost.get(position).getPost_title());
            holder.title.setText(businessonepost.get(position).getSus_name());
            holder.description.setText(businessonepost.get(position).getService_description());
            holder.time.setText(businessonepost.get(position).getPost_time());
            String imagepath = businessonepost.get(position).getPost_image();
            //holder.discount.setText(serviceonepost.get(position).getD);
            String imagepath2 = Base_Url.imagepath + imagepath;
            ;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        if (businessonepost == null) {
            return 0;
        }else
        {
            return businessonepost.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView discount, title, description, time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            discount = itemView.findViewById(R.id.post_sale);
            imageView = itemView.findViewById(R.id.post_image);
            title = itemView.findViewById(R.id.post_title);
            description = itemView.findViewById(R.id.post_desc);
            time = itemView.findViewById(R.id.post_time);
        }
    }
}
