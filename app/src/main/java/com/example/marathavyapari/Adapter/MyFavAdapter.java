package com.example.marathavyapari.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowFavouriteModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.ShowServiceShopDetails;
import com.example.marathavyapari.User.UserFavouriteActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyFavAdapter extends RecyclerView.Adapter<MyFavAdapter.ViewHolder> {

    Context context;
    List<ShowFavouriteModel> showFavouriteList;
    String type, token, Number;
    String businessid, serviceid;
    SharedPrefManager sharedPrefManager;
    ProgressDialog progressDialog;

    public MyFavAdapter(Context context, List<ShowFavouriteModel> showFavouriteList) {
        this.context = context;
        this.showFavouriteList = showFavouriteList;
        sharedPrefManager = new SharedPrefManager(context);
        token = sharedPrefManager.getToken("Token", context);
        progressDialog = new ProgressDialog(context);
    }

    @NonNull
    @Override
    public MyFavAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_userfavourite, null);
        return new MyFavAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyFavAdapter.ViewHolder holder, int position) {

        type = showFavouriteList.get(position).getType();
        String imagepath = showFavouriteList.get(position).getPhoto();
        String imagepath1 = showFavouriteList.get(position).getLicence();
        businessid = showFavouriteList.get(position).getBus_id();
        serviceid= showFavouriteList.get(position).getSus_id();
        if (type.equals("business"))
        {
            holder.title.setText(showFavouriteList.get(position).getBus_name());
            holder.address.setText(showFavouriteList.get(position).getBusiness_address());
            String imagepath2  = Base_Url.imagepath + imagepath;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.image);

            holder.ratingBar.setRating(Float.parseFloat(showFavouriteList.get(position).getAvrage_rating()));
            holder.norating.setText(showFavouriteList.get(position).getNo_of_rating());
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    businessid = showFavouriteList.get(position).getBus_id();
                    sharedPrefManager.setBusinessId("BusinessId", businessid, context);
                    Intent catlist = new Intent(context, Activity_ShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    businessid = showFavouriteList.get(position).getBus_id();
                    sharedPrefManager.setBusinessId("BusinessId", businessid, context);
                    Intent catlist = new Intent(context, Activity_ShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.lay_trans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Number = showFavouriteList.get(position).getBus_mobile();
                    String contact = "91" +Number ; // use country code with your phone number
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    context.startActivity(intent);
                }
            });
           holder.cancel.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   showFavouriteList.remove(position);
                   notifyItemRemoved(position);
                   notifyItemRangeChanged(position, showFavouriteList.size());
                   getRemoveFav(businessid);
               }
           });
        }else if (type.equals("service"))
        {
            holder.title.setText(showFavouriteList.get(position).getSus_name());
            holder.address.setText(showFavouriteList.get(position).getService_address());
            String imagepath3 = Base_Url.imagepath + imagepath1;
            Glide.with(context)
                    .load(imagepath3)
                    .into(holder.image);

           holder.ratingBar.setRating(Float.parseFloat(showFavouriteList.get(position).getAvrage_rating()));
           holder.norating.setText(showFavouriteList.get(position).getNo_of_rating());
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    serviceid = showFavouriteList.get(position).getSus_id();
                    sharedPrefManager.getServiceID("ServiceId", serviceid, context);
                    Intent catlist = new Intent(context, ShowServiceShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    serviceid = showFavouriteList.get(position).getSus_id();
                    sharedPrefManager.getServiceID("ServiceId", serviceid, context);
                    Intent catlist = new Intent(context, ShowServiceShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.lay_trans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Number = showFavouriteList.get(position).getSus_mobile();
                    String contact = "91" +Number ; // use country code with your phone number
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    context.startActivity(intent);
                }
            });
            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getRemoveServiceFav(serviceid);
                }
            });
        }

    }

    private void getRemoveServiceFav(String serviceid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UNFAV_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);



                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("service_id", serviceid);
                return params;

            }
        };

        RequestHandlerNew.getInstance(context).addToRequestQueue(stringRequest);
    }

    private void getRemoveFav(String businessid) {
        progressDialog.setTitle("Updating");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Base_Url.UNFAV_BUSINESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
//                        Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);

                    }else
                    {
                        progressDialog.dismiss();
                        String message = jsonObject.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: MADAN"+message);
                    }



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                return params;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("business_id", businessid);
                return params;

            }
        };

        RequestHandlerNew.getInstance(context).addToRequestQueue(stringRequest);
    }

    @Override
    public int getItemCount() {

        if (showFavouriteList == null) {
            return 0;
        }else
        {
            return showFavouriteList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image, cancel;
        TextView title, address, norating;
        RatingBar ratingBar;
        LinearLayout lay_trans;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.rule_image);
            cancel = itemView.findViewById(R.id.btn_cancel);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            norating =itemView.findViewById(R.id.ratingcount);
            ratingBar = itemView.findViewById(R.id.show_rating);
            lay_trans = itemView.findViewById(R.id.lay_what);
        }
    }
}
