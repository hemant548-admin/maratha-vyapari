package com.example.marathavyapari.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.API.ApiClient;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.SubBusinessDataModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.SearchSubBusinessActivity;
import com.example.marathavyapari.User.SearchSubServiceActivity;
import com.example.marathavyapari.User.UserChangePassword;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchSubBusinessListAdapter extends RecyclerView.Adapter<SearchSubBusinessListAdapter.ViewHolder>{

    Context context;
    ArrayList<SubBusinessDataModel.BusinessListModel> subbusinesslistdata;


    public SearchSubBusinessListAdapter(Context context, ArrayList<SubBusinessDataModel.BusinessListModel> subbusinesslistdata) {
        this.context = context;
        this.subbusinesslistdata = subbusinesslistdata;

    }

    @NonNull
    @Override
    public SearchSubBusinessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_subserachlist, null);
        return new SearchSubBusinessListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchSubBusinessListAdapter.ViewHolder holder, int position) {
        String photo = subbusinesslistdata.get(position).getPhoto();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.title.setText(subbusinesslistdata.get(position).getBus_name());
        holder.address.setText(subbusinesslistdata.get(position).getBusiness_address());
        holder.ratingBar.setRating(Float.parseFloat(subbusinesslistdata.get(position).getAvrage_rating()));
        /*LayerDrawable stars=(LayerDrawable)holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FFBE0F"), PorterDuff.Mode.DARKEN);*/
        holder.countrating.setText(subbusinesslistdata.get(position).getNo_of_rating());



    }

    @Override
    public int getItemCount() {
        if (subbusinesslistdata == null)
        {
            return 0;
        }else {
            return subbusinesslistdata.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title,address, countrating;
        RatingBar ratingBar;
        ImageView image;
        LinearLayout lay_mytrans;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            ratingBar = itemView.findViewById(R.id.show_rating);
            countrating = itemView.findViewById(R.id.ratingcount);
            lay_mytrans = itemView.findViewById(R.id.lay_mytrans);
        }
    }
}
