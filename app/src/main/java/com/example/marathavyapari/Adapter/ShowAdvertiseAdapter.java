package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessAdvertiseModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.SearchSubBusinessActivity;

import java.util.ArrayList;

public class ShowAdvertiseAdapter extends RecyclerView.Adapter<ShowAdvertiseAdapter.ViewHolder> {

    Context context;
    ArrayList<BusinessAdvertiseModel.AdvertiseList> advertiseLists;

    public ShowAdvertiseAdapter(Context context, ArrayList<BusinessAdvertiseModel.AdvertiseList> advertiseLists) {
        this.context = context;
        this.advertiseLists= advertiseLists;
    }

    @NonNull
    @Override
    public ShowAdvertiseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_advertisememnt, null);
        return new ShowAdvertiseAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowAdvertiseAdapter.ViewHolder holder, int position) {
        String photo = advertiseLists.get(position).getAdv_image();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.title.setText(advertiseLists.get(position).getAdv_title());
        holder.desc.setText(advertiseLists.get(position).getAdv_description());
    }

    @Override
    public int getItemCount() {
        if (advertiseLists == null) {
            return 0;
        }else
        {
            return advertiseLists.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title, desc;
        public ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.adv_title);
            desc = itemView.findViewById(R.id.adv_desc);
            image = itemView.findViewById(R.id.noti_image);
        }
    }
}
