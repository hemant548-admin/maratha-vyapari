package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Activity.MyReviews;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessReviewModel;
import com.example.marathavyapari.R;

import java.util.List;

public class BusinessReviewAdapter extends RecyclerView.Adapter<BusinessReviewAdapter.ViewHolder>{

    Context context;
    List<BusinessReviewModel> businessReviewList;

    public BusinessReviewAdapter(Context context, List<BusinessReviewModel> businessReviewList) {

        this.context = context;
        this.businessReviewList = businessReviewList;
    }

    @NonNull
    @Override
    public BusinessReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businessreview, null);
        return new BusinessReviewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessReviewAdapter.ViewHolder holder, int position) {

        String imagepath = businessReviewList.get(position).getUser_photo();

        //loading the image
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.desc.setText(businessReviewList.get(position).getReview_message());
        holder.ratingBar.setRating(Float.parseFloat(businessReviewList.get(position).getAvrage_rating()));
        holder.name.setText(businessReviewList.get(position).getUser_name());
        holder.mobile.setText(businessReviewList.get(position).getUus_mobile());
        holder.time.setText(businessReviewList.get(position).getReview_time());

    }

    @Override
    public int getItemCount() {

        if (businessReviewList == null)
        {
            return  0;
        }else
        {
            return businessReviewList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView desc,time, name, mobile;
        public RatingBar ratingBar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            desc = itemView.findViewById(R.id.rule_title);
            ratingBar = itemView.findViewById(R.id.show_rating);
            time = itemView.findViewById(R.id.textdate);
            name = itemView.findViewById(R.id.text_username);
            mobile = itemView.findViewById(R.id.text_number);
        }
    }
}
