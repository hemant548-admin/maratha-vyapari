package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marathavyapari.Model.UserNotificationModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.Activity_UserNotification;

import java.util.ArrayList;
import java.util.List;

public class ShowNotifyAdapter extends RecyclerView.Adapter<ShowNotifyAdapter.ViewHolder> {

    Context context;
    ArrayList<UserNotificationModel.NotificationList> notificationList;

    public ShowNotifyAdapter(Context context, ArrayList<UserNotificationModel.NotificationList> notificationList) {
        this.context = context;
        this.notificationList = notificationList;
    }

    @NonNull
    @Override
    public ShowNotifyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_user_notification, null);
        return new ShowNotifyAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowNotifyAdapter.ViewHolder holder, int position) {

       holder.title.setText(notificationList.get(position).getNotify_title());
       holder.desc.setText(notificationList.get(position).getNotify_message());
       holder.time.setText(notificationList.get(position).getNotify_time());
       holder.date.setText(notificationList.get(position).getNotify_date());
    }

    @Override
    public int getItemCount() {
        if (notificationList == null)
        {
            return 0;
        }else {
            return notificationList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        TextView title, desc, date, time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.noti_title);
            desc = itemView.findViewById(R.id.noti_desc);
            date = itemView.findViewById(R.id.textdate);
            time = itemView.findViewById(R.id.post_time);
            imageView = itemView.findViewById(R.id.noti_image);
        }
    }
}
