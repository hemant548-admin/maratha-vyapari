package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessCategoryModel;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.BusinessActivity;
import com.example.marathavyapari.User.SubBusinessActivity;
import com.example.marathavyapari.User.UserMainActivity;

import java.util.ArrayList;
import java.util.List;

public class BusinessListAdapter extends RecyclerView.Adapter<BusinessListAdapter.ViewHolder> {

    Context context;
    ArrayList<BusinessCategoryModel.CategoryList> businesslist;

    public BusinessListAdapter(UserMainActivity context, ArrayList<BusinessCategoryModel.CategoryList> businesslist) {
        this.context = context;
        this.businesslist = businesslist;
    }

    @NonNull
    @Override
    public BusinessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businessservicelist, null);
        return new BusinessListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessListAdapter.ViewHolder holder, int position) {

        if (position == getItemCount() - 1)
        {
            holder.image.setImageResource(R.drawable.ic_morebutton);
            holder.name.setText(R.string.more);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(context, BusinessActivity.class);
                    context.startActivity(intent);
                }
            });
        }
        else {
            holder.name.setText(businesslist.get(position).getBc_name());
            String imagepath = businesslist.get(position).getBc_image();
            String imagepath2 = Base_Url.imagepath + imagepath;
            ;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String category_id = businesslist.get(position).getBc_id();

                    Intent catlist = new Intent(context, SubBusinessActivity.class);
                    catlist.putExtra("category_id", category_id);
                    context.startActivity(catlist);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (businesslist == null)
        {
            return 0;
        }else{
            return 8;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
