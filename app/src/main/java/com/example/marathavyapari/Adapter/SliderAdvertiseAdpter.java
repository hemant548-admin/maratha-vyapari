package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessAdvertiseModel;
import com.example.marathavyapari.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class SliderAdvertiseAdpter extends SliderViewAdapter<SliderAdvertiseAdpter.ViewHolder> {

    Context context;
    ArrayList<BusinessAdvertiseModel.AdvertiseList> advertiseLists;

    public SliderAdvertiseAdpter(Context context, ArrayList<BusinessAdvertiseModel.AdvertiseList> advertiseLists) {
        this.context = context;
        this.advertiseLists = advertiseLists;
    }


    @Override
    public SliderAdvertiseAdpter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_advertise, null);
        return new SliderAdvertiseAdpter.ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdvertiseAdpter.ViewHolder viewHolder, int position) {
        String photo = advertiseLists.get(position).getAdv_image();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(viewHolder.image);
        viewHolder.title.setText(advertiseLists.get(position).getAdv_title());
        viewHolder.desc.setText(advertiseLists.get(position).getAdv_description());
    }

    @Override
    public int getCount() {
        if (advertiseLists == null) {
            return 0;
        }
        else {
          return advertiseLists.size();
        }
    }

    public class ViewHolder extends SliderViewAdapter.ViewHolder {

        public TextView title, desc;
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.adv_title);
            desc = itemView.findViewById(R.id.adv_desc);
            image = itemView.findViewById(R.id.noti_image);
        }
    }
}
