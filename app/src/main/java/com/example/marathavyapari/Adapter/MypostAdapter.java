package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessUserModel;
import com.example.marathavyapari.Model.MyPostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;

import java.util.List;

public class MypostAdapter extends RecyclerView.Adapter<MypostAdapter.ProductViewHolder>
{
    private Context mCtx;
    private List<MyPostModel> productList;

    public MypostAdapter(Context mCtx, List<MyPostModel> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @NonNull
    @Override
    public MypostAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.my_post_list, null);
//        LayoutInflater inflater;
//        inflater.from(parent.getContext()).inflate(R.layout.subject_list,parent,false);
        return new MypostAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MypostAdapter.ProductViewHolder holder, int position) {
        MyPostModel product = productList.get(position);

        String imagepath = product.getPost_image();

        //loading the image
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(mCtx)
                .load(imagepath2)
                .into(holder.image);

        BusinessUserModel userModel = SharedPrefManager.getInstance(mCtx).getUser();

        holder.title.setText(product.getPost_title());
        holder.businessname.setText(userModel.getBus_name());
        holder.desc.setText(product.getPost_description());
        holder.time.setText(product.getPost_time());
        holder.date.setText(product.getPost_date());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView title,businessname,desc,time, date;
        ImageView image;

        public ProductViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.post_title);
            businessname= itemView.findViewById(R.id.post_businessname);
            desc = itemView.findViewById(R.id.post_desc);
            time = itemView.findViewById(R.id.post_time);
            image = itemView.findViewById(R.id.post_image);
            date = itemView.findViewById(R.id.textdate);

        }
    }
}
