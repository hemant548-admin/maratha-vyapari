package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowTransactionModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.MyTransactionActivity;
import com.example.marathavyapari.User.ShowServiceShopDetails;

import java.util.List;

public class MyTransAdapter extends RecyclerView.Adapter<MyTransAdapter.ViewHolder> {

    Context context;
    List<ShowTransactionModel> showTransactionList;
    String type, serviceid, businessId, Number;
    SharedPrefManager sharedPrefManager;

    public MyTransAdapter(Context context, List<ShowTransactionModel> showTransactionList) {

        this.context = context;
        this.showTransactionList = showTransactionList;
        sharedPrefManager = new SharedPrefManager(context);
    }

    @NonNull
    @Override
    public MyTransAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_subserachlist, null);
        return new MyTransAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyTransAdapter.ViewHolder holder, int position) {
        type = showTransactionList.get(position).getType();
        String imagepath = showTransactionList.get(position).getPhoto();
        String imagepath1 = showTransactionList.get(position).getLicence();
        if (type.equals("business"))
        {
            holder.title.setText(showTransactionList.get(position).getBus_name());
            holder.address.setText(showTransactionList.get(position).getBusiness_address());
            String imagepath2  = Base_Url.imagepath + imagepath;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.image);
             holder.ratingBar.setRating(Float.parseFloat(showTransactionList.get(position).getAvrage_rating()));
              holder.countrating.setText(showTransactionList.get(position).getNo_of_rating());
            holder.lay_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    businessId = showTransactionList.get(position).getBus_id();
                    sharedPrefManager.setBusinessId("BusinessId", businessId, context);
                    Intent catlist = new Intent(context, Activity_ShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.lay_mytrans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Number = showTransactionList.get(position).getBus_mobile();
                    String contact = "91" +Number ; // use country code with your phone number
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    context.startActivity(intent);
                }
            });
        }else if (type.equals("service"))
        {
            holder.title.setText(showTransactionList.get(position).getSus_name());
            holder.address.setText(showTransactionList.get(position).getService_address());
            String imagepath2  = Base_Url.imagepath + imagepath1;
            Glide.with(context)
                    .load(imagepath2)
                    .into(holder.image);

             holder.ratingBar.setRating(Float.parseFloat(showTransactionList.get(position).getAvrage_rating()));
             holder.countrating.setText(showTransactionList.get(position).getNo_of_rating());
            holder.lay_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    serviceid = showTransactionList.get(position).getSus_id();
                    sharedPrefManager.getServiceID("ServiceId", serviceid, context);
                    Intent catlist = new Intent(context, ShowServiceShopDetails.class);
                    context.startActivity(catlist);
                }
            });
            holder.lay_mytrans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Number = showTransactionList.get(position).getSus_mobile();
                    String contact = "91" +Number ; // use country code with your phone number
                    String url = "https://api.whatsapp.com/send?phone=" + contact;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    context.startActivity(intent);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        if (showTransactionList == null) {
            return 0;
        }else
        {
            return showTransactionList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView title, address, countrating;
        public RatingBar ratingBar;
        public LinearLayout lay_details, lay_mytrans;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            countrating = itemView.findViewById(R.id.ratingcount);
            ratingBar = itemView.findViewById(R.id.show_rating);
            lay_details = itemView.findViewById(R.id.lay_details);
            lay_mytrans = itemView.findViewById(R.id.lay_mytrans);
        }
    }
}
