package com.example.marathavyapari.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.SubServiceDataModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.RequestHandler.RequestHandlerNew;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.SearchSubServiceActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchSubServiceListAdapter extends RecyclerView.Adapter<SearchSubServiceListAdapter.ViewHolder> {

    Context context;
    ArrayList<SubServiceDataModel.ServiceListData> subservicelistdata;


    public SearchSubServiceListAdapter(Context context, ArrayList<SubServiceDataModel.ServiceListData> subservicelistdata) {
        this.context = context;
        this.subservicelistdata = subservicelistdata;
    }

    @NonNull
    @Override
    public SearchSubServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_subserachlist, null);
        return new SearchSubServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchSubServiceListAdapter.ViewHolder holder, int position) {
        String photo = subservicelistdata.get(position).getLicence();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.title.setText(subservicelistdata.get(position).getSus_name());
        holder.address.setText(subservicelistdata.get(position).getService_description());
        holder.ratingBar.setRating(Float.parseFloat(subservicelistdata.get(position).getAvrage_rating()));
        holder.countrating.setText(subservicelistdata.get(position).getNo_of_rating());
       /* LayerDrawable stars=(LayerDrawable)holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FFBE0F"), PorterDuff.Mode.DST);*/

    }

    @Override
    public int getItemCount() {
        if (subservicelistdata == null)
        {
           return 0;
        }else {
            return subservicelistdata.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,address, countrating;
        RatingBar ratingBar;
        ImageView image;
        LinearLayout lay_mytrans;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            ratingBar = itemView.findViewById(R.id.show_rating);
            countrating = itemView.findViewById(R.id.ratingcount);
            lay_mytrans = itemView.findViewById(R.id.lay_mytrans);
        }
    }
}
