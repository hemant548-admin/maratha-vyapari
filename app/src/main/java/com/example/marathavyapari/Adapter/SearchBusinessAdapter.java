package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessSearchModel;
import com.example.marathavyapari.Model.SearchResultModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.SharedPrefManager.SharedPrefManager;
import com.example.marathavyapari.User.Activity_ShopDetails;
import com.example.marathavyapari.User.SearchSreenActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchBusinessAdapter extends RecyclerView.Adapter<SearchBusinessAdapter.ViewHolder> {

    Context context;
    ArrayList<SearchResultModel.BusinessList> busineslist;
   // List<BusinessSearchModel> busineslist;
    String businessId;
    SharedPrefManager sharedPrefManager;

    public SearchBusinessAdapter(Context context, ArrayList<SearchResultModel.BusinessList> busineslist) {
        this.context = context;
        this.busineslist = busineslist;
    }

    @NonNull
    @Override
    public SearchBusinessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_subserachlist, null);
        return new SearchBusinessAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchBusinessAdapter.ViewHolder holder, int position) {
        String photo = busineslist.get(position).getPhoto();

        String imagepath2  = Base_Url.imagepath + photo; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
        holder.title.setText(busineslist.get(position).getBus_name());
        holder.address.setText(busineslist.get(position).getBusiness_address());
        holder.ratingBar.setRating(Float.parseFloat(busineslist.get(position).getAvrage_rating()));
        holder.countrating.setText(busineslist.get(position).getNo_of_rating());
        holder.lay_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        businessId = busineslist.get(position).getBus_id();
                        sharedPrefManager.setBusinessId("BusinessId", businessId, context);
                        Intent catlist = new Intent(context, Activity_ShopDetails.class);
                        context.startActivity(catlist);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (busineslist == null)
        {
            return 0;
        }else {
            return busineslist.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,address, countrating;
        RatingBar ratingBar;
        ImageView image;
        LinearLayout lay_details;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.rule_image);
            title = itemView.findViewById(R.id.text_title);
            address = itemView.findViewById(R.id.text_address);
            ratingBar = itemView.findViewById(R.id.show_rating);
            countrating = itemView.findViewById(R.id.ratingcount);
            lay_details = itemView.findViewById(R.id.lay_details);
        }
    }
}
