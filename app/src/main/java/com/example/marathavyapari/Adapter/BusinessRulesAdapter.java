package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marathavyapari.Model.ShowBusineePostModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.Activity_ShopDetails;

import java.util.ArrayList;

public class BusinessRulesAdapter extends RecyclerView.Adapter<BusinessRulesAdapter.ViewHolder> {

    Context context;
    ArrayList<ShowBusineePostModel.RuleList> rulelists;

    public BusinessRulesAdapter(Context context, ArrayList<ShowBusineePostModel.RuleList> rulelists) {
        this.context = context;
        this.rulelists = rulelists;
    }

    @NonNull
    @Override
    public BusinessRulesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_rulelist, null);
        return new BusinessRulesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessRulesAdapter.ViewHolder holder, int position) {

        holder.title.setText(rulelists.get(position).getRule_title());
        holder.desc.setText(rulelists.get(position).getRule_description());
    }

    @Override
    public int getItemCount() {
        return rulelists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, desc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.rule_title);
            desc = itemView.findViewById(R.id.rule_desc);
        }
    }
}
