package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.ShowBusineePostModel;
import com.example.marathavyapari.Model.SliderItem;
import com.example.marathavyapari.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class BusinessSliderAapter extends SliderViewAdapter<BusinessSliderAapter.ViewHolder> {

    Context context;
    ArrayList<ShowBusineePostModel.PhotoList> imagelist;

    public BusinessSliderAapter(Context context, ArrayList<ShowBusineePostModel.PhotoList> imagelist) {
        this.context = context;
        this.imagelist = imagelist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


       /* viewHolder.textViewDescription.setText(imagelist.get(position).getImage_file());
        viewHolder.textViewDescription.setTextSize(16);
        viewHolder.textViewDescription.setTextColor(Color.WHITE);*/

        String imagepath = Base_Url.imagepath + imagelist.get(position).getImage_file();

        Glide.with(viewHolder.itemView)
                .load(imagepath)
//                .fitCenter()
                .into(viewHolder.imageViewBackground);
    }

    @Override
    public int getCount() {
        return imagelist.size();
    }

    public class ViewHolder extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
