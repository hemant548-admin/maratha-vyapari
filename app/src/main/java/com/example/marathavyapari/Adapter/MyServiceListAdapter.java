package com.example.marathavyapari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marathavyapari.Base_Url.Base_Url;
import com.example.marathavyapari.Model.BusinessListModel;
import com.example.marathavyapari.Model.ServiceCategoryModel;
import com.example.marathavyapari.Model.ServiceListModel;
import com.example.marathavyapari.R;
import com.example.marathavyapari.User.ServiceActivity;

import java.util.ArrayList;
import java.util.List;

public class MyServiceListAdapter extends RecyclerView.Adapter<MyServiceListAdapter.ViewHolder>{

    Context context;
    ArrayList<ServiceCategoryModel.CategoryList> serviceList;

    public MyServiceListAdapter(Context context, ArrayList<ServiceCategoryModel.CategoryList> serviceList) {
        this.context = context;;
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public MyServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_businesslist, null);
        return new MyServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyServiceListAdapter.ViewHolder holder, int position) {

        holder.name.setText(serviceList.get(position).getSc_name());
        String imagepath = serviceList.get(position).getSc_image();
        String imagepath2  = Base_Url.imagepath + imagepath; ;
        Glide.with(context)
                .load(imagepath2)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.businessname);
            image = itemView.findViewById(R.id.business_image);
        }
    }
}
